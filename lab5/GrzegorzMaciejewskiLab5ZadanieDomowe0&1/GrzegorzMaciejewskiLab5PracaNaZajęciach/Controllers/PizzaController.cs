﻿using GrzegorzMaciejewskiLab5PracaNaZajęciach.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GrzegorzMaciejewskiLab5PracaNaZajęciach.Controllers
{
    public class PizzaController : Controller
    {
        //Kliknięcie na menu przenosi do widoku listy
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }
        /// <summary>
        /// Metoda obsługująca kliknięcie przycisku zamówienie na liscie wszystkich pizz, przenosi do widoku
        /// zamawiania pizzy, id pizzy jest przekazywane
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Order(int id)
        {
            return View(new Order());
        }

        /// <summary>
        /// Przycisk "zamów" w widoku zamawiania pizzy, tworzy nowe zamówienie z danych wprowadzonych w widoku
        /// oraz z pizzy którą znajdujemy po ID.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Order(Order model,int id)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Order");
            }
            
            using (var ctx = new EFDbContext())
            {
                Pizza pizza = ctx.Pizzas.FirstOrDefault(m => m.Id == id);
                model.PizzaName = pizza.Name;
                ctx.Orders.Add(model);
                ctx.SaveChanges();
            }
            return RedirectToAction("OrderList");
        }
        /// <summary>
        /// Wyświetla w widoku OrderList listę wszystkich zamówień 
        /// </summary>
        /// <returns></returns>
        public ActionResult OrderList()
        {
            var orders = new List<Order>();
            using (var ctx = new EFDbContext())
            {
                orders = ctx.Orders.ToList();
            }
            return View(orders);
        }
        /// <summary>
        /// wyświetla w menu listę wszystkich pizzy które oferuje pizzeria
        /// </summary>
        /// <returns></returns>
        public ActionResult List()
        {
            var pizzas=new List<Pizza>();
            using (var ctx = new EFDbContext())
            {
                pizzas = ctx.Pizzas.ToList();
            }
              return View(pizzas);
        }
        /// <summary>
        /// Przenosi do widoku dodawania nowej pizzy
        /// </summary>
        /// <returns></returns>
        public ActionResult Add()
        {
            return View(new Pizza());
        }
        /// <summary>
        /// Widok dodawania nowej pizzy, dodaje nową pizze do listy wszstkich oferowanych przez pizzerie.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Add(Pizza model)
        {
            if(!ModelState.IsValid)
            {
                return RedirectToAction("Add");
            }
            using (var ctx = new EFDbContext())
            {
                ctx.Pizzas.Add(model);
                ctx.SaveChanges();
            }
                return RedirectToAction("List");
        }
        /// <summary>
        /// Przenosi do widoku edycji już istnieącej pizzy, identyfidujemy ją po ID które przekazujemy do widoku
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Edit(int id)
        {
            Pizza pizza;
            using (var ctx = new EFDbContext())
            {
                pizza = ctx.Pizzas.FirstOrDefault(m => m.Id == id);
            }
            return View(pizza);
        }
        /// <summary>
        /// Widok edycji, modyfikujemy  już istniejącą pizze i przypisujemy jej inne składniki/nazwe itd
        /// </summary>
        /// <param name="model"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Edit(Pizza model,int id)
        {
           
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Edit");
            }
            Pizza pizza;
            using (var ctx = new EFDbContext())
            {
                pizza = ctx.Pizzas.FirstOrDefault(m => m.Id == id);
                pizza.Name = model.Name;
                pizza.Ingredients = model.Ingredients;
                ctx.SaveChanges();
            }
            return RedirectToAction("List");
        }
        /// <summary>
        /// Usuwanie pizzy z listy menu, usuwamy ją z bazy danch odnajdując ją po ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Delete(int id)
        {
            Pizza pizzaToDelete;
            using (var ctx = new EFDbContext())
            {
                pizzaToDelete = ctx.Pizzas.FirstOrDefault(m => m.Id == id);
                ctx.Pizzas.Remove(pizzaToDelete);
                ctx.SaveChanges();
            }
            return RedirectToAction("List");
        }
        /// <summary>
        /// Usuwanie zamówienia z listy zamówień w bazie danych, zamówienie odnajdujemy po ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult DeleteOrder(int id)
        {
            Order orderToDelete;
            using (var ctx = new EFDbContext())
            {
                orderToDelete = ctx.Orders.FirstOrDefault(m => m.Id == id);
                ctx.Orders.Remove(orderToDelete);
                ctx.SaveChanges();
            }
            return RedirectToAction("OrderList");
        }

        
    }
}