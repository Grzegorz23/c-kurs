﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GrzegorzMaciejewskiLab5PracaNaZajęciach.Controllers
{
    /// <summary>
    /// Obsługa strony głownej, metoda zwraca widok Index
    /// </summary>
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View(); // prawym i add view
        }
    }
}