namespace GrzegorzMaciejewskiLab5PracaNaZajęciach.Migrations
{
    using Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<GrzegorzMaciejewskiLab5PracaNaZajęciach.Models.EFDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(GrzegorzMaciejewskiLab5PracaNaZajęciach.Models.EFDbContext context)
        {
            //  This method will be called after migrating to the latest version.
            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            
            context.Pizzas.AddOrUpdate(p => p.Name, new Pizza { Id = 1, Name = "Margharitta", Ingredients = "Ser,sos" },
                new Pizza { Id = 2, Name = "Salami", Ingredients = "salami" },
                new Pizza { Id = 3, Name = "Hawajska", Ingredients = "ananasy" });
            //  enable-migratons
            // add-migration "init"
            // update-database
        }
    }
}
