﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GrzegorzMaciejewskiLab5PracaNaZajęciach.Models
{
    public class Order
    {
        public int Id { get; set; }
        [Display(Name = "Adres")]
        public string Adress { get; set; }
        public String PizzaName {get;set;}
        [Display(Name = "Numer kontaktowy")]
        public String PhoneNumber { get; set; }
    }
}