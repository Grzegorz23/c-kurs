﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GrzegorzMaciejewskiLab5PracaNaZajęciach.Models
{
    public class Pizza
    {
        public int Id { get; set; }
        [Display(Name="Nazwa")]
        public string Name { get; set; }
        [Display(Name = "Skladniki")]
        public string Ingredients { get; set; }

    }
}