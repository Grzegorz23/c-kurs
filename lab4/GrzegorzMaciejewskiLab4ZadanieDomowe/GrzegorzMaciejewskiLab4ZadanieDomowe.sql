USE [master]
GO
/****** Object:  Database [GrzegorzMaciejewskiLab4ZadanieDomowe]    Script Date: 2016-06-16 18:07:10 ******/
CREATE DATABASE [GrzegorzMaciejewskiLab4ZadanieDomowe]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Example.Model.DatabaseContext', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\Example.Model.DatabaseContext.mdf' , SIZE = 4288KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Example.Model.DatabaseContext_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\Example.Model.DatabaseContext_log.ldf' , SIZE = 1072KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [GrzegorzMaciejewskiLab4ZadanieDomowe] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [GrzegorzMaciejewskiLab4ZadanieDomowe].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [GrzegorzMaciejewskiLab4ZadanieDomowe] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [GrzegorzMaciejewskiLab4ZadanieDomowe] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [GrzegorzMaciejewskiLab4ZadanieDomowe] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [GrzegorzMaciejewskiLab4ZadanieDomowe] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [GrzegorzMaciejewskiLab4ZadanieDomowe] SET ARITHABORT OFF 
GO
ALTER DATABASE [GrzegorzMaciejewskiLab4ZadanieDomowe] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [GrzegorzMaciejewskiLab4ZadanieDomowe] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [GrzegorzMaciejewskiLab4ZadanieDomowe] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [GrzegorzMaciejewskiLab4ZadanieDomowe] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [GrzegorzMaciejewskiLab4ZadanieDomowe] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [GrzegorzMaciejewskiLab4ZadanieDomowe] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [GrzegorzMaciejewskiLab4ZadanieDomowe] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [GrzegorzMaciejewskiLab4ZadanieDomowe] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [GrzegorzMaciejewskiLab4ZadanieDomowe] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [GrzegorzMaciejewskiLab4ZadanieDomowe] SET  ENABLE_BROKER 
GO
ALTER DATABASE [GrzegorzMaciejewskiLab4ZadanieDomowe] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [GrzegorzMaciejewskiLab4ZadanieDomowe] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [GrzegorzMaciejewskiLab4ZadanieDomowe] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [GrzegorzMaciejewskiLab4ZadanieDomowe] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [GrzegorzMaciejewskiLab4ZadanieDomowe] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [GrzegorzMaciejewskiLab4ZadanieDomowe] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [GrzegorzMaciejewskiLab4ZadanieDomowe] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [GrzegorzMaciejewskiLab4ZadanieDomowe] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [GrzegorzMaciejewskiLab4ZadanieDomowe] SET  MULTI_USER 
GO
ALTER DATABASE [GrzegorzMaciejewskiLab4ZadanieDomowe] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [GrzegorzMaciejewskiLab4ZadanieDomowe] SET DB_CHAINING OFF 
GO
ALTER DATABASE [GrzegorzMaciejewskiLab4ZadanieDomowe] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [GrzegorzMaciejewskiLab4ZadanieDomowe] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [GrzegorzMaciejewskiLab4ZadanieDomowe] SET DELAYED_DURABILITY = DISABLED 
GO
USE [GrzegorzMaciejewskiLab4ZadanieDomowe]
GO
/****** Object:  Table [dbo].[__MigrationHistory]    Script Date: 2016-06-16 18:07:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[__MigrationHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ContextKey] [nvarchar](300) NOT NULL,
	[Model] [varbinary](max) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_dbo.__MigrationHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC,
	[ContextKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Fields]    Script Date: 2016-06-16 18:07:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Fields](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FieldCoordinates] [nvarchar](max) NULL,
	[CrossOrCircle] [nvarchar](max) NULL,
	[GameId] [int] NOT NULL,
 CONSTRAINT [PK_dbo.Fields] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Games]    Script Date: 2016-06-16 18:07:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Games](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Player1Score] [nvarchar](max) NULL,
	[Player2Score] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.Games] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Players]    Script Date: 2016-06-16 18:07:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Players](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[GameId] [int] NOT NULL,
 CONSTRAINT [PK_dbo.Players] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
INSERT [dbo].[__MigrationHistory] ([MigrationId], [ContextKey], [Model], [ProductVersion]) VALUES (N'201605071942504_InitialCreate', N'GrzegorzMaciejewskiLab4ZadanieDomowe.Model.TickTackToeDatabaseContext', 0x1F8B0800000000000400ED5A5F6FDB36107F1FB0EF20E8691B522B71F7B005768BCC498A60491C446931ECA5A0A5B3C3952235924AED0EFB647BD847DA57D8E9AF25CAB2A5347692A20812C4E2DDF1EEF8E3F1F893FFFBE7DFC1EB79C0AC3B908A0A3EB40F7AFBB605DC133EE5B3A11DE9E98B9FECD7AFBEFD6670E20773EB5D2EF73296434DAE86F6ADD6E1A1E328EF1602A27A01F5A45062AA7B9E081CE20BA7BFBFFFB37370E0009AB0D196650DAE23AE6900C907FC3812DC835047845D081F98CA9EE3889B58B52E49002A241E0CED37F213CC84FC74413C0A7FC047F5819E93C98FBF139F700AC722101FA19798B1AD2346097AE8029BDA16E15C68A2D1FFC3B70A5C2D059FB9213E20EC661102CA4D095390C575B8146F1BE27E3F0ED1592AE6A6BC486911743478F032CB9963AADF2BF3769153CCEA09665F2FE2A893CC6252F1AF6D99131D8E988C85BA24BD179BDAB3DA28EC157842D8C53F7BD628623A9230E4106949D89E75154D18F57E85C58DF8007CC823C6CA91602C385679808FAEA40841EAC5354CB3F8CE7CDB72AA7A8EA958A89574D2E8CFB87ED9B7AD4B9C9C4C1814402965CAD542C21BE0208906FF8A680D92C7362049756D7663AE2B4616200F5C0FADE4B322427113DAD605999F039FE9DBA18DFFDAD6299D839F3FC93C79CB29EE5954D232827693F5B734D925B9A3B32429C6B4E754E9F1F49402F3956D5D034B84D42D0DD31D9A20E77D55EA548AE05AB00CA195C1F7AE88A41707209A246E889C81EEEA619A9E8D2E16622B7DCC46D739998BACF272E02CB7E8DA8D9B44FA403B37B1F575EB76DCBA49D64642483C32D180DAFAF61D6191576339A2D263DB2F16316037A7F35ED84D37C003813735F615BD1DD17B991CFCCF0643474A098F2691976C570F8DAA2B27DCB7369E2069F87981C70C201A6888EB8F2E60536C9BEB3CE6C7C0408375E4A5FDDC88288FF8F54460187E7B7FF293A0E44F56DFAB0EFD509B070107325E71C2B097560861CA751D9D947B34246C53420CC596C08EC32DA630478E21041E637253F06DE6CEF1549FBF98C658864DF919382564B5065CDE02B458E15A3FF0D89033BB8F9243795DDE36E88C9CEC167546FC4F0C7669F5431D8D1A2033076EA8F7E186E0AF8063A2C98428884560AE571CA278C7CDCE519555691315F1142EE85274D8BC2CEB6E059ECE66651727861506B212B6C142D14ED7F473381A064A99ABFA615C214A728DF70C7341DB1C1A450045EA6AB86853EB0D335912CD3D5D0DB65B228ACCAECDC4CA5AD6B69ADD371766112AD929BC6E9D8ABC672876CD924572521A29A79B9C06BE697041C2105BA012FF943DB1DC947C1ABD70BBB32F416AC3F1D40A12A6F0B698091B3A32036314A7464F4FA9543ADFFAB635F2839AD89A1AD1B001F389CB65A0BE7AF9A6CCA5E3FF338D6EDCD08A529BD93CC5B883B858273DAD81A9BA5AC20E1246E48AF679245814F0E633A359BBCAC494ED5447BA5AEC375AEC375A1C3846666A074F6D5D6ABD4075915B4320AD435B82417A2274C74183DE768050BFD7976DD547DB5B362EF065B3C6507B9B796F5236D6D4AF3C1AB09ACF98078155D62874C75593E27680955EB9CBFA972B5AAC67B2D8B543D71429662F0E5FE3901D6407DEE6373FB5133015B12D4CCF1DF5E3D3CF5D280D412F16E8B97FB211A318EF52E002313305A5534EC7EEEF1FF48D97444FE7858DA394CF5ABCB5D9392945E38C6EA49DD6D23BED5E80F03B22BD5B22BF0BC8FCFBB2C5FBBEE4E860B02301FF652C421395FD590BB192AEFE2C8B553A31C944EDF67B86D7F1F9D0FE2B5139B4CE7E7B9F6AED59638985E0D0DAB7FE7E601EFBCBC04099107ECEABF4B94CF1635174195BB2430A7817C45B53C7FFBCB9DDC7C248CE88ED92B3DD054A1A1BF827C8C5D6E998CD0C6923BB9A36B243DB9F085CD5B4D6AD24D26A66B3E2D044BAAE329C13A3AD08D9357CEC2ADB4DACDDEEC8DA1A19B9968BACA4AFC4313C5902B65B78D5352CDF75B742ABD66F72B8ED4ADFF4C30DAFE86C6922FEDE1F07AFB2E10A99333E15F9BE373CCA458C8EE30234F171371E494DA7C4D338EC8152C93BED7784452872124CC03FE3E3488791C6902198B0CA3BF2B87EAC9B3FE18EAB3E0FC661F22DA1870801DDA418028CF92F118D7198F97DBAA2F169301117A6AC1F8CD752C77DE16C5158BA14BCA5A12C7D453DBD812064F1B560CC5D7207F7F1EDAD827398116F915FC89B8D6C5E886ADA07C794CC2409546663A98F1F11C37E307FF53FD2E5E713F02A0000, N'6.1.3-40302')
/****** Object:  Index [IX_GameId]    Script Date: 2016-06-16 18:07:10 ******/
CREATE NONCLUSTERED INDEX [IX_GameId] ON [dbo].[Fields]
(
	[GameId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_GameId]    Script Date: 2016-06-16 18:07:10 ******/
CREATE NONCLUSTERED INDEX [IX_GameId] ON [dbo].[Players]
(
	[GameId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Fields]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Fields_dbo.Games_GameId] FOREIGN KEY([GameId])
REFERENCES [dbo].[Games] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Fields] CHECK CONSTRAINT [FK_dbo.Fields_dbo.Games_GameId]
GO
ALTER TABLE [dbo].[Players]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Players_dbo.Games_GameId] FOREIGN KEY([GameId])
REFERENCES [dbo].[Games] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Players] CHECK CONSTRAINT [FK_dbo.Players_dbo.Games_GameId]
GO
USE [master]
GO
ALTER DATABASE [GrzegorzMaciejewskiLab4ZadanieDomowe] SET  READ_WRITE 
GO
