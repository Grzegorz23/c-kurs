﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrzegorzMaciejewskiLab4ZadanieDomowe.Model
{
  
    public class Player : Entity
    {
        public Player() { }
        public Player(String name)
        {
            this.Name = name;
        }
        public virtual string Name { get; set; }
       public virtual int GameId { get; set; }
    }
}
