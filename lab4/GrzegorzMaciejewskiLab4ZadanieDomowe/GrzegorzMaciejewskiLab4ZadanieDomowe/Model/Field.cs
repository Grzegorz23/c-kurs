﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GrzegorzMaciejewskiLab4ZadanieDomowe.Model
{
    public class Field : Entity
    {
       public string FieldCoordinates { get; set; }
       public String CrossOrCircle { get; set; }
        public int GameId { get; set; }

       public Field(String coordinates,String image)
        {
            FieldCoordinates = coordinates;
            CrossOrCircle = image;
        }

        public Field() { }

    }
}
