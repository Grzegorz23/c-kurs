﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrzegorzMaciejewskiLab4ZadanieDomowe.Model
{
    public class TickTackToeDatabaseContext : DbContext
    {
        // podajemy nazwę bazy danych podaną w app.config
        public TickTackToeDatabaseContext()
            : base("name=TickTackToe")
        {
            
            // usuwa całą bazę danych i tworzy nową gdy Model zostanie zmieniony
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<TickTackToeDatabaseContext>());
        }

        //tabele w bazie danych
        public virtual DbSet<Field> Fields { get; set; }
        public virtual DbSet<Player> Players { get; set; }
        public virtual DbSet<Game> Games { get; set; }
        
    }
}

