﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrzegorzMaciejewskiLab4ZadanieDomowe.Model
{
    public class Game:Entity
    {

        
        public virtual IList<Player> ListOfPlayers { get; set; }
        public virtual IList<Field> ListOfFields { get; set; }
        public string Player1Score { get; set; }
        public string Player2Score { get; set; }

        public Game()
        {
            ListOfFields = new List<Field>();
            ListOfPlayers = new List<Player>();
            
        }
    }
}
