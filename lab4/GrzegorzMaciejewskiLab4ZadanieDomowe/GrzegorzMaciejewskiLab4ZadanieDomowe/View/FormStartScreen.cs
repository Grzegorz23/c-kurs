﻿using GrzegorzMaciejewskiLab4ZadanieDomowe.Model;
using GrzegorzMaciejewskiLab4ZadanieDomowe.Query;
using GrzegorzMaciejewskiLab4ZadanieDomowe.Repository.Command;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GrzegorzMaciejewskiLab4ZadanieDomowe
{
    public partial class FormStartScreen : Form
    {
        private readonly WriteRepository<Game> _writeGameRepository;
        private readonly ReadRepository<Game> _readGameRepository;
        private readonly ReadFields _readFieldsRepository;
        private readonly TickTackToeDatabaseContext _context;
        public static Player playerOne; // zmienna przekazywana do Form tiktak, tak samo ta niżej
        public static Player playerTwo;
        public static IList<Field> listOfFields; // lista którą przekazuje do Formt tiktaka, żeby odpowiednio przydzielic pola
        public static string playerOneScore; // zmienne przekazywane do tiktaka żeby ustalić wynik po odczycie
        public static string playerTwoScore;
         public FormStartScreen()
        {
            _context = new TickTackToeDatabaseContext();
            _writeGameRepository = new WriteRepository<Game>(_context);
            _readGameRepository = new ReadRepository<Game>(_context);
            _readFieldsRepository = new ReadFields(_context); ;
            InitializeComponent();
            dataGridViewSavedGames.DataSource = null;
            dataGridViewSavedGames.DataSource = _readGameRepository.GetAll();
            dataGridViewSavedGames.Columns["ListOfPlayers"].Visible = false;
            dataGridViewSavedGames.Columns["ListOfFields"].Visible = false;
        }

       /// <summary>
       /// Przy starcie pobiera nazwy graczy podanych przez nas i tworzy nowych, chowa okienko
       /// </summary>
       /// <param name="sender"></param>
       /// <param name="e"></param>
        private void buttonStart_Click(object sender, EventArgs e)
        {
            if (textBoxPlayer1Name.Text != "" && textBoxPlayer2Name.Text != "")
            {
                playerOne = new Player(textBoxPlayer1Name.Text);
                playerTwo = new Player(textBoxPlayer2Name.Text);
                Form.ActiveForm.Hide();
                TickTackToe tickTackToe = new TickTackToe();
                tickTackToe.ShowDialog();
                Close();
            }
            else
                MessageBox.Show("Wprowadz nazwy graczy");
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            Close();
        }
        /// <summary>
        /// Ładowanie zapisanego stanu gry tworzy nową zmienną typu Game która jest wybranym przez nas stanem gry, 
        /// niestety stany gry się nadpisują przy wielokrotnym klikaniu przycisku przez co wyskakuje wyjątek przy próbie
        /// odczytu tego nadpisanego, a ze względu na późną pore nie byłem w stanie wymiślić lepszego rozwiązania
        /// niż dodanie możliwości usunięcia nadpisanego stanu i wyświetleniu komunikatu że został nadpisany i daremny
        /// jego odczyt :c . A no i jeszcze tworze nowych graczy dlatego że potrzebują nowego ID, bo przy próbie zapisu
        /// poprzednio wczytanej gry wyskakiwał błąd który tylko tak udało mi się naprawić - jeżeli jakoś inaczej da się
        /// to zrobić to byłbym wdzięczny za podpowiedź.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonLoad_Click(object sender, EventArgs e)
        {
            Boolean goodSave = false;
            Game gameToLoad = new Game();
                gameToLoad = _readGameRepository.GetAll()[dataGridViewSavedGames.SelectedRows[0].Index];
                listOfFields = _readFieldsRepository.FieldsByGameID(gameToLoad.Id).ToList();
                try
                {
                    playerOne = new Player(gameToLoad.ListOfPlayers.ToList().ElementAt(0).Name);
                    playerTwo = new Player(gameToLoad.ListOfPlayers.ToList().ElementAt(1).Name);
                    goodSave = true;
                }
                catch (Exception exc)
                {
                    MessageBox.Show("Ta Gra zostałą nadpisana, spróbuj tą z dalszym ID");
                }
            if (goodSave)
            {
                playerOneScore = gameToLoad.Player1Score;
                playerTwoScore = gameToLoad.Player2Score;
                Form.ActiveForm.Hide();
                TickTackToe tickTackToe = new TickTackToe();
                tickTackToe.LoadGame();
                tickTackToe.ShowDialog();
                Close();
            }
            
        }
        /// <summary>
        /// Usuwanie save'a, ukryte kolumny list graczy i pół żeby było ładnie.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonDelete_Click(object sender, EventArgs e)
        {
            Game gameToDelete = _readGameRepository.GetAll()[dataGridViewSavedGames.SelectedRows[0].Index];
            _writeGameRepository.Delete(gameToDelete);
            dataGridViewSavedGames.DataSource = null;
            dataGridViewSavedGames.DataSource = _readGameRepository.GetAll();
            dataGridViewSavedGames.Columns["ListOfPlayers"].Visible = false;
            dataGridViewSavedGames.Columns["ListOfFields"].Visible = false;
        }
    }
}
