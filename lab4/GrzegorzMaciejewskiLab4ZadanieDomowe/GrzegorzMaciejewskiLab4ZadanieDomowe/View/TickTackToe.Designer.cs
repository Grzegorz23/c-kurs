﻿namespace GrzegorzMaciejewskiLab4ZadanieDomowe
{
    partial class TickTackToe
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelPlayerOne = new System.Windows.Forms.Label();
            this.labelPlayerTwo = new System.Windows.Forms.Label();
            this.labelResult = new System.Windows.Forms.Label();
            this.labelPlayer2Score = new System.Windows.Forms.Label();
            this.labelPlayer1Score = new System.Windows.Forms.Label();
            this.labelWhoseTurn = new System.Windows.Forms.Label();
            this.pictureBox5x2 = new System.Windows.Forms.PictureBox();
            this.pictureBox5x3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4x3 = new System.Windows.Forms.PictureBox();
            this.pictureBox5x4 = new System.Windows.Forms.PictureBox();
            this.pictureBox5x5 = new System.Windows.Forms.PictureBox();
            this.pictureBox4x5 = new System.Windows.Forms.PictureBox();
            this.pictureBox4x4 = new System.Windows.Forms.PictureBox();
            this.pictureBox5x1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2x5 = new System.Windows.Forms.PictureBox();
            this.pictureBox4x1 = new System.Windows.Forms.PictureBox();
            this.pictureBox4x2 = new System.Windows.Forms.PictureBox();
            this.pictureBox2x4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3x1 = new System.Windows.Forms.PictureBox();
            this.pictureBox3x2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3x3 = new System.Windows.Forms.PictureBox();
            this.pictureBox3x4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3x5 = new System.Windows.Forms.PictureBox();
            this.pictureBox1x3 = new System.Windows.Forms.PictureBox();
            this.pictureBox1x2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1x4 = new System.Windows.Forms.PictureBox();
            this.pictureBox1x5 = new System.Windows.Forms.PictureBox();
            this.pictureBox2x1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2x2 = new System.Windows.Forms.PictureBox();
            this.pictureBox2x3 = new System.Windows.Forms.PictureBox();
            this.pictureBox1x1 = new System.Windows.Forms.PictureBox();
            this.labelPlayerOneName = new System.Windows.Forms.Label();
            this.labelPlayerTwoName = new System.Windows.Forms.Label();
            this.buttonSaveGame = new System.Windows.Forms.Button();
            this.buttonExit = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5x2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5x3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4x3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5x4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5x5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4x5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4x4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5x1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2x5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4x1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4x2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2x4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3x1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3x2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3x3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3x4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3x5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1x3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1x2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1x4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1x5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2x1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2x2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2x3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1x1)).BeginInit();
            this.SuspendLayout();
            // 
            // labelPlayerOne
            // 
            this.labelPlayerOne.AutoSize = true;
            this.labelPlayerOne.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelPlayerOne.Location = new System.Drawing.Point(12, 78);
            this.labelPlayerOne.Name = "labelPlayerOne";
            this.labelPlayerOne.Size = new System.Drawing.Size(94, 29);
            this.labelPlayerOne.TabIndex = 25;
            this.labelPlayerOne.Text = "Gracz 1";
            // 
            // labelPlayerTwo
            // 
            this.labelPlayerTwo.AutoSize = true;
            this.labelPlayerTwo.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelPlayerTwo.Location = new System.Drawing.Point(787, 78);
            this.labelPlayerTwo.Name = "labelPlayerTwo";
            this.labelPlayerTwo.Size = new System.Drawing.Size(94, 29);
            this.labelPlayerTwo.TabIndex = 26;
            this.labelPlayerTwo.Text = "Gracz 2";
            // 
            // labelResult
            // 
            this.labelResult.AutoSize = true;
            this.labelResult.Font = new System.Drawing.Font("Monotype Corsiva", 21.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelResult.Location = new System.Drawing.Point(394, 406);
            this.labelResult.Name = "labelResult";
            this.labelResult.Size = new System.Drawing.Size(112, 36);
            this.labelResult.TabIndex = 30;
            this.labelResult.Text = "WYNIK";
            // 
            // labelPlayer2Score
            // 
            this.labelPlayer2Score.AutoSize = true;
            this.labelPlayer2Score.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelPlayer2Score.Location = new System.Drawing.Point(551, 486);
            this.labelPlayer2Score.Name = "labelPlayer2Score";
            this.labelPlayer2Score.Size = new System.Drawing.Size(51, 55);
            this.labelPlayer2Score.TabIndex = 31;
            this.labelPlayer2Score.Text = "0";
            // 
            // labelPlayer1Score
            // 
            this.labelPlayer1Score.AutoSize = true;
            this.labelPlayer1Score.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelPlayer1Score.Location = new System.Drawing.Point(306, 486);
            this.labelPlayer1Score.Name = "labelPlayer1Score";
            this.labelPlayer1Score.Size = new System.Drawing.Size(51, 55);
            this.labelPlayer1Score.TabIndex = 32;
            this.labelPlayer1Score.Text = "0";
            // 
            // labelWhoseTurn
            // 
            this.labelWhoseTurn.AutoSize = true;
            this.labelWhoseTurn.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelWhoseTurn.Location = new System.Drawing.Point(382, 40);
            this.labelWhoseTurn.Name = "labelWhoseTurn";
            this.labelWhoseTurn.Size = new System.Drawing.Size(0, 24);
            this.labelWhoseTurn.TabIndex = 33;
            // 
            // pictureBox5x2
            // 
            this.pictureBox5x2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pictureBox5x2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox5x2.Location = new System.Drawing.Point(343, 340);
            this.pictureBox5x2.Name = "pictureBox5x2";
            this.pictureBox5x2.Size = new System.Drawing.Size(69, 59);
            this.pictureBox5x2.TabIndex = 83;
            this.pictureBox5x2.TabStop = false;
            this.pictureBox5x2.Click += new System.EventHandler(this.PictureBoxClick);
            // 
            // pictureBox5x3
            // 
            this.pictureBox5x3.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pictureBox5x3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox5x3.Location = new System.Drawing.Point(413, 340);
            this.pictureBox5x3.Name = "pictureBox5x3";
            this.pictureBox5x3.Size = new System.Drawing.Size(69, 59);
            this.pictureBox5x3.TabIndex = 82;
            this.pictureBox5x3.TabStop = false;
            this.pictureBox5x3.Click += new System.EventHandler(this.PictureBoxClick);
            // 
            // pictureBox4x3
            // 
            this.pictureBox4x3.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pictureBox4x3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox4x3.Location = new System.Drawing.Point(413, 279);
            this.pictureBox4x3.Name = "pictureBox4x3";
            this.pictureBox4x3.Size = new System.Drawing.Size(69, 59);
            this.pictureBox4x3.TabIndex = 81;
            this.pictureBox4x3.TabStop = false;
            this.pictureBox4x3.Click += new System.EventHandler(this.PictureBoxClick);
            // 
            // pictureBox5x4
            // 
            this.pictureBox5x4.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pictureBox5x4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox5x4.Location = new System.Drawing.Point(483, 340);
            this.pictureBox5x4.Name = "pictureBox5x4";
            this.pictureBox5x4.Size = new System.Drawing.Size(69, 59);
            this.pictureBox5x4.TabIndex = 80;
            this.pictureBox5x4.TabStop = false;
            this.pictureBox5x4.Click += new System.EventHandler(this.PictureBoxClick);
            // 
            // pictureBox5x5
            // 
            this.pictureBox5x5.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pictureBox5x5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox5x5.Location = new System.Drawing.Point(553, 340);
            this.pictureBox5x5.Name = "pictureBox5x5";
            this.pictureBox5x5.Size = new System.Drawing.Size(69, 59);
            this.pictureBox5x5.TabIndex = 79;
            this.pictureBox5x5.TabStop = false;
            this.pictureBox5x5.Click += new System.EventHandler(this.PictureBoxClick);
            // 
            // pictureBox4x5
            // 
            this.pictureBox4x5.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pictureBox4x5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox4x5.Location = new System.Drawing.Point(553, 279);
            this.pictureBox4x5.Name = "pictureBox4x5";
            this.pictureBox4x5.Size = new System.Drawing.Size(69, 59);
            this.pictureBox4x5.TabIndex = 78;
            this.pictureBox4x5.TabStop = false;
            this.pictureBox4x5.Click += new System.EventHandler(this.PictureBoxClick);
            // 
            // pictureBox4x4
            // 
            this.pictureBox4x4.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pictureBox4x4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox4x4.Location = new System.Drawing.Point(483, 279);
            this.pictureBox4x4.Name = "pictureBox4x4";
            this.pictureBox4x4.Size = new System.Drawing.Size(69, 59);
            this.pictureBox4x4.TabIndex = 77;
            this.pictureBox4x4.TabStop = false;
            this.pictureBox4x4.Click += new System.EventHandler(this.PictureBoxClick);
            // 
            // pictureBox5x1
            // 
            this.pictureBox5x1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pictureBox5x1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox5x1.Location = new System.Drawing.Point(273, 340);
            this.pictureBox5x1.Name = "pictureBox5x1";
            this.pictureBox5x1.Size = new System.Drawing.Size(69, 59);
            this.pictureBox5x1.TabIndex = 76;
            this.pictureBox5x1.TabStop = false;
            this.pictureBox5x1.Click += new System.EventHandler(this.PictureBoxClick);
            // 
            // pictureBox2x5
            // 
            this.pictureBox2x5.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pictureBox2x5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox2x5.Location = new System.Drawing.Point(553, 157);
            this.pictureBox2x5.Name = "pictureBox2x5";
            this.pictureBox2x5.Size = new System.Drawing.Size(69, 59);
            this.pictureBox2x5.TabIndex = 75;
            this.pictureBox2x5.TabStop = false;
            this.pictureBox2x5.Click += new System.EventHandler(this.PictureBoxClick);
            // 
            // pictureBox4x1
            // 
            this.pictureBox4x1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pictureBox4x1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox4x1.Location = new System.Drawing.Point(273, 279);
            this.pictureBox4x1.Name = "pictureBox4x1";
            this.pictureBox4x1.Size = new System.Drawing.Size(69, 59);
            this.pictureBox4x1.TabIndex = 74;
            this.pictureBox4x1.TabStop = false;
            this.pictureBox4x1.Click += new System.EventHandler(this.PictureBoxClick);
            // 
            // pictureBox4x2
            // 
            this.pictureBox4x2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pictureBox4x2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox4x2.Location = new System.Drawing.Point(343, 279);
            this.pictureBox4x2.Name = "pictureBox4x2";
            this.pictureBox4x2.Size = new System.Drawing.Size(69, 59);
            this.pictureBox4x2.TabIndex = 73;
            this.pictureBox4x2.TabStop = false;
            this.pictureBox4x2.Click += new System.EventHandler(this.PictureBoxClick);
            // 
            // pictureBox2x4
            // 
            this.pictureBox2x4.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pictureBox2x4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox2x4.Location = new System.Drawing.Point(483, 157);
            this.pictureBox2x4.Name = "pictureBox2x4";
            this.pictureBox2x4.Size = new System.Drawing.Size(69, 59);
            this.pictureBox2x4.TabIndex = 72;
            this.pictureBox2x4.TabStop = false;
            this.pictureBox2x4.Click += new System.EventHandler(this.PictureBoxClick);
            // 
            // pictureBox3x1
            // 
            this.pictureBox3x1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pictureBox3x1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox3x1.Location = new System.Drawing.Point(273, 218);
            this.pictureBox3x1.Name = "pictureBox3x1";
            this.pictureBox3x1.Size = new System.Drawing.Size(69, 59);
            this.pictureBox3x1.TabIndex = 71;
            this.pictureBox3x1.TabStop = false;
            this.pictureBox3x1.Click += new System.EventHandler(this.PictureBoxClick);
            // 
            // pictureBox3x2
            // 
            this.pictureBox3x2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pictureBox3x2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox3x2.Location = new System.Drawing.Point(343, 218);
            this.pictureBox3x2.Name = "pictureBox3x2";
            this.pictureBox3x2.Size = new System.Drawing.Size(69, 59);
            this.pictureBox3x2.TabIndex = 70;
            this.pictureBox3x2.TabStop = false;
            this.pictureBox3x2.Click += new System.EventHandler(this.PictureBoxClick);
            // 
            // pictureBox3x3
            // 
            this.pictureBox3x3.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pictureBox3x3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox3x3.Location = new System.Drawing.Point(413, 218);
            this.pictureBox3x3.Name = "pictureBox3x3";
            this.pictureBox3x3.Size = new System.Drawing.Size(69, 59);
            this.pictureBox3x3.TabIndex = 69;
            this.pictureBox3x3.TabStop = false;
            this.pictureBox3x3.Click += new System.EventHandler(this.PictureBoxClick);
            // 
            // pictureBox3x4
            // 
            this.pictureBox3x4.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pictureBox3x4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox3x4.Location = new System.Drawing.Point(483, 218);
            this.pictureBox3x4.Name = "pictureBox3x4";
            this.pictureBox3x4.Size = new System.Drawing.Size(69, 59);
            this.pictureBox3x4.TabIndex = 68;
            this.pictureBox3x4.TabStop = false;
            this.pictureBox3x4.Click += new System.EventHandler(this.PictureBoxClick);
            // 
            // pictureBox3x5
            // 
            this.pictureBox3x5.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pictureBox3x5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox3x5.Location = new System.Drawing.Point(553, 218);
            this.pictureBox3x5.Name = "pictureBox3x5";
            this.pictureBox3x5.Size = new System.Drawing.Size(69, 59);
            this.pictureBox3x5.TabIndex = 67;
            this.pictureBox3x5.TabStop = false;
            this.pictureBox3x5.Click += new System.EventHandler(this.PictureBoxClick);
            // 
            // pictureBox1x3
            // 
            this.pictureBox1x3.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pictureBox1x3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1x3.Location = new System.Drawing.Point(413, 96);
            this.pictureBox1x3.Name = "pictureBox1x3";
            this.pictureBox1x3.Size = new System.Drawing.Size(69, 59);
            this.pictureBox1x3.TabIndex = 66;
            this.pictureBox1x3.TabStop = false;
            this.pictureBox1x3.Click += new System.EventHandler(this.PictureBoxClick);
            // 
            // pictureBox1x2
            // 
            this.pictureBox1x2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pictureBox1x2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1x2.Location = new System.Drawing.Point(343, 96);
            this.pictureBox1x2.Name = "pictureBox1x2";
            this.pictureBox1x2.Size = new System.Drawing.Size(69, 59);
            this.pictureBox1x2.TabIndex = 65;
            this.pictureBox1x2.TabStop = false;
            this.pictureBox1x2.Click += new System.EventHandler(this.PictureBoxClick);
            // 
            // pictureBox1x4
            // 
            this.pictureBox1x4.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pictureBox1x4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1x4.Location = new System.Drawing.Point(483, 96);
            this.pictureBox1x4.Name = "pictureBox1x4";
            this.pictureBox1x4.Size = new System.Drawing.Size(69, 59);
            this.pictureBox1x4.TabIndex = 64;
            this.pictureBox1x4.TabStop = false;
            this.pictureBox1x4.Click += new System.EventHandler(this.PictureBoxClick);
            // 
            // pictureBox1x5
            // 
            this.pictureBox1x5.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pictureBox1x5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1x5.Location = new System.Drawing.Point(553, 96);
            this.pictureBox1x5.Name = "pictureBox1x5";
            this.pictureBox1x5.Size = new System.Drawing.Size(69, 59);
            this.pictureBox1x5.TabIndex = 63;
            this.pictureBox1x5.TabStop = false;
            this.pictureBox1x5.Click += new System.EventHandler(this.PictureBoxClick);
            // 
            // pictureBox2x1
            // 
            this.pictureBox2x1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pictureBox2x1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox2x1.Location = new System.Drawing.Point(273, 157);
            this.pictureBox2x1.Name = "pictureBox2x1";
            this.pictureBox2x1.Size = new System.Drawing.Size(69, 59);
            this.pictureBox2x1.TabIndex = 62;
            this.pictureBox2x1.TabStop = false;
            this.pictureBox2x1.Click += new System.EventHandler(this.PictureBoxClick);
            // 
            // pictureBox2x2
            // 
            this.pictureBox2x2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pictureBox2x2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox2x2.Location = new System.Drawing.Point(343, 157);
            this.pictureBox2x2.Name = "pictureBox2x2";
            this.pictureBox2x2.Size = new System.Drawing.Size(69, 59);
            this.pictureBox2x2.TabIndex = 61;
            this.pictureBox2x2.TabStop = false;
            this.pictureBox2x2.Click += new System.EventHandler(this.PictureBoxClick);
            // 
            // pictureBox2x3
            // 
            this.pictureBox2x3.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pictureBox2x3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox2x3.Location = new System.Drawing.Point(413, 157);
            this.pictureBox2x3.Name = "pictureBox2x3";
            this.pictureBox2x3.Size = new System.Drawing.Size(69, 59);
            this.pictureBox2x3.TabIndex = 60;
            this.pictureBox2x3.TabStop = false;
            this.pictureBox2x3.Click += new System.EventHandler(this.PictureBoxClick);
            // 
            // pictureBox1x1
            // 
            this.pictureBox1x1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pictureBox1x1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1x1.Location = new System.Drawing.Point(273, 96);
            this.pictureBox1x1.Name = "pictureBox1x1";
            this.pictureBox1x1.Size = new System.Drawing.Size(69, 59);
            this.pictureBox1x1.TabIndex = 59;
            this.pictureBox1x1.TabStop = false;
            this.pictureBox1x1.Click += new System.EventHandler(this.PictureBoxClick);
            // 
            // labelPlayerOneName
            // 
            this.labelPlayerOneName.AutoSize = true;
            this.labelPlayerOneName.Font = new System.Drawing.Font("Monotype Corsiva", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelPlayerOneName.Location = new System.Drawing.Point(12, 119);
            this.labelPlayerOneName.Name = "labelPlayerOneName";
            this.labelPlayerOneName.Size = new System.Drawing.Size(154, 25);
            this.labelPlayerOneName.TabIndex = 84;
            this.labelPlayerOneName.Text = "Nazwa Gracza 1";
            // 
            // labelPlayerTwoName
            // 
            this.labelPlayerTwoName.AutoSize = true;
            this.labelPlayerTwoName.Font = new System.Drawing.Font("Monotype Corsiva", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelPlayerTwoName.Location = new System.Drawing.Point(651, 119);
            this.labelPlayerTwoName.Name = "labelPlayerTwoName";
            this.labelPlayerTwoName.Size = new System.Drawing.Size(154, 25);
            this.labelPlayerTwoName.TabIndex = 85;
            this.labelPlayerTwoName.Text = "Nazwa Gracza 2";
            // 
            // buttonSaveGame
            // 
            this.buttonSaveGame.Location = new System.Drawing.Point(12, 570);
            this.buttonSaveGame.Name = "buttonSaveGame";
            this.buttonSaveGame.Size = new System.Drawing.Size(137, 33);
            this.buttonSaveGame.TabIndex = 86;
            this.buttonSaveGame.Text = "Zapisz";
            this.buttonSaveGame.UseVisualStyleBackColor = true;
            this.buttonSaveGame.Click += new System.EventHandler(this.buttonSaveGame_Click);
            // 
            // buttonExit
            // 
            this.buttonExit.Cursor = System.Windows.Forms.Cursors.Default;
            this.buttonExit.Location = new System.Drawing.Point(155, 570);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(133, 33);
            this.buttonExit.TabIndex = 87;
            this.buttonExit.Text = "Wyjdź bez zapisu";
            this.buttonExit.UseVisualStyleBackColor = true;
            this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
            // 
            // TickTackToe
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.ClientSize = new System.Drawing.Size(893, 615);
            this.Controls.Add(this.buttonExit);
            this.Controls.Add(this.buttonSaveGame);
            this.Controls.Add(this.labelPlayerTwoName);
            this.Controls.Add(this.labelPlayerOneName);
            this.Controls.Add(this.pictureBox5x2);
            this.Controls.Add(this.pictureBox5x3);
            this.Controls.Add(this.pictureBox4x3);
            this.Controls.Add(this.pictureBox5x4);
            this.Controls.Add(this.pictureBox5x5);
            this.Controls.Add(this.pictureBox4x5);
            this.Controls.Add(this.pictureBox4x4);
            this.Controls.Add(this.pictureBox5x1);
            this.Controls.Add(this.pictureBox2x5);
            this.Controls.Add(this.pictureBox4x1);
            this.Controls.Add(this.pictureBox4x2);
            this.Controls.Add(this.pictureBox2x4);
            this.Controls.Add(this.pictureBox3x1);
            this.Controls.Add(this.pictureBox3x2);
            this.Controls.Add(this.pictureBox3x3);
            this.Controls.Add(this.pictureBox3x4);
            this.Controls.Add(this.pictureBox3x5);
            this.Controls.Add(this.pictureBox1x3);
            this.Controls.Add(this.pictureBox1x2);
            this.Controls.Add(this.pictureBox1x4);
            this.Controls.Add(this.pictureBox1x5);
            this.Controls.Add(this.pictureBox2x1);
            this.Controls.Add(this.pictureBox2x2);
            this.Controls.Add(this.pictureBox2x3);
            this.Controls.Add(this.pictureBox1x1);
            this.Controls.Add(this.labelWhoseTurn);
            this.Controls.Add(this.labelPlayer1Score);
            this.Controls.Add(this.labelPlayer2Score);
            this.Controls.Add(this.labelResult);
            this.Controls.Add(this.labelPlayerTwo);
            this.Controls.Add(this.labelPlayerOne);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "TickTackToe";
            this.Text = "Kółko i krzyżyk";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5x2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5x3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4x3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5x4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5x5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4x5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4x4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5x1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2x5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4x1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4x2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2x4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3x1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3x2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3x3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3x4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3x5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1x3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1x2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1x4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1x5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2x1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2x2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2x3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1x1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label labelPlayerOne;
        private System.Windows.Forms.Label labelPlayerTwo;
        private System.Windows.Forms.Label labelResult;
        private System.Windows.Forms.Label labelPlayer2Score;
        private System.Windows.Forms.Label labelPlayer1Score;
        private System.Windows.Forms.Label labelWhoseTurn;
        private System.Windows.Forms.PictureBox pictureBox1x1;
        private System.Windows.Forms.PictureBox pictureBox2x3;
        private System.Windows.Forms.PictureBox pictureBox2x2;
        private System.Windows.Forms.PictureBox pictureBox2x1;
        private System.Windows.Forms.PictureBox pictureBox1x5;
        private System.Windows.Forms.PictureBox pictureBox1x4;
        private System.Windows.Forms.PictureBox pictureBox1x2;
        private System.Windows.Forms.PictureBox pictureBox1x3;
        private System.Windows.Forms.PictureBox pictureBox3x5;
        private System.Windows.Forms.PictureBox pictureBox3x4;
        private System.Windows.Forms.PictureBox pictureBox3x3;
        private System.Windows.Forms.PictureBox pictureBox3x2;
        private System.Windows.Forms.PictureBox pictureBox3x1;
        private System.Windows.Forms.PictureBox pictureBox2x4;
        private System.Windows.Forms.PictureBox pictureBox4x2;
        private System.Windows.Forms.PictureBox pictureBox4x1;
        private System.Windows.Forms.PictureBox pictureBox2x5;
        private System.Windows.Forms.PictureBox pictureBox5x1;
        private System.Windows.Forms.PictureBox pictureBox4x4;
        private System.Windows.Forms.PictureBox pictureBox4x5;
        private System.Windows.Forms.PictureBox pictureBox5x5;
        private System.Windows.Forms.PictureBox pictureBox5x4;
        private System.Windows.Forms.PictureBox pictureBox4x3;
        private System.Windows.Forms.PictureBox pictureBox5x3;
        private System.Windows.Forms.PictureBox pictureBox5x2;
        private System.Windows.Forms.Label labelPlayerOneName;
        private System.Windows.Forms.Label labelPlayerTwoName;
        private System.Windows.Forms.Button buttonSaveGame;
        private System.Windows.Forms.Button buttonExit;
    }
}

