﻿using GrzegorzMaciejewskiLab4ZadanieDomowe.Model;
using GrzegorzMaciejewskiLab4ZadanieDomowe.Properties;
using GrzegorzMaciejewskiLab4ZadanieDomowe.Query;
using GrzegorzMaciejewskiLab4ZadanieDomowe.Repository.Command;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GrzegorzMaciejewskiLab4ZadanieDomowe
{
    public partial class TickTackToe : Form
    {

        private readonly WriteRepository<Game> _writeGameRepository;
        private readonly ReadRepository<Game> _readGameRepository;
        private readonly TickTackToeDatabaseContext _context;
        IList<Field> listOfOccuredFields; // Lista zawierajaca wszystkie zajęte pola planszy
        Field[][] gameArray;              // tablica odzwierciedlająca plansze, w celu łatwego rozpoznawania stanu gry
        IList<Game> listOfFinishedGames;
        /// <summary>
        /// Przy inicjalizacji Forma ustawiane są imiona graczy oraz tablica pozorująca plansze wypełniana jest
        /// pustymi referencjami żeby przy przeszukiwaniu całej planszy w poszukiwaniu zwycięzcy nie wyrzuciło
        /// null pointer exception.
        /// </summary>
        public TickTackToe()
        {
            _context = new TickTackToeDatabaseContext();
            _writeGameRepository = new WriteRepository<Game>(_context);
            _readGameRepository = new ReadRepository<Game>(_context);
            InitializeComponent();
            labelPlayerOneName.Text = FormStartScreen.playerOne.Name;
            labelPlayerTwoName.Text = FormStartScreen.playerTwo.Name;
            listOfOccuredFields = new List<Field>();
            listOfFinishedGames = new List<Game>();
            gameArray = new Field[5][];
            for(int x=0;x<gameArray.Length;x++)
            {
                gameArray[x] = new Field[5];
            }
            for (int i = 0; i < 5; i++)
                for (int j = 0; j < 5; j++)
                    gameArray[i][j] = new Field();
            labelWhoseTurn.Text = "Ruch Gracza nr 1";
        }
        /// <summary>
        /// Obsługa kliknięcia na każdy z PictureBoxów, każdy pic box ma swoją nazwe która jednocześnie jest
        /// jego identyfikatorem położenia, w nazwie ukryte są współrzędne x i y dzięki czemu przy użyciu 
        /// Regex.Split można łatwo przełożyć planszę na tablice i szybko zobaczyć czy ktoś jeszcze nie wygrał
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PictureBoxClick(object sender, EventArgs e)
        {
            PictureBox temporaryPic = (PictureBox)sender;
            String tempPicName = temporaryPic.Name.ToString();
            if (labelWhoseTurn.Text == "Ruch Gracza nr 1")
            {
                if (temporaryPic.Image==null)
                    {
                        Field fieldToAdd = new Field(tempPicName, "Krzyżyk");
                        temporaryPic.Image = Resources.Krzyżyk;
                        labelWhoseTurn.Text = "Ruch Gracza nr 2";
                        listOfOccuredFields.Add(fieldToAdd);
                    String[] coordinatesToArray = Regex.Split(tempPicName, @"\D+");
                    gameArray[Int32.Parse(coordinatesToArray[1])-1][Int32.Parse(coordinatesToArray[2])-1]=fieldToAdd;
                    }
            }
            else
            {
                if (temporaryPic.Image == null)
                {
                    Field fieldToAdd = new Field(tempPicName, "Kółko");
                    temporaryPic.Image = Resources.Kółko;
                    labelWhoseTurn.Text = "Ruch Gracza nr 1";
                    listOfOccuredFields.Add(new Field(tempPicName, "Kółko"));
                    String[] coordinatesToArray = Regex.Split(tempPicName, @"\D+");
                    gameArray[Int32.Parse(coordinatesToArray[1])-1][Int32.Parse(coordinatesToArray[2])-1] = fieldToAdd;
                }
            }
            bool somebodyWon=DidSomebodyWin();
            if(somebodyWon)
            {
                if (Int32.Parse(labelPlayer1Score.Text) >= 3)
                {
                    MessageBox.Show("Gracz 1 wygrał tę partie");
                    ClearAll();
                    ClearScores();
                }
                else if (Int32.Parse(labelPlayer2Score.Text) >= 3)
                {
                    MessageBox.Show("Gracz 2 wygrał tę partię");
                    ClearAll();
                    ClearScores();
                }
            }
            if(CheckTheDraw())
            {
                MessageBox.Show("REMIS");
                ClearAll();
            }
        }
        // Metoda sprawdzająca czy obecny stan planszy wskazuje na to czy ktoś wygrał czy nie
        // metode uruchamiam po każdym ruchu, sprawdza wszystkie możliwe kombinacje w których ktoś może wygrać
        public bool DidSomebodyWin()
        {
            bool somebodyWin = false;
            for (int pionowo = 0; pionowo < 5; pionowo++)
            {
                // poziomo
                int licznikPoziomoKrzyże = 0;
                int licznikPoziomoKółka = 0;
                //pionowo
                int licznikPionowoKrzyże = 0;
                int licznikPionowoKółka = 0;
                //środkowa przekątna od lewego górnego rogu
                int licznikSkosKrzyżeSrodekOdZLewa = 0;
                int licznikSkosKółkaSrodekOdLewa = 0;
                // środkowa przekątna od prawego górnego rogu
                int licznikSkosKrzyżeŚrodekZPrawa = 0;
                int licznikSkosKółkaŚrodekZPrawa = 0;
                // przekątna pod środkową przekątną od lewej
                int licznikSkosPodOdLewejKrzyże = 0;
                int licznikSkosPodOdLewejKółka = 0;
                // przekątna nad środkową od lewej
                int licznikSkosNadOdLewejKrzyże = 0;
                int licznikSkosNadOdLewejKółka = 0;
                // przekątna pod środkową od prawej
                int licznikSkosPodOdPrawejKrzyże = 0;
                int licznikSkosPodOdPrawejKółka = 0;
                // przekątna nad środkową prawej
                int licznikSkosNadOdPrawejKrzyże = 0;
                int licznikSkosNadOdPrawejKółka = 0;
                //PĘTLA DRUGA POZIOMA
                for (int poziomo = 0; poziomo < 5&&!somebodyWin; poziomo++)
                {
                    //poziomo
                    if (gameArray[pionowo][poziomo].CrossOrCircle == "Krzyżyk")
                        licznikPoziomoKrzyże++;
                    else if (gameArray[pionowo][poziomo].CrossOrCircle == "Kółko")
                        licznikPoziomoKółka++;
                    //pionowo
                    if (gameArray[poziomo][pionowo].CrossOrCircle == "Krzyżyk")
                        licznikPionowoKrzyże++;
                    else if (gameArray[poziomo][pionowo].CrossOrCircle == "Kółko")
                        licznikPionowoKółka++;
                    //skos Środek od lewego górnego rogu
                    if (gameArray[poziomo][poziomo].CrossOrCircle == "Krzyżyk")
                        licznikSkosKrzyżeSrodekOdZLewa++;
                    else if (gameArray[poziomo][poziomo].CrossOrCircle == "Kółko")
                        licznikSkosKółkaSrodekOdLewa++;
                    //skos środek og prawego górnego rogu
                    if (gameArray[poziomo][gameArray.GetLength(0) - 1 - poziomo].CrossOrCircle == "Krzyżyk")
                        licznikSkosKrzyżeŚrodekZPrawa++;
                    else if (gameArray[poziomo][gameArray.GetLength(0) - 1 - poziomo].CrossOrCircle == "Kółko")
                        licznikSkosKółkaŚrodekZPrawa++;

                    if (licznikPoziomoKrzyże == 4 || licznikPionowoKrzyże == 4 || licznikSkosKrzyżeSrodekOdZLewa == 4 ||
                        licznikSkosKrzyżeŚrodekZPrawa == 4)
                    {
                        labelPlayer1Score.Text = (Int32.Parse(labelPlayer1Score.Text) + 1).ToString();
                        ClearAll();
                        somebodyWin = true;
                    }
                    else if (licznikPoziomoKółka == 4 || licznikPionowoKółka == 4 || licznikSkosKółkaSrodekOdLewa == 4 ||
                             licznikSkosKółkaŚrodekZPrawa == 4)
                    {
                        labelPlayer2Score.Text = (Int32.Parse(labelPlayer2Score.Text) + 1).ToString();
                        ClearAll();
                        somebodyWin = true;
                    }
                }
                // specjalna pętla dla przekątnych nad główną przekątną i pod nią
                for (int w = 0; w < 4&&!somebodyWin; w++)
                {
                    if(gameArray[w+1][w].CrossOrCircle == "Krzyżyk")
                        licznikSkosPodOdLewejKrzyże++;
                    else if (gameArray[w+1][w].CrossOrCircle == "Kółko")
                        licznikSkosPodOdLewejKółka++;
                    if (gameArray[w][w+1].CrossOrCircle == "Krzyżyk")
                        licznikSkosNadOdLewejKrzyże++;
                    else if (gameArray[w][w+1].CrossOrCircle == "Kółko")
                        licznikSkosNadOdLewejKółka++;
                    if (gameArray[w+1][gameArray.GetLength(0)-1-w].CrossOrCircle == "Krzyżyk")
                        licznikSkosPodOdPrawejKrzyże++;
                    else if (gameArray[w + 1][gameArray.GetLength(0) - 1 - w].CrossOrCircle == "Kółko")
                        licznikSkosPodOdPrawejKółka++;
                    if (gameArray[w][3-w].CrossOrCircle == "Krzyżyk")
                        licznikSkosNadOdPrawejKrzyże++;
                    else if (gameArray[w][3-w].CrossOrCircle == "Kółko")
                        licznikSkosNadOdPrawejKółka++;
                    if (licznikSkosPodOdLewejKrzyże == 4 || licznikSkosNadOdLewejKrzyże == 4 ||
                    licznikSkosPodOdPrawejKrzyże == 4 || licznikSkosNadOdPrawejKrzyże == 4)
                    {
                        labelPlayer1Score.Text = (Int32.Parse(labelPlayer1Score.Text) + 1).ToString();
                        ClearAll();
                        somebodyWin = true;
                    }
                    else if (licznikSkosPodOdLewejKółka == 4 || licznikSkosNadOdLewejKółka == 4 ||
                    licznikSkosPodOdPrawejKółka == 4 || licznikSkosNadOdPrawejKółka == 4)
                    {
                        labelPlayer2Score.Text = (Int32.Parse(labelPlayer2Score.Text) + 1).ToString();
                        ClearAll();
                        somebodyWin = true;
                    }
                }
            }
            return somebodyWin;
        }
        /// <summary>
        /// Metoda używana w momencie zapełnienia planszy lub wygranej jednego z graczy, czyści plansze
        /// i pojemniki danych, wypełnia tablice pustymi referencjami żeby nie było null pointer exc przy sprawdzaniu
        /// czy ktoś jeszcze nie wygrał
        /// </summary>
        public void ClearAll()
        {
            listOfOccuredFields = new List<Field>();
            gameArray = new Field[5][];
            for (int x = 0; x < gameArray.Length; x++)
            {
                gameArray[x] = new Field[5];
            }
            for (int i = 0; i < 5; i++)
                for (int j = 0; j < 5; j++)
                    gameArray[i][j] = new Field();
            labelWhoseTurn.Text = "Ruch Gracza nr 1";
            pictureBox1x1.Image = null;
            pictureBox1x2.Image = null;
            pictureBox1x3.Image = null;
            pictureBox1x4.Image = null;
            pictureBox1x5.Image = null;
            pictureBox2x1.Image = null;
            pictureBox2x2.Image = null;
            pictureBox2x3.Image = null;
            pictureBox2x4.Image = null;
            pictureBox2x5.Image = null;
            pictureBox3x1.Image = null;
            pictureBox3x2.Image = null;
            pictureBox3x3.Image = null;
            pictureBox3x4.Image = null;
            pictureBox3x5.Image = null;
            pictureBox4x1.Image = null;
            pictureBox4x2.Image = null;
            pictureBox4x3.Image = null;
            pictureBox4x4.Image = null;
            pictureBox4x5.Image = null;
            pictureBox5x1.Image = null;
            pictureBox5x2.Image = null;
            pictureBox5x3.Image = null;
            pictureBox5x4.Image = null;
            pictureBox5x5.Image = null;
        }
        /// <summary>
        /// Metoda używana przy wygranej jednego z graczy, czyli jak ktos osiągnie 3 punkty, służy do zerowania wyników
        /// </summary>
        public void ClearScores()
        {
            labelPlayer2Score.Text = "0";
            labelPlayer1Score.Text = "0";
        }
        /// <summary>
        /// Metoda używana przy każdym kliknięciu na pole, sprawdza czy czasem nie doszło do remisu przy zapełnionych 
        /// wszystkich polach
        /// </summary>
        /// <returns></returns>
        public bool CheckTheDraw()
        {
            if (
            pictureBox1x2.Image != null &&
            pictureBox1x3.Image != null &&
            pictureBox1x4.Image != null &&
            pictureBox2x1.Image != null &&
            pictureBox2x2.Image != null &&
            pictureBox2x3.Image != null &&
            pictureBox2x4.Image != null &&
            pictureBox2x5.Image != null &&
            pictureBox3x1.Image != null &&
            pictureBox3x2.Image != null &&
            pictureBox3x3.Image != null &&
            pictureBox3x4.Image != null &&
            pictureBox3x5.Image != null &&
            pictureBox4x1.Image != null &&
            pictureBox4x2.Image != null &&
            pictureBox4x3.Image != null &&
            pictureBox4x4.Image != null &&
            pictureBox4x5.Image != null &&
            pictureBox5x1.Image != null &&
            pictureBox5x2.Image != null &&
            pictureBox5x3.Image != null &&
            pictureBox5x4.Image != null &&
            pictureBox5x5.Image != null)
                return true;
            else return false;
        }
        /// <summary>
        /// Metoda zapisuje obecny stan gry do bazy danych 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonSaveGame_Click(object sender, EventArgs e)
        {
            Game gameToSave = new Game();
            gameToSave.ListOfFields = listOfOccuredFields.ToList();
            gameToSave.ListOfPlayers.Add(FormStartScreen.playerOne);
            gameToSave.ListOfPlayers.Add(FormStartScreen.playerTwo);
            gameToSave.Player1Score = labelPlayer1Score.Text;
            gameToSave.Player2Score = labelPlayer2Score.Text;
            _writeGameRepository.Save(gameToSave);
        }
         /// <summary>
        /// Jest to metoda uruchamiana przyciskiem LOAD z FormStartScreen, przypisuje wartości pół któe zostały pobrane
        /// z bazy danych do odpowienich pól z formy TickTackToe, takie jak nazwy graczy, wynik gry itd...
        /// metoda działa na zasadzie sprawdzania czy dane pole było krzyżykiem czy kółkiem, przewidziane są wszystkie
        /// możliwości, żmudnie wypisane, nie widziałem prostszego rozwiązania
        /// </summary>
        public void LoadGame()
        {
            listOfOccuredFields = FormStartScreen.listOfFields;
            labelPlayer1Score.Text = FormStartScreen.playerOneScore;
            labelPlayer2Score.Text = FormStartScreen.playerTwoScore;
            Field[] arrayOfFields=listOfOccuredFields.ToArray();
            
            for (int i = 0; i < arrayOfFields.Length; i++)
            {
                String coordinatesToArray = arrayOfFields[i].FieldCoordinates;
                switch(coordinatesToArray)
                {
                    case "pictureBox1x1":
                        if (arrayOfFields[i].CrossOrCircle == "Krzyżyk")
                        {
                            pictureBox1x1.Image = Resources.Krzyżyk;
                            gameArray[0][0] = new Field(coordinatesToArray, "Krzyżyk");
                        }
                        else
                        {
                            pictureBox1x1.Image = Resources.Kółko;
                            gameArray[0][0] = new Field(coordinatesToArray, "Kółko");
                        }
                        break;
                    case "pictureBox1x2":
                        if (arrayOfFields[i].CrossOrCircle == "Krzyżyk")
                        {
                            pictureBox1x2.Image = Resources.Krzyżyk;
                            gameArray[0][1] = new Field(coordinatesToArray, "Krzyżyk");
                        }
                        else
                        {
                            pictureBox1x2.Image = Resources.Kółko;
                            gameArray[0][1] = new Field(coordinatesToArray, "Kółko");
                        }
                        break;
                    case "pictureBox1x3":
                        if (arrayOfFields[i].CrossOrCircle == "Krzyżyk")
                        {
                            pictureBox1x3.Image = Resources.Krzyżyk;
                            gameArray[0][2] = new Field(coordinatesToArray, "Krzyżyk");
                        }
                        else
                        {
                            pictureBox1x3.Image = Resources.Kółko;
                            gameArray[0][2] = new Field(coordinatesToArray, "Kółko");
                        }
                        break;
                    case "pictureBox1x4":
                        if (arrayOfFields[i].CrossOrCircle == "Krzyżyk")
                        {
                            pictureBox1x4.Image = Resources.Krzyżyk;
                            gameArray[0][3] = new Field(coordinatesToArray, "Krzyżyk");
                        }
                        else
                        {
                            pictureBox1x4.Image = Resources.Kółko;
                            gameArray[0][3] = new Field(coordinatesToArray, "Kółko");
                        }
                        break;
                    case "pictureBox1x5":
                        if (arrayOfFields[i].CrossOrCircle == "Krzyżyk")
                        {
                            pictureBox1x5.Image = Resources.Krzyżyk;
                            gameArray[0][4] = new Field(coordinatesToArray, "Krzyżyk");
                        }
                        else
                        {
                            pictureBox1x5.Image = Resources.Kółko;
                            gameArray[0][4] = new Field(coordinatesToArray, "Kółko");
                        }
                        break;
                    case "pictureBox2x1":
                        if (arrayOfFields[i].CrossOrCircle == "Krzyżyk")
                        {
                            pictureBox2x1.Image = Resources.Krzyżyk;
                            gameArray[1][0] = new Field(coordinatesToArray, "Krzyżyk");
                        }
                        else
                        {
                            pictureBox2x1.Image = Resources.Kółko;
                            gameArray[1][0] = new Field(coordinatesToArray, "Kółko");
                        }
                        break;
                    case "pictureBox2x2":
                        if (arrayOfFields[i].CrossOrCircle == "Krzyżyk")
                        {
                            pictureBox2x2.Image = Resources.Krzyżyk;
                            gameArray[1][1] = new Field(coordinatesToArray, "Krzyżyk");
                        }
                        else
                        {
                            pictureBox2x2.Image = Resources.Kółko;
                            gameArray[1][1] = new Field(coordinatesToArray, "Kółko");
                        }
                        break;
                    case "pictureBox2x3":
                        if (arrayOfFields[i].CrossOrCircle == "Krzyżyk")
                        {
                            pictureBox2x3.Image = Resources.Krzyżyk;
                            gameArray[1][2] = new Field(coordinatesToArray, "Krzyżyk");
                        }
                        else
                        {
                            pictureBox2x3.Image = Resources.Kółko;
                            gameArray[1][2] = new Field(coordinatesToArray, "Kółko");
                        }
                        break;
                    case "pictureBox2x4":
                        if (arrayOfFields[i].CrossOrCircle == "Krzyżyk")
                        {
                            pictureBox2x4.Image = Resources.Krzyżyk;
                            gameArray[1][3] = new Field(coordinatesToArray, "Krzyżyk");
                        }
                        else
                        {
                            pictureBox2x4.Image = Resources.Kółko;
                            gameArray[1][3] = new Field(coordinatesToArray, "Kółko");
                        }
                        break;
                    case "pictureBox2x5":
                        if (arrayOfFields[i].CrossOrCircle == "Krzyżyk")
                        {
                            pictureBox2x5.Image = Resources.Krzyżyk;
                            gameArray[1][4] = new Field(coordinatesToArray, "Krzyżyk");
                        }
                        else
                        {
                            pictureBox2x5.Image = Resources.Kółko;
                            gameArray[1][4] = new Field(coordinatesToArray, "Kółko");
                        }
                        break;
                    case "pictureBox3x1":
                        if (arrayOfFields[i].CrossOrCircle == "Krzyżyk")
                        {
                            pictureBox3x1.Image = Resources.Krzyżyk;
                            gameArray[2][0] = new Field(coordinatesToArray, "Krzyżyk");
                        }
                        else
                        {
                            pictureBox3x1.Image = Resources.Kółko;
                            gameArray[2][0] = new Field(coordinatesToArray, "Kółko");
                        }
                        break;
                    case "pictureBox3x2":
                        if (arrayOfFields[i].CrossOrCircle == "Krzyżyk")
                        {
                            pictureBox3x2.Image = Resources.Krzyżyk;
                            gameArray[2][1] = new Field(coordinatesToArray, "Krzyżyk");
                        }
                        else
                        {
                            pictureBox3x2.Image = Resources.Kółko;
                            gameArray[2][1] = new Field(coordinatesToArray, "Kółko");
                        }
                        break;
                    case "pictureBox3x3":
                        if (arrayOfFields[i].CrossOrCircle == "Krzyżyk")
                        {
                            pictureBox3x3.Image = Resources.Krzyżyk;
                            gameArray[2][2] = new Field(coordinatesToArray, "Krzyżyk");
                        }
                        else
                        {
                            pictureBox3x3.Image = Resources.Kółko;
                            gameArray[2][2] = new Field(coordinatesToArray, "Kółko");
                        }
                        break;
                    case "pictureBox3x4":
                        if (arrayOfFields[i].CrossOrCircle == "Krzyżyk")
                        {
                            pictureBox3x4.Image = Resources.Krzyżyk;
                            gameArray[2][3] = new Field(coordinatesToArray, "Krzyżyk");
                        }
                        else
                        {
                            pictureBox3x4.Image = Resources.Kółko;
                            gameArray[2][3] = new Field(coordinatesToArray, "Kółko");
                        }
                        break;
                    case "pictureBox3x5":
                        if (arrayOfFields[i].CrossOrCircle == "Krzyżyk")
                        {
                            pictureBox3x5.Image = Resources.Krzyżyk;
                            gameArray[2][4] = new Field(coordinatesToArray, "Krzyżyk");
                        }
                        else
                        {
                            pictureBox3x5.Image = Resources.Kółko;
                            gameArray[2][4] = new Field(coordinatesToArray, "Kółko");
                        }
                        break;
                    case "pictureBox4x1":
                        if (arrayOfFields[i].CrossOrCircle == "Krzyżyk")
                        {
                            pictureBox4x1.Image = Resources.Krzyżyk;
                            gameArray[3][0] = new Field(coordinatesToArray, "Krzyżyk");
                        }
                        else
                        {
                            pictureBox4x1.Image = Resources.Kółko;
                            gameArray[3][0] = new Field(coordinatesToArray, "Kółko");
                        }
                        break;
                    case "pictureBox4x2":
                        if (arrayOfFields[i].CrossOrCircle == "Krzyżyk")
                        {
                            pictureBox4x2.Image = Resources.Krzyżyk;
                            gameArray[3][1] = new Field(coordinatesToArray, "Krzyżyk");
                        }
                        else
                        {
                            pictureBox4x2.Image = Resources.Kółko;
                            gameArray[3][1] = new Field(coordinatesToArray, "Kółko");
                        }
                        break;
                    case "pictureBox4x3":
                        if (arrayOfFields[i].CrossOrCircle == "Krzyżyk")
                        {
                            pictureBox4x3.Image = Resources.Krzyżyk;
                            gameArray[3][2] = new Field(coordinatesToArray, "Krzyżyk");
                        }
                        else
                        {
                            pictureBox4x3.Image = Resources.Kółko;
                            gameArray[3][2] = new Field(coordinatesToArray, "Kółko");
                        }
                        break;
                    case "pictureBox4x4":
                        if (arrayOfFields[i].CrossOrCircle == "Krzyżyk")
                        {
                            pictureBox4x4.Image = Resources.Krzyżyk;
                            gameArray[3][3] = new Field(coordinatesToArray, "Krzyżyk");
                        }
                        else
                        {
                            pictureBox4x4.Image = Resources.Kółko;
                            gameArray[3][3] = new Field(coordinatesToArray, "Kółko");
                        }
                        break;
                    case "pictureBox4x5":
                        if (arrayOfFields[i].CrossOrCircle == "Krzyżyk")
                        {
                            pictureBox4x5.Image = Resources.Krzyżyk;
                            gameArray[3][4] = new Field(coordinatesToArray, "Krzyżyk");
                        }
                        else
                        {
                            pictureBox4x5.Image = Resources.Kółko;
                            gameArray[3][4] = new Field(coordinatesToArray, "Kółko");
                        }
                        break;
                    case "pictureBox5x1":
                        if (arrayOfFields[i].CrossOrCircle == "Krzyżyk")
                        {
                            pictureBox5x1.Image = Resources.Krzyżyk;
                            gameArray[4][0] = new Field(coordinatesToArray, "Krzyżyk");
                        }
                        else
                        {
                            pictureBox5x1.Image = Resources.Kółko;
                            gameArray[4][0] = new Field(coordinatesToArray, "Kółko");
                        }
                        break;
                    case "pictureBox5x2":
                        if (arrayOfFields[i].CrossOrCircle == "Krzyżyk")
                        {
                            pictureBox5x2.Image = Resources.Krzyżyk;
                            gameArray[4][1] = new Field(coordinatesToArray, "Krzyżyk");
                        }
                        else
                        {
                            pictureBox5x2.Image = Resources.Kółko;
                            gameArray[4][1] = new Field(coordinatesToArray, "Kółko");
                        }
                        break;
                    case "pictureBox5x3":
                        if (arrayOfFields[i].CrossOrCircle == "Krzyżyk")
                        {
                            pictureBox5x3.Image = Resources.Krzyżyk;
                            gameArray[4][2] = new Field(coordinatesToArray, "Krzyżyk");
                        }
                        else
                        {
                            pictureBox5x3.Image = Resources.Kółko;
                            gameArray[4][2] = new Field(coordinatesToArray, "Kółko");
                        }
                        break;
                    case "pictureBox5x4":
                        if (arrayOfFields[i].CrossOrCircle == "Krzyżyk")
                        {
                            pictureBox5x4.Image = Resources.Krzyżyk;
                            gameArray[4][3] = new Field(coordinatesToArray, "Krzyżyk");
                        }
                        else
                        {
                            pictureBox5x4.Image = Resources.Kółko;
                            gameArray[4][3] = new Field(coordinatesToArray, "Kółko");
                        }
                        break;
                    case "pictureBox5x5":
                        if (arrayOfFields[i].CrossOrCircle == "Krzyżyk")
                        {
                            pictureBox5x5.Image = Resources.Krzyżyk;
                            gameArray[4][4] = new Field(coordinatesToArray, "Krzyżyk");
                        }
                        else
                        {
                            pictureBox5x5.Image = Resources.Kółko;
                            gameArray[4][4] = new Field(coordinatesToArray, "Kółko");
                        }
                        break;
                }
             }
        }
        /// <summary>
        /// Wyjście
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonExit_Click(object sender, EventArgs e)
        {
            Close();
        }
    }

}
