﻿using GrzegorzMaciejewskiLab4ZadanieDomowe.Model;
using GrzegorzMaciejewskiLab4ZadanieDomowe.Repository.Command.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrzegorzMaciejewskiLab4ZadanieDomowe.Repository.Command
{
    /// <summary>
    /// Typ generyczny daje możliwosć określenia typu w momencie tworzenia obiektu
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class WriteRepository<T> : IWriteRepository<T> where T : Entity
    {
        private readonly TickTackToeDatabaseContext _context;
        public WriteRepository(TickTackToeDatabaseContext context)
        {
            _context = context;
        }
        /// <summary>
        /// Usuwanie z bazy danych
        /// </summary>
        /// <param name="entity"></param>
        public void Delete(T entity)
        {
            _context.Set<T>().Remove(entity);
            _context.SaveChanges();
        }
        /// <summary>
        /// Zapis do bazy
        /// </summary>
        /// <param name="entity"></param>
        public void Save(T entity)
        {
            _context.Set<T>().Add(entity);
            _context.SaveChanges();
        }
        /// <summary>
        /// Modyfikacja
        /// </summary>
        /// <param name="entity"></param>
        public void Update(T entity)
        {
            _context.Entry(entity);
        }
    }
}
