﻿using GrzegorzMaciejewskiLab4ZadanieDomowe.Model;
using GrzegorzMaciejewskiLab4ZadanieDomowe.Query.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrzegorzMaciejewskiLab4ZadanieDomowe.Query
{
    public class ReadRepository<T> : IReadRepository<T> where T : Entity
    {
        protected readonly TickTackToeDatabaseContext Context;
        public ReadRepository(TickTackToeDatabaseContext context)
        {
            Context = context;
        }

        /// <summary>
        /// odczyt wszystkiech elementw typu generycznego T
        /// </summary>
        /// <returns></returns>
        public IList<T> GetAll()
        {
            return Context.Set<T>().ToList();
        }

        
        /// <summary>
        /// Odczyt elementu o podanym ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public T GetById(int id)
        {
            return Context.Set<T>().Where(x => x.Id == id).FirstOrDefault();
        }
    
    }
}
