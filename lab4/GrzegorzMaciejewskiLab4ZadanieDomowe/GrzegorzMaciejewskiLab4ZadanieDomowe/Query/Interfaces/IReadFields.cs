﻿using GrzegorzMaciejewskiLab4ZadanieDomowe.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrzegorzMaciejewskiLab4ZadanieDomowe.Query.Interfaces
{
    /// <summary>
    /// odczyt wszystkich pól z gry o podanym ID
    /// </summary>
    public interface IReadFields
    {
        IList<Field> FieldsByGameID(int Id);
    }
}
