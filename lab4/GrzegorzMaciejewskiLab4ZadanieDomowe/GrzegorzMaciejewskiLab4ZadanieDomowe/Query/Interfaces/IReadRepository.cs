﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrzegorzMaciejewskiLab4ZadanieDomowe.Query.Interfaces
{
    public interface IReadRepository<T> where T : class
    {
        IList<T> GetAll();
        T GetById(int id);
        
    }
}
