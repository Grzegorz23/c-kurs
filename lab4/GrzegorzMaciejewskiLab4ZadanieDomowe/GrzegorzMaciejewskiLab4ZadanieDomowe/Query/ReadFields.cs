﻿using GrzegorzMaciejewskiLab4ZadanieDomowe.Model;
using GrzegorzMaciejewskiLab4ZadanieDomowe.Query.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrzegorzMaciejewskiLab4ZadanieDomowe.Query
{
    public class ReadFields : ReadRepository<Field>, IReadFields
    {

        public ReadFields(TickTackToeDatabaseContext context) 
            : base(context)
        {}
        public IList<Field> FieldsByGameID(int Id)
        {
            return Context.Set<Field>().Where(Query => Query.GameId ==Id).ToList();
        }

        

       
    }
}
