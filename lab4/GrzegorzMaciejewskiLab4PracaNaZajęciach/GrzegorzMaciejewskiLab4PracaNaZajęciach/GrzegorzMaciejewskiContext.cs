﻿namespace GrzegorzMaciejewskiLab4PracaNaZajęciach
{
    using Model;
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class GrzegorzMaciejewskiContext : DbContext
    {
        
        public GrzegorzMaciejewskiContext()
            : base("name=GrzegorzMaciejewskiContext")
        {
            Database.SetInitializer<GrzegorzMaciejewskiContext>(new DropCreateDatabaseIfModelChanges<GrzegorzMaciejewskiContext>());
        }
        public virtual DbSet<Grade> Grades { get; set; }
        public virtual DbSet<Student> Students { get; set; }
        public virtual DbSet<Adress> Adress { get; set; }
        // public virtual DbSet<MyEntity> MyEntities { get; set; }
    }

    
}