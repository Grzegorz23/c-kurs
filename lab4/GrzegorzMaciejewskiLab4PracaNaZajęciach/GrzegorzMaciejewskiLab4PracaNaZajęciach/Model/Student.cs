﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrzegorzMaciejewskiLab4PracaNaZajęciach.Model
{
    public class Student : Entity
    {
        public Student()
        {
            Grades = new List<Grade>();
        }

        
        public string Imie { get; set; }
        public string Nazwisko { get; set; }
        public string Indeks { get; set; }
        //jeden do ielu
        public IList<Grade> Grades { get; set; }
        //jeden do jeden
        public virtual Adress Adress { get; set; } // virtual zeby przy pobieraniu studenta pobierało cały jego adres na raz

    }
}
