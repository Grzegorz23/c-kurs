﻿using GrzegorzMaciejewskiLab4PracaNaZajęciach.Model;
using GrzegorzMaciejewskiLab4PracaNaZajęciach.Repository.Command;
using GrzegorzMaciejewskiLab4PracaNaZajęciach.Repository.Query;
using GrzegorzMaciejewskiLab4PracaNaZajęciach.Repository.Query.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GrzegorzMaciejewskiLab4PracaNaZajęciach
{
    public partial class Form1 : Form
    {
        private readonly WriteRepository<Student> _writeStudentRepository;
        private readonly ReadRepository<Student> _readStudentRepository;
        private readonly GrzegorzMaciejewskiContext _context;
        private readonly IUserQuery _userQuery;
        public Form1()
        {
            _context = new GrzegorzMaciejewskiContext();
            _writeStudentRepository = new WriteRepository<Student>(_context);
            _readStudentRepository = new ReadRepository<Student>(_context);
            _userQuery = new UserQuery(_context);
            InitializeComponent();
        }

        private void buttonAddNewStudent_Click(object sender, EventArgs e)
        {
            Student student = new Student()
            {
                Imie = textBoxAddName.Text,
                Nazwisko = textBoxAddSurname.Text,
                Indeks = textBoxAddIndeks.Text,
            Adress = new Adress()
            {
                City = textBoxCity.Text,
                PostCode = textBoxPostcode.Text
            }
            };
            _writeStudentRepository.Create(student);
            
        }

        private void buttonShowStudents_Click(object sender, EventArgs e)
        {
            dataGridViewStudents.DataSource = null;
            dataGridViewStudents.DataSource = _userQuery.UserWithAdressByName(textBoxName.Text)
                .Select(x => new
                {
                    Imie = x.Imie,
                    Nazwisko = x.Nazwisko,
                    Miasto = x.Adress.City,
                    KodPocztowy = x.Adress.PostCode
                })
            .ToList();
        }
    }
}
