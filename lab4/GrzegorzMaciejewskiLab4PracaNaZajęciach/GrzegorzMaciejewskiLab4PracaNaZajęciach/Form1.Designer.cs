﻿namespace GrzegorzMaciejewskiLab4PracaNaZajęciach
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonAddNewStudent = new System.Windows.Forms.Button();
            this.dataGridViewStudents = new System.Windows.Forms.DataGridView();
            this.buttonShowStudents = new System.Windows.Forms.Button();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.textBoxAddName = new System.Windows.Forms.TextBox();
            this.textBoxAddSurname = new System.Windows.Forms.TextBox();
            this.textBoxAddIndeks = new System.Windows.Forms.TextBox();
            this.groupBoxStudent = new System.Windows.Forms.GroupBox();
            this.groupBoxAdress = new System.Windows.Forms.GroupBox();
            this.textBoxCity = new System.Windows.Forms.TextBox();
            this.textBoxPostcode = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewStudents)).BeginInit();
            this.groupBoxStudent.SuspendLayout();
            this.groupBoxAdress.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonAddNewStudent
            // 
            this.buttonAddNewStudent.Location = new System.Drawing.Point(46, 155);
            this.buttonAddNewStudent.Name = "buttonAddNewStudent";
            this.buttonAddNewStudent.Size = new System.Drawing.Size(75, 36);
            this.buttonAddNewStudent.TabIndex = 0;
            this.buttonAddNewStudent.Text = "Dodaj Studenta";
            this.buttonAddNewStudent.UseVisualStyleBackColor = true;
            this.buttonAddNewStudent.Click += new System.EventHandler(this.buttonAddNewStudent_Click);
            // 
            // dataGridViewStudents
            // 
            this.dataGridViewStudents.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewStudents.Location = new System.Drawing.Point(291, 12);
            this.dataGridViewStudents.Name = "dataGridViewStudents";
            this.dataGridViewStudents.Size = new System.Drawing.Size(502, 425);
            this.dataGridViewStudents.TabIndex = 1;
            // 
            // buttonShowStudents
            // 
            this.buttonShowStudents.Location = new System.Drawing.Point(46, 88);
            this.buttonShowStudents.Name = "buttonShowStudents";
            this.buttonShowStudents.Size = new System.Drawing.Size(75, 36);
            this.buttonShowStudents.TabIndex = 2;
            this.buttonShowStudents.Text = "Pokaz Studentow";
            this.buttonShowStudents.UseVisualStyleBackColor = true;
            this.buttonShowStudents.Click += new System.EventHandler(this.buttonShowStudents_Click);
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(127, 88);
            this.textBoxName.Multiline = true;
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(100, 36);
            this.textBoxName.TabIndex = 3;
            // 
            // textBoxAddName
            // 
            this.textBoxAddName.Location = new System.Drawing.Point(6, 16);
            this.textBoxAddName.Name = "textBoxAddName";
            this.textBoxAddName.Size = new System.Drawing.Size(100, 20);
            this.textBoxAddName.TabIndex = 4;
            // 
            // textBoxAddSurname
            // 
            this.textBoxAddSurname.Location = new System.Drawing.Point(6, 42);
            this.textBoxAddSurname.Name = "textBoxAddSurname";
            this.textBoxAddSurname.Size = new System.Drawing.Size(100, 20);
            this.textBoxAddSurname.TabIndex = 5;
            // 
            // textBoxAddIndeks
            // 
            this.textBoxAddIndeks.Location = new System.Drawing.Point(6, 68);
            this.textBoxAddIndeks.Name = "textBoxAddIndeks";
            this.textBoxAddIndeks.Size = new System.Drawing.Size(100, 20);
            this.textBoxAddIndeks.TabIndex = 6;
            // 
            // groupBoxStudent
            // 
            this.groupBoxStudent.Controls.Add(this.textBoxAddSurname);
            this.groupBoxStudent.Controls.Add(this.textBoxAddName);
            this.groupBoxStudent.Controls.Add(this.textBoxAddIndeks);
            this.groupBoxStudent.Location = new System.Drawing.Point(127, 155);
            this.groupBoxStudent.Name = "groupBoxStudent";
            this.groupBoxStudent.Size = new System.Drawing.Size(132, 100);
            this.groupBoxStudent.TabIndex = 7;
            this.groupBoxStudent.TabStop = false;
            this.groupBoxStudent.Text = "Student";
            // 
            // groupBoxAdress
            // 
            this.groupBoxAdress.Controls.Add(this.textBoxPostcode);
            this.groupBoxAdress.Controls.Add(this.textBoxCity);
            this.groupBoxAdress.Location = new System.Drawing.Point(127, 261);
            this.groupBoxAdress.Name = "groupBoxAdress";
            this.groupBoxAdress.Size = new System.Drawing.Size(122, 100);
            this.groupBoxAdress.TabIndex = 7;
            this.groupBoxAdress.TabStop = false;
            this.groupBoxAdress.Text = "Adres";
            // 
            // textBoxCity
            // 
            this.textBoxCity.Location = new System.Drawing.Point(3, 16);
            this.textBoxCity.Name = "textBoxCity";
            this.textBoxCity.Size = new System.Drawing.Size(100, 20);
            this.textBoxCity.TabIndex = 0;
            // 
            // textBoxPostcode
            // 
            this.textBoxPostcode.Location = new System.Drawing.Point(6, 42);
            this.textBoxPostcode.Name = "textBoxPostcode";
            this.textBoxPostcode.Size = new System.Drawing.Size(100, 20);
            this.textBoxPostcode.TabIndex = 1;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(912, 623);
            this.Controls.Add(this.groupBoxAdress);
            this.Controls.Add(this.groupBoxStudent);
            this.Controls.Add(this.textBoxName);
            this.Controls.Add(this.buttonShowStudents);
            this.Controls.Add(this.dataGridViewStudents);
            this.Controls.Add(this.buttonAddNewStudent);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewStudents)).EndInit();
            this.groupBoxStudent.ResumeLayout(false);
            this.groupBoxStudent.PerformLayout();
            this.groupBoxAdress.ResumeLayout(false);
            this.groupBoxAdress.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonAddNewStudent;
        private System.Windows.Forms.DataGridView dataGridViewStudents;
        private System.Windows.Forms.Button buttonShowStudents;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.TextBox textBoxAddName;
        private System.Windows.Forms.TextBox textBoxAddSurname;
        private System.Windows.Forms.TextBox textBoxAddIndeks;
        private System.Windows.Forms.GroupBox groupBoxStudent;
        private System.Windows.Forms.GroupBox groupBoxAdress;
        private System.Windows.Forms.TextBox textBoxPostcode;
        private System.Windows.Forms.TextBox textBoxCity;
    }
}

