﻿using GrzegorzMaciejewskiLab4PracaNaZajęciach.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrzegorzMaciejewskiLab4PracaNaZajęciach.Repository.Query.Interfaces
{
    public interface IUserQuery
    {
        IList<Student> UserWithAdressByName(String name);
    }
}
