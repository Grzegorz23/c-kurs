﻿using GrzegorzMaciejewskiLab4PracaNaZajęciach.Model;
using GrzegorzMaciejewskiLab4PracaNaZajęciach.Repository.Query.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrzegorzMaciejewskiLab4PracaNaZajęciach.Repository.Query
{
    public class UserQuery : ReadRepository<Student>, IUserQuery
    {
        public UserQuery(GrzegorzMaciejewskiContext context):base(context)
        {

        }

        public IList<Student> UserWithAdressByName(string name)
        {
            return _context.Set<Student>().Where(Query => Query.Imie == name).ToList();
        }
    }

}
