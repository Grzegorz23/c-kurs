﻿using GrzegorzMaciejewskiLab4PracaNaZajęciach.Model;
using GrzegorzMaciejewskiLab4PracaNaZajęciach.Repository.Query.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrzegorzMaciejewskiLab4PracaNaZajęciach.Repository.Query
{
    public class ReadRepository<T> : IReadRepository<T> where T :Entity
    {
        protected readonly GrzegorzMaciejewskiContext _context;

        public ReadRepository(GrzegorzMaciejewskiContext context)
        {
            _context = context;
        }
        public IList<T> GetAll()
        {
            return _context.Set<T>().ToList();
        }
        public T GetById(int id)
        {
            return _context.Set<T>().Where(x => x.Id == id).FirstOrDefault();
        }
    }
}
