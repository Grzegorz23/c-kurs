﻿using GrzegorzMaciejewskiLab4PracaNaZajęciach.Model;
using GrzegorzMaciejewskiLab4PracaNaZajęciach.Repository.Command.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrzegorzMaciejewskiLab4PracaNaZajęciach.Repository.Command
{
    public class WriteRepository<T> : IWriteRepository<T> where T : Entity
    {
        private readonly GrzegorzMaciejewskiContext _context;
        public WriteRepository(GrzegorzMaciejewskiContext context)
        {
            _context = context;
        }
        public void Create(T entity)
        {
            _context.Set<T>().Add(entity);
            _context.SaveChanges();
        }

        public void Delete(T entity)
        {
            _context.Set<T>().Remove(entity);
       }

        public void Edit(T entity)
        {
            _context.Entry<T>(entity);
        }
    }
}
