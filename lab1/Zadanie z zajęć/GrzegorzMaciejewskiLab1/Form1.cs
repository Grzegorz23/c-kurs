﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GrzegorzMaciejewskiLab1
{
    public partial class Form1 : Form
    {
        int number=0;
        public Form1()
        {
            InitializeComponent();
        }

        private void textBoxOpen_TextChanged(object sender, EventArgs e)
        {

        }

        private void buttonOpen_Click(object sender, EventArgs e)
        {
            if (number < 50)
            {
                number += 7;
                textBoxOpen.Text = number.ToString();
                textBoxOpen.BackColor = Color.Green;
            }
            else
            {
                textBoxOpen.Text = "liczba jest za duza";
            }

            //MessageBox.Show("Wartosc: "+ number.ToString());
            
            FormNewWindow formNewWindow = new FormNewWindow();
            formNewWindow.lastNumber = Int32.Parse(textBoxOpen.Text);
            formNewWindow.ShowDialog();
        }

       
    }
}
