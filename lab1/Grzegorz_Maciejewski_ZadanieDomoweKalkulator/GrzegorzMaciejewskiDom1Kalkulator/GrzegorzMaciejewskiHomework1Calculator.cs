﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GrzegorzMaciejewskiDom1Kalkulator
{
    public partial class Calculator : Form
    {
        ///Pierwszy składnik działania, zmienia się przy każdorazowym wciśnięciu operatora bądź znaku
        ///równości.
        //
        Double valueFirst = 0;
        /// Drugi składnik działania, stały i niezmienny przy mnożeniu, dzieleniu itd.
        //
        Double valueSecond = 0;
        ///zmienna przechowująca operator który będzie używany, początkowo brak
        //
        String operation ="";
        ///czy jakiś operator został wciśnięty, początkowo nie został.
        //
        Boolean operation_pressed = false; 
        /// Jeżeli wyrzucony został wyjątek, to trzeba nacisnąć clear
        /// to jest zmienna która sprawdza czy nie chcemy zrobić czasem czegoś innego
        //
        Boolean exception = false;

        public Calculator()
        {
            InitializeComponent();
            textBoxResult.Text = "0";
        }

        /// Metoda, która po kliknięciu na przycisk wyświetla w oknie
        /// textBoxResult to co kliknięty przycisk ma w polu "text"
        /// metoda jest uniwersalna dla każdego przycisku ponieważ pobiera wartość z pola
        /// sender, stosuje ją do każdej cyfry oraz doatkowo dla przecinka jako
        /// że i tak jest składnikiem docelowej liczby.
        //
        private void number_Click(object sender, EventArgs e)  
        {
            if ((textBoxResult.Text == "0"))
                textBoxResult.Clear();
            if (!exception)
            {
               Button b = (Button)sender;
               textBoxResult.Text += b.Text;
            }
        }
        
        /// Metoda którą stosuje do obsługi kliknięcia operatorów
        /// *,/,-,+; wyświetla operator w textBoxCalculations chyba że złapany został wyjątek
        /// wtedy oczekujemy na kliknięcie clear.
        /// jeżeli wciśnięto operator to operation_pressed= true i wartość z textBoxResult zostaje zapisana do pola
        /// valueFirst. Jeżeli wciśnięto operator ponownie przed kliknięciem "=" to wywołaj metode calculate() która 
        /// zaktualizuje wynik o wprowadzoną wartość.
        // 
        private void operator_Click(object sender, EventArgs e)
        {
            if (!exception)
            {
                if (operation_pressed)
                    calculate(operation);
                Button b = (Button)sender;
                operation = b.Text;
                valueFirst = Double.Parse(textBoxResult.Text);
                operation_pressed = true;
                textBoxResult.Clear();
                textBoxResult.Text = "0";
                textBoxCalculations.Text = valueFirst + " " + operation;
            }
        }
        
        /// Obsługa przycisku "="
        /// wywołuje metode liczącą
        //
        private void buttonEquals_Click(object sender, EventArgs e)
        {
            calculate(operation);
        }


        ///Metoda obliczająca wyrażenie w zależności od operatora który jest w zmiennej operation
        ///jednorazowe kliknięcie "="  powoduje wyczyszczenie textBoxCalculations i przeniesienie wyniku
        ///działania do textBoxResult. Każde kolejne kliknięcie "=" powoduje że na aktualnej wartości valueFirst która
        ///zmienia się z każdym kliknięciem "=" wykonywana jest operacja ze zmiennej operation, drugim składnikiem
        ///działania jest stałą wartość valueSecond. Np 2+3=5, 5 zapisujemy do valueFirst a valueSecond zostaje 3
        ///następne kliknięcie "=" powoduje 5+3 itd. Metoda zadziała tylko jak nie został wyrzucony wyjątek.
        //
        private void calculate(String s)
        {
            if (!exception)
            {
                if (textBoxCalculations.Text.Length != 0)
                    valueSecond = Double.Parse(textBoxResult.Text);
                else
                    valueFirst = Double.Parse(textBoxResult.Text);
                textBoxCalculations.Clear();
                switch (s)
                {
                    case "+":
                        textBoxResult.Text = (valueFirst + valueSecond).ToString();
                        break;
                    case "-":
                        textBoxResult.Text = (valueFirst - valueSecond).ToString();
                        break;
                    case "*":
                        textBoxResult.Text = (valueFirst * valueSecond).ToString();
                        break;
                    case "/":
                        try
                        {
                            if (Double.Parse(textBoxResult.Text) == 0 && !exception)
                                throw new DivideByZeroException();
                            else
                                textBoxResult.Text = (valueFirst / valueSecond).ToString();
                        }
                        catch (DivideByZeroException)
                        {
                            textBoxResult.Text = "nie wolno dzielic przez 0";
                            exception = true;

                        }
                        break;
                    default:
                        break;
                }
                operation_pressed = false;
            }
        }
        ///Metoda do obsługi pierwiastkowania tak samo jak na kalkulatorze prostym
        ///najpierw podajemy liczbe a później pierwiastkujemy
        ///Metoda przechwytuje wyjątek który występuje podczas pierwiastkowania liczby ujemnej
        ///po wystąpieniu wyjątku oczekuje na Clear
        //
        private void buttonRoot_Click(object sender, EventArgs e)
        {
            if (!exception)
            {
                try
                {
                    if (Double.Parse(textBoxResult.Text) < 0)
                        throw new NotANumberException();
                    textBoxResult.Text = Math.Sqrt(Double.Parse(textBoxResult.Text)).ToString();
                }
                catch (NotANumberException)
                {
                    textBoxResult.Text = "Nie ma pierw z liczb ujemnych";
                    exception = true;
                }
            }
         }
        
        /// Metoda która czyści okienka i ustawia wartość w oknie Result na 0
        /// jeżeli wystąpił wyjątek to następuje reset.
        //
        private void buttonClear_Click(object sender, EventArgs e)
        {
            textBoxResult.Text = "0";
            textBoxCalculations.Clear();
            exception = false;
            operation = "";
            operation_pressed = false;
        }

        private void textBoxCalculations_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBoxResult_TextChanged(object sender, EventArgs e)
        {
            
        }

        
    }

    
}
