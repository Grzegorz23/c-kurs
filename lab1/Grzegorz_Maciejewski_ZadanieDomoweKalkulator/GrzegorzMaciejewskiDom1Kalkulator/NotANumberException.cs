﻿using System;
using System.Runtime.Serialization;

namespace GrzegorzMaciejewskiDom1Kalkulator
{
    [Serializable]
    internal class NotANumberException : Exception
    {
        public NotANumberException()
        {
        }

        public NotANumberException(string message) : base(message)
        {
        }

        public NotANumberException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected NotANumberException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}