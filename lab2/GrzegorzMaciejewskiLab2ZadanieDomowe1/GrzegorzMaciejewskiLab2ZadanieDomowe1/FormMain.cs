﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GrzegorzMaciejewskiLab2ZadanieDomowe1
{
    public partial class FormMain : Form
    {
        ///Deklaracja list, BindingList żeby można było łatwo usuwać elementy
        //
        public BindingList<Weapon> listOfWeapons;
        public BindingList<Armor> listOfArmors;
        public FormMain()
        {
            InitializeComponent();
            listOfArmors = new BindingList<Armor>();        //Inizjalizacja
            listOfWeapons = new BindingList<Weapon>();
        }
        ///Metoda wyświetlająca liste przedmiotów, wyświetli tylko jeżeli na liście coś jest
        //
        private void buttonShowItemList_Click(object sender, EventArgs e)
        {
            if (listOfArmors.Any() && listOfWeapons.Any())
            {
                dataGridViewArmorList.DataSource = null;
                dataGridViewWeaponList.DataSource = null;
                dataGridViewArmorList.DataSource = listOfArmors;
                dataGridViewWeaponList.DataSource = listOfWeapons;
            }
            else if(listOfArmors.Any())
            {
                dataGridViewArmorList.DataSource = null;
                dataGridViewArmorList.DataSource = listOfArmors;
            }
            else if(listOfWeapons.Any())
            {
                dataGridViewWeaponList.DataSource = null;
                dataGridViewWeaponList.DataSource = listOfWeapons;
            }
        }
        ///Kolejne dwie metody są przypisane do pól dataGridView i przy kliknięciu na jakiś rekord pokazują jego
        ///szczegółowy opis oraz zdjęcie jeżeli zostało dodane
        //
        public void ShowArmor(object sender, EventArgs e)
        {
            try
            {
                Armor temporaryArmor = listOfArmors[dataGridViewArmorList.SelectedRows[0].Index];
                textBoxItemDescription.Text = temporaryArmor.Description;
                pictureBoxItemPicture.Image = temporaryArmor.Image;
                System.Media.SoundPlayer epicItemSound = new System.Media.SoundPlayer(Properties.Resources.d3_legendary_sound);
                epicItemSound.Play();
            }
            catch(Exception ex)
            {
                MessageBox.Show("Nie ma takiego klikania");
            }
         }

        public void ShowWeapon(object sender,EventArgs e)
        {
            try
            { 
                Weapon temporaryWeapon = listOfWeapons[dataGridViewWeaponList.SelectedRows[0].Index];
                textBoxItemDescription.Text = temporaryWeapon.Description;
                pictureBoxItemPicture.Image = temporaryWeapon.Image;
                System.Media.SoundPlayer epicItemSound = new System.Media.SoundPlayer(Properties.Resources.d3_legendary_sound);
                epicItemSound.Play();
            }
            catch(Exception ex)
            {
                MessageBox.Show("Nie ma takiego klikania");
            }
        }
        ///Metoda obsługująca przycisk dodawania broni
        //
        private void buttonAddWeapon_Click(object sender, EventArgs e)
        {
            FormAddWeapon formAddWeapon = new FormAddWeapon();
            formAddWeapon.ShowDialog();
            if (formAddWeapon.weaponToAdd != null)
                listOfWeapons.Add(formAddWeapon.weaponToAdd);
        }
        ///Metoda obsługująca przycisk dodawania pancerzy
        //
        private void buttonAddArmor_Click(object sender, EventArgs e)
        {
            FormAddArmor formAddArmor = new FormAddArmor();
            formAddArmor.ShowDialog();
            if (formAddArmor.armorToAdd != null)
                listOfArmors.Add(formAddArmor.armorToAdd);

        }
        ///Metoda która usuwa dany przedmiot z listy, przedmiot musi być zaznaczony, czyści pole z obrazkiem i opisem
        //
        private void buttonDeleteItem_Click(object sender, EventArgs e)
        {
            if (dataGridViewArmorList.SelectedRows.Count>0)
            {
               dataGridViewArmorList.Rows.RemoveAt(dataGridViewArmorList.SelectedRows[0].Index);
                pictureBoxItemPicture.Image = null;
                textBoxItemDescription.Text = "";
            }
            if(dataGridViewWeaponList.SelectedRows.Count>0)
            {
                dataGridViewWeaponList.Rows.RemoveAt(dataGridViewWeaponList.SelectedRows[0].Index);
                pictureBoxItemPicture.Image = null;
                textBoxItemDescription.Text = "";
            }
        }
        ///Kolejne dwie metody służą do binarnego zapisywania naszych epickich rzeczy do pliku w wybranym
        ///przez nas miejscu. Żeby wybrać satysfakcjonujące miejsce używam najpierw folderDialog a przy odczycie
        ///OpenFileDialog
        //
        private void buttonSave_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderDialog = new FolderBrowserDialog();
                folderDialog.ShowDialog();
            String pathToSave = folderDialog.SelectedPath;
            if (pathToSave != "")
                using (System.IO.Stream streamToSave = System.IO.File.Open((pathToSave + "\\Lista_Przedmiotow.bin"), System.IO.FileMode.Create))
            {
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                binaryFormatter.Serialize(streamToSave, listOfWeapons);
                binaryFormatter.Serialize(streamToSave, listOfArmors);
                }
        }
        ///Metoda odcztująca zawartość wskazanego przez nas pliku binarnego UWAGA musi to być plik z rzeczami
        ///do których odczytu stworzony został program
        //
        private void buttonLoad_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.ShowDialog();
            String pathToLoad = openFileDialog.FileName;
            if (pathToLoad != "")
                using (System.IO.Stream streamToLoad = System.IO.File.Open((pathToLoad), System.IO.FileMode.Open))
                {
                    BinaryFormatter binaryFormatter = new BinaryFormatter();
                    listOfWeapons= (BindingList<Weapon>)binaryFormatter.Deserialize(streamToLoad);
                    listOfArmors = (BindingList<Armor>)binaryFormatter.Deserialize(streamToLoad);
                }  
        }

        
    }
}
