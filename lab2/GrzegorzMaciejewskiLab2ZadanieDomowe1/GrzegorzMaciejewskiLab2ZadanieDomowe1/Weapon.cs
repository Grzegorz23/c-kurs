﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace GrzegorzMaciejewskiLab2ZadanieDomowe1
{
    [Serializable]
    public class Weapon : Item
    {   
        ///klasa dziedzicząca klase Item, opisuje bronie
        //
        public int Damage { get; set; }
        ///konstruktory z parametrami
        //
        public Weapon(String name, Image image,String description,int damage):base(name,image, description)
        {
            Damage = damage;
        }
        public Weapon(String name,String description, int damage) : base(name, description)
        {
            Damage = damage;
        }
    }
}
