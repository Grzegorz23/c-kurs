﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrzegorzMaciejewskiLab2ZadanieDomowe1
{
    [Serializable]
    public class Item
    {  
        ///Zmienne klasy matki, nazwa, obrazek i opis przedmiotu
        //
        public String Name { get; set; }
        public Image Image;
        public String Description;
       
        ///konstruktory z paramerami
        //
        public Item(String name,Image image,String description)
        {
            this.Name = name;
            Image = image;
            Description = description;
        }
        ///konstruktor który w razie braku obrazka przydziela obrazek brak obrazka
        //
        public Item(String name, String description)
        {
            this.Name = name;
            Image = Image.FromFile(System.IO.Path.GetFullPath(@"..\\..\\") + "Pictures\\brakObrazka.jpg");
            Description = description;
        }
    }
}
