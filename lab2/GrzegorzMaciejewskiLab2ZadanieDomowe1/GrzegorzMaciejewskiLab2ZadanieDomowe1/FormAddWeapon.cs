﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GrzegorzMaciejewskiLab2ZadanieDomowe1
{
    public partial class FormAddWeapon : Form
    {
        ///zmienna pomocnicza do przekazywania przedmiotu do FormMain
        //
        public Weapon weaponToAdd = null;
      

        public FormAddWeapon()
        {
            InitializeComponent();
            textBoxImagePath.Text =""; // deklaracje zabezpieczające przed dodaniem zdęcia bez ścieżki lub nazwy
            textBoxWeaponName.Text ="";
        }
        ///Obsługa zatwierdzenia dodawania naszej broni, można się mylić, i wprowadzać ile się chce
        //
        private void buttonOK_Click(object sender, EventArgs e)
        {
            try
            {
                if (textBoxImagePath.Text != "")
                    weaponToAdd = new Weapon(textBoxWeaponName.Text, Image.FromFile(Path.GetFullPath(@"..\\..\\") + "Pictures\\" + textBoxWeaponName.Text + ".jpg"), textBoxWeaponDescription.Text, Int32.Parse(textBoxWeaponDamage.Text));
                else
                    weaponToAdd = new Weapon(textBoxWeaponName.Text, textBoxWeaponDescription.Text, Int32.Parse(textBoxWeaponDamage.Text));
                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Nieprawidłowo wprowadzone dane");
            }
        }
        ///anulowanie
        //
        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
        ///wybieranie ścieżki z której ma zostać pobrane zdjęcie broni
        //
        private void buttonChoosePath_Click(object sender, EventArgs e)
        {
            try
            {
                if (textBoxWeaponName.Text == "")
                    MessageBox.Show("Podaj najpierw nazwe przedmiotu");
                else
                {
                    OpenFileDialog openFileDialog = new OpenFileDialog();
                    openFileDialog.ShowDialog();
                    String pathToImage = openFileDialog.FileName;
                    textBoxImagePath.Text = pathToImage;
                    File.Copy(pathToImage, Path.GetFullPath(@"..\\..\\") + "Pictures\\" + textBoxWeaponName.Text + ".jpg");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Niepoprawna ścieżka do pliku");
            }
        }
    }
}
