﻿namespace GrzegorzMaciejewskiLab2ZadanieDomowe1
{
    partial class FormAddWeapon
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelWeaponName = new System.Windows.Forms.Label();
            this.labelDamage = new System.Windows.Forms.Label();
            this.labelImagePath = new System.Windows.Forms.Label();
            this.labelWeaponDescription = new System.Windows.Forms.Label();
            this.buttonOK = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.textBoxWeaponDescription = new System.Windows.Forms.TextBox();
            this.textBoxWeaponName = new System.Windows.Forms.TextBox();
            this.textBoxWeaponDamage = new System.Windows.Forms.TextBox();
            this.textBoxImagePath = new System.Windows.Forms.TextBox();
            this.buttonChoosePath = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelWeaponName
            // 
            this.labelWeaponName.AutoSize = true;
            this.labelWeaponName.Font = new System.Drawing.Font("Monotype Corsiva", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelWeaponName.Location = new System.Drawing.Point(9, 45);
            this.labelWeaponName.Name = "labelWeaponName";
            this.labelWeaponName.Size = new System.Drawing.Size(83, 18);
            this.labelWeaponName.TabIndex = 0;
            this.labelWeaponName.Text = "Nazwa broni";
            // 
            // labelDamage
            // 
            this.labelDamage.AutoSize = true;
            this.labelDamage.Font = new System.Drawing.Font("Monotype Corsiva", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelDamage.Location = new System.Drawing.Point(9, 79);
            this.labelDamage.Name = "labelDamage";
            this.labelDamage.Size = new System.Drawing.Size(82, 18);
            this.labelDamage.TabIndex = 1;
            this.labelDamage.Text = "Ilość obrażeń";
            // 
            // labelImagePath
            // 
            this.labelImagePath.AutoSize = true;
            this.labelImagePath.Font = new System.Drawing.Font("Monotype Corsiva", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelImagePath.Location = new System.Drawing.Point(9, 115);
            this.labelImagePath.Name = "labelImagePath";
            this.labelImagePath.Size = new System.Drawing.Size(112, 18);
            this.labelImagePath.TabIndex = 2;
            this.labelImagePath.Text = "Ścieżka do zdjęcia";
            // 
            // labelWeaponDescription
            // 
            this.labelWeaponDescription.AutoSize = true;
            this.labelWeaponDescription.Font = new System.Drawing.Font("Modern No. 20", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelWeaponDescription.Location = new System.Drawing.Point(428, 9);
            this.labelWeaponDescription.Name = "labelWeaponDescription";
            this.labelWeaponDescription.Size = new System.Drawing.Size(48, 21);
            this.labelWeaponDescription.TabIndex = 3;
            this.labelWeaponDescription.Text = "Opis";
            // 
            // buttonOK
            // 
            this.buttonOK.BackColor = System.Drawing.Color.DarkRed;
            this.buttonOK.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.buttonOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonOK.Font = new System.Drawing.Font("Microsoft YaHei UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonOK.ForeColor = System.Drawing.Color.Orange;
            this.buttonOK.Location = new System.Drawing.Point(37, 234);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(93, 45);
            this.buttonOK.TabIndex = 4;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = false;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.BackColor = System.Drawing.Color.DarkRed;
            this.buttonCancel.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.buttonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCancel.Font = new System.Drawing.Font("Microsoft YaHei UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonCancel.ForeColor = System.Drawing.Color.Orange;
            this.buttonCancel.Location = new System.Drawing.Point(136, 234);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(96, 45);
            this.buttonCancel.TabIndex = 5;
            this.buttonCancel.Text = "Anuluj";
            this.buttonCancel.UseVisualStyleBackColor = false;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // textBoxWeaponDescription
            // 
            this.textBoxWeaponDescription.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.textBoxWeaponDescription.Location = new System.Drawing.Point(291, 36);
            this.textBoxWeaponDescription.Multiline = true;
            this.textBoxWeaponDescription.Name = "textBoxWeaponDescription";
            this.textBoxWeaponDescription.Size = new System.Drawing.Size(328, 255);
            this.textBoxWeaponDescription.TabIndex = 6;
            // 
            // textBoxWeaponName
            // 
            this.textBoxWeaponName.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.textBoxWeaponName.Location = new System.Drawing.Point(127, 43);
            this.textBoxWeaponName.Name = "textBoxWeaponName";
            this.textBoxWeaponName.Size = new System.Drawing.Size(100, 20);
            this.textBoxWeaponName.TabIndex = 7;
            // 
            // textBoxWeaponDamage
            // 
            this.textBoxWeaponDamage.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.textBoxWeaponDamage.Location = new System.Drawing.Point(127, 77);
            this.textBoxWeaponDamage.Name = "textBoxWeaponDamage";
            this.textBoxWeaponDamage.Size = new System.Drawing.Size(100, 20);
            this.textBoxWeaponDamage.TabIndex = 8;
            // 
            // textBoxImagePath
            // 
            this.textBoxImagePath.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.textBoxImagePath.Location = new System.Drawing.Point(127, 113);
            this.textBoxImagePath.Name = "textBoxImagePath";
            this.textBoxImagePath.Size = new System.Drawing.Size(100, 20);
            this.textBoxImagePath.TabIndex = 9;
            // 
            // buttonChoosePath
            // 
            this.buttonChoosePath.BackColor = System.Drawing.Color.DarkRed;
            this.buttonChoosePath.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.buttonChoosePath.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonChoosePath.Font = new System.Drawing.Font("Microsoft YaHei UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonChoosePath.ForeColor = System.Drawing.Color.Orange;
            this.buttonChoosePath.Location = new System.Drawing.Point(152, 139);
            this.buttonChoosePath.Name = "buttonChoosePath";
            this.buttonChoosePath.Size = new System.Drawing.Size(75, 27);
            this.buttonChoosePath.TabIndex = 10;
            this.buttonChoosePath.Text = "wybierz";
            this.buttonChoosePath.UseVisualStyleBackColor = false;
            this.buttonChoosePath.Click += new System.EventHandler(this.buttonChoosePath_Click);
            // 
            // FormAddWeapon
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.ClientSize = new System.Drawing.Size(645, 323);
            this.Controls.Add(this.buttonChoosePath);
            this.Controls.Add(this.textBoxImagePath);
            this.Controls.Add(this.textBoxWeaponDamage);
            this.Controls.Add(this.textBoxWeaponName);
            this.Controls.Add(this.textBoxWeaponDescription);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.labelWeaponDescription);
            this.Controls.Add(this.labelImagePath);
            this.Controls.Add(this.labelDamage);
            this.Controls.Add(this.labelWeaponName);
            this.Name = "FormAddWeapon";
            this.Text = "FormAddWeapon";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelWeaponName;
        private System.Windows.Forms.Label labelDamage;
        private System.Windows.Forms.Label labelImagePath;
        private System.Windows.Forms.Label labelWeaponDescription;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.TextBox textBoxWeaponDescription;
        private System.Windows.Forms.TextBox textBoxWeaponName;
        private System.Windows.Forms.TextBox textBoxWeaponDamage;
        private System.Windows.Forms.TextBox textBoxImagePath;
        private System.Windows.Forms.Button buttonChoosePath;
    }
}