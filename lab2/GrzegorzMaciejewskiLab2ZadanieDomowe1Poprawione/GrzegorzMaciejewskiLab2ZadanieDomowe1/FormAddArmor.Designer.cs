﻿namespace GrzegorzMaciejewskiLab2ZadanieDomowe1
{
    partial class FormAddArmor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonOK = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.labelArmorName = new System.Windows.Forms.Label();
            this.labelArmorDescription = new System.Windows.Forms.Label();
            this.labelDurability = new System.Windows.Forms.Label();
            this.textBoxArmorDescription = new System.Windows.Forms.TextBox();
            this.textBoxArmorName = new System.Windows.Forms.TextBox();
            this.textBoxArmorDurability = new System.Windows.Forms.TextBox();
            this.textBoxImagePath = new System.Windows.Forms.TextBox();
            this.labelImagePath = new System.Windows.Forms.Label();
            this.buttonChoosePath = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonOK
            // 
            this.buttonOK.BackColor = System.Drawing.Color.Maroon;
            this.buttonOK.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonOK.Font = new System.Drawing.Font("Microsoft YaHei UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonOK.ForeColor = System.Drawing.Color.Orange;
            this.buttonOK.Location = new System.Drawing.Point(62, 231);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(87, 45);
            this.buttonOK.TabIndex = 0;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = false;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.BackColor = System.Drawing.Color.Maroon;
            this.buttonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonCancel.Font = new System.Drawing.Font("Microsoft YaHei UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonCancel.ForeColor = System.Drawing.Color.Orange;
            this.buttonCancel.Location = new System.Drawing.Point(178, 231);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(90, 45);
            this.buttonCancel.TabIndex = 1;
            this.buttonCancel.Text = "Anuluj";
            this.buttonCancel.UseVisualStyleBackColor = false;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // labelArmorName
            // 
            this.labelArmorName.AutoSize = true;
            this.labelArmorName.Font = new System.Drawing.Font("Monotype Corsiva", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelArmorName.Location = new System.Drawing.Point(25, 63);
            this.labelArmorName.Name = "labelArmorName";
            this.labelArmorName.Size = new System.Drawing.Size(50, 18);
            this.labelArmorName.TabIndex = 2;
            this.labelArmorName.Text = "Nazwa";
            // 
            // labelArmorDescription
            // 
            this.labelArmorDescription.AutoSize = true;
            this.labelArmorDescription.Font = new System.Drawing.Font("Microsoft YaHei UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelArmorDescription.Location = new System.Drawing.Point(486, 9);
            this.labelArmorDescription.Name = "labelArmorDescription";
            this.labelArmorDescription.Size = new System.Drawing.Size(46, 20);
            this.labelArmorDescription.TabIndex = 3;
            this.labelArmorDescription.Text = "Opis";
            // 
            // labelDurability
            // 
            this.labelDurability.AutoSize = true;
            this.labelDurability.Font = new System.Drawing.Font("Monotype Corsiva", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelDurability.Location = new System.Drawing.Point(25, 90);
            this.labelDurability.Name = "labelDurability";
            this.labelDurability.Size = new System.Drawing.Size(90, 18);
            this.labelDurability.TabIndex = 4;
            this.labelDurability.Text = "Wytrzymałosc";
            // 
            // textBoxArmorDescription
            // 
            this.textBoxArmorDescription.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.textBoxArmorDescription.Location = new System.Drawing.Point(338, 40);
            this.textBoxArmorDescription.Multiline = true;
            this.textBoxArmorDescription.Name = "textBoxArmorDescription";
            this.textBoxArmorDescription.Size = new System.Drawing.Size(342, 273);
            this.textBoxArmorDescription.TabIndex = 5;
            // 
            // textBoxArmorName
            // 
            this.textBoxArmorName.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.textBoxArmorName.Location = new System.Drawing.Point(153, 56);
            this.textBoxArmorName.Name = "textBoxArmorName";
            this.textBoxArmorName.Size = new System.Drawing.Size(131, 20);
            this.textBoxArmorName.TabIndex = 6;
            // 
            // textBoxArmorDurability
            // 
            this.textBoxArmorDurability.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.textBoxArmorDurability.Location = new System.Drawing.Point(153, 87);
            this.textBoxArmorDurability.Name = "textBoxArmorDurability";
            this.textBoxArmorDurability.Size = new System.Drawing.Size(131, 20);
            this.textBoxArmorDurability.TabIndex = 7;
            // 
            // textBoxImagePath
            // 
            this.textBoxImagePath.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.textBoxImagePath.Location = new System.Drawing.Point(153, 118);
            this.textBoxImagePath.Name = "textBoxImagePath";
            this.textBoxImagePath.Size = new System.Drawing.Size(131, 20);
            this.textBoxImagePath.TabIndex = 8;
            // 
            // labelImagePath
            // 
            this.labelImagePath.AutoSize = true;
            this.labelImagePath.Font = new System.Drawing.Font("Monotype Corsiva", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelImagePath.Location = new System.Drawing.Point(25, 121);
            this.labelImagePath.Name = "labelImagePath";
            this.labelImagePath.Size = new System.Drawing.Size(112, 18);
            this.labelImagePath.TabIndex = 9;
            this.labelImagePath.Text = "Ścieżka do zdjęcia";
            // 
            // buttonChoosePath
            // 
            this.buttonChoosePath.BackColor = System.Drawing.Color.Maroon;
            this.buttonChoosePath.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonChoosePath.Font = new System.Drawing.Font("Microsoft YaHei UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonChoosePath.ForeColor = System.Drawing.Color.Orange;
            this.buttonChoosePath.Location = new System.Drawing.Point(209, 153);
            this.buttonChoosePath.Name = "buttonChoosePath";
            this.buttonChoosePath.Size = new System.Drawing.Size(75, 23);
            this.buttonChoosePath.TabIndex = 10;
            this.buttonChoosePath.Text = "wybierz";
            this.buttonChoosePath.UseVisualStyleBackColor = false;
            this.buttonChoosePath.Click += new System.EventHandler(this.buttonChoosePath_Click);
            // 
            // FormAddArmor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.ClientSize = new System.Drawing.Size(692, 339);
            this.Controls.Add(this.buttonChoosePath);
            this.Controls.Add(this.labelImagePath);
            this.Controls.Add(this.textBoxImagePath);
            this.Controls.Add(this.textBoxArmorDurability);
            this.Controls.Add(this.textBoxArmorName);
            this.Controls.Add(this.textBoxArmorDescription);
            this.Controls.Add(this.labelDurability);
            this.Controls.Add(this.labelArmorDescription);
            this.Controls.Add(this.labelArmorName);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOK);
            this.Name = "FormAddArmor";
            this.Text = "FormAddArmor";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Label labelArmorName;
        private System.Windows.Forms.Label labelArmorDescription;
        private System.Windows.Forms.Label labelDurability;
        private System.Windows.Forms.TextBox textBoxArmorDescription;
        private System.Windows.Forms.TextBox textBoxArmorName;
        private System.Windows.Forms.TextBox textBoxArmorDurability;
        private System.Windows.Forms.TextBox textBoxImagePath;
        private System.Windows.Forms.Label labelImagePath;
        private System.Windows.Forms.Button buttonChoosePath;
    }
}