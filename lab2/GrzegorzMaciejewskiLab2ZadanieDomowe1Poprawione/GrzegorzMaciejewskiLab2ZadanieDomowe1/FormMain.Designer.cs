﻿namespace GrzegorzMaciejewskiLab2ZadanieDomowe1
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.labelListName = new System.Windows.Forms.Label();
            this.labelItemDecscription = new System.Windows.Forms.Label();
            this.dataGridViewWeaponList = new System.Windows.Forms.DataGridView();
            this.textBoxItemDescription = new System.Windows.Forms.TextBox();
            this.pictureBoxItemPicture = new System.Windows.Forms.PictureBox();
            this.buttonAddWeapon = new System.Windows.Forms.Button();
            this.buttonDeleteItem = new System.Windows.Forms.Button();
            this.buttonSave = new System.Windows.Forms.Button();
            this.buttonLoad = new System.Windows.Forms.Button();
            this.buttonShowItemList = new System.Windows.Forms.Button();
            this.dataGridViewArmorList = new System.Windows.Forms.DataGridView();
            this.buttonAddArmor = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewWeaponList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxItemPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewArmorList)).BeginInit();
            this.SuspendLayout();
            // 
            // labelListName
            // 
            this.labelListName.AutoSize = true;
            this.labelListName.BackColor = System.Drawing.Color.Transparent;
            this.labelListName.Font = new System.Drawing.Font("Perpetua Titling MT", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelListName.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.labelListName.Location = new System.Drawing.Point(31, 9);
            this.labelListName.Name = "labelListName";
            this.labelListName.Size = new System.Drawing.Size(199, 26);
            this.labelListName.TabIndex = 0;
            this.labelListName.Text = "Lista Epickości";
            // 
            // labelItemDecscription
            // 
            this.labelItemDecscription.AutoSize = true;
            this.labelItemDecscription.BackColor = System.Drawing.Color.Transparent;
            this.labelItemDecscription.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.labelItemDecscription.Font = new System.Drawing.Font("Perpetua Titling MT", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelItemDecscription.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.labelItemDecscription.Location = new System.Drawing.Point(379, 9);
            this.labelItemDecscription.Name = "labelItemDecscription";
            this.labelItemDecscription.Size = new System.Drawing.Size(61, 26);
            this.labelItemDecscription.TabIndex = 1;
            this.labelItemDecscription.Text = "Opis";
            // 
            // dataGridViewWeaponList
            // 
            this.dataGridViewWeaponList.BackgroundColor = System.Drawing.Color.Orange;
            this.dataGridViewWeaponList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewWeaponList.Location = new System.Drawing.Point(12, 53);
            this.dataGridViewWeaponList.Name = "dataGridViewWeaponList";
            this.dataGridViewWeaponList.Size = new System.Drawing.Size(240, 182);
            this.dataGridViewWeaponList.TabIndex = 2;
            this.dataGridViewWeaponList.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.ShowWeapon);
            this.dataGridViewWeaponList.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.ShowWeapon);
            // 
            // textBoxItemDescription
            // 
            this.textBoxItemDescription.BackColor = System.Drawing.Color.Goldenrod;
            this.textBoxItemDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxItemDescription.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxItemDescription.Location = new System.Drawing.Point(258, 53);
            this.textBoxItemDescription.Multiline = true;
            this.textBoxItemDescription.Name = "textBoxItemDescription";
            this.textBoxItemDescription.Size = new System.Drawing.Size(374, 182);
            this.textBoxItemDescription.TabIndex = 3;
            // 
            // pictureBoxItemPicture
            // 
            this.pictureBoxItemPicture.BackColor = System.Drawing.Color.Transparent;
            this.pictureBoxItemPicture.Location = new System.Drawing.Point(258, 241);
            this.pictureBoxItemPicture.Name = "pictureBoxItemPicture";
            this.pictureBoxItemPicture.Size = new System.Drawing.Size(369, 191);
            this.pictureBoxItemPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxItemPicture.TabIndex = 4;
            this.pictureBoxItemPicture.TabStop = false;
            // 
            // buttonAddWeapon
            // 
            this.buttonAddWeapon.BackColor = System.Drawing.Color.LightGray;
            this.buttonAddWeapon.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonAddWeapon.Font = new System.Drawing.Font("Modern No. 20", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAddWeapon.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.buttonAddWeapon.Location = new System.Drawing.Point(638, 53);
            this.buttonAddWeapon.Name = "buttonAddWeapon";
            this.buttonAddWeapon.Size = new System.Drawing.Size(90, 58);
            this.buttonAddWeapon.TabIndex = 5;
            this.buttonAddWeapon.Text = "Dodaj Bron";
            this.buttonAddWeapon.UseVisualStyleBackColor = false;
            this.buttonAddWeapon.Click += new System.EventHandler(this.buttonAddWeapon_Click);
            // 
            // buttonDeleteItem
            // 
            this.buttonDeleteItem.BackColor = System.Drawing.Color.LightGray;
            this.buttonDeleteItem.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonDeleteItem.Font = new System.Drawing.Font("Modern No. 20", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDeleteItem.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.buttonDeleteItem.Location = new System.Drawing.Point(638, 112);
            this.buttonDeleteItem.Name = "buttonDeleteItem";
            this.buttonDeleteItem.Size = new System.Drawing.Size(181, 58);
            this.buttonDeleteItem.TabIndex = 6;
            this.buttonDeleteItem.Text = "Usun Przedmiot";
            this.buttonDeleteItem.UseVisualStyleBackColor = false;
            this.buttonDeleteItem.Click += new System.EventHandler(this.buttonDeleteItem_Click);
            // 
            // buttonSave
            // 
            this.buttonSave.BackColor = System.Drawing.Color.LightGray;
            this.buttonSave.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonSave.Font = new System.Drawing.Font("Modern No. 20", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSave.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.buttonSave.Location = new System.Drawing.Point(638, 241);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(90, 53);
            this.buttonSave.TabIndex = 7;
            this.buttonSave.Text = "Zapisz";
            this.buttonSave.UseVisualStyleBackColor = false;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // buttonLoad
            // 
            this.buttonLoad.BackColor = System.Drawing.Color.LightGray;
            this.buttonLoad.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonLoad.Font = new System.Drawing.Font("Modern No. 20", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonLoad.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.buttonLoad.Location = new System.Drawing.Point(729, 241);
            this.buttonLoad.Name = "buttonLoad";
            this.buttonLoad.Size = new System.Drawing.Size(90, 53);
            this.buttonLoad.TabIndex = 8;
            this.buttonLoad.Text = "Wczytaj";
            this.buttonLoad.UseVisualStyleBackColor = false;
            this.buttonLoad.Click += new System.EventHandler(this.buttonLoad_Click);
            // 
            // buttonShowItemList
            // 
            this.buttonShowItemList.BackColor = System.Drawing.Color.LightGray;
            this.buttonShowItemList.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonShowItemList.Font = new System.Drawing.Font("Modern No. 20", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonShowItemList.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.buttonShowItemList.Location = new System.Drawing.Point(638, 176);
            this.buttonShowItemList.Name = "buttonShowItemList";
            this.buttonShowItemList.Size = new System.Drawing.Size(181, 59);
            this.buttonShowItemList.TabIndex = 9;
            this.buttonShowItemList.Text = "Aktualizuj Liste Przedmiotow";
            this.buttonShowItemList.UseVisualStyleBackColor = false;
            this.buttonShowItemList.Click += new System.EventHandler(this.buttonShowItemList_Click);
            // 
            // dataGridViewArmorList
            // 
            this.dataGridViewArmorList.BackgroundColor = System.Drawing.Color.Orange;
            this.dataGridViewArmorList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewArmorList.Location = new System.Drawing.Point(12, 241);
            this.dataGridViewArmorList.Name = "dataGridViewArmorList";
            this.dataGridViewArmorList.Size = new System.Drawing.Size(240, 191);
            this.dataGridViewArmorList.TabIndex = 10;
            this.dataGridViewArmorList.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.ShowArmor);
            // 
            // buttonAddArmor
            // 
            this.buttonAddArmor.BackColor = System.Drawing.Color.LightGray;
            this.buttonAddArmor.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonAddArmor.Font = new System.Drawing.Font("Modern No. 20", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAddArmor.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.buttonAddArmor.Location = new System.Drawing.Point(729, 53);
            this.buttonAddArmor.Name = "buttonAddArmor";
            this.buttonAddArmor.Size = new System.Drawing.Size(90, 58);
            this.buttonAddArmor.TabIndex = 11;
            this.buttonAddArmor.Text = "Dodaj Pancerz";
            this.buttonAddArmor.UseVisualStyleBackColor = false;
            this.buttonAddArmor.Click += new System.EventHandler(this.buttonAddArmor_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.MenuBar;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(831, 444);
            this.Controls.Add(this.buttonAddArmor);
            this.Controls.Add(this.dataGridViewArmorList);
            this.Controls.Add(this.buttonShowItemList);
            this.Controls.Add(this.buttonLoad);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.buttonDeleteItem);
            this.Controls.Add(this.buttonAddWeapon);
            this.Controls.Add(this.pictureBoxItemPicture);
            this.Controls.Add(this.textBoxItemDescription);
            this.Controls.Add(this.dataGridViewWeaponList);
            this.Controls.Add(this.labelItemDecscription);
            this.Controls.Add(this.labelListName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "FormMain";
            this.Text = "Baza Epickości";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewWeaponList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxItemPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewArmorList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelListName;
        private System.Windows.Forms.Label labelItemDecscription;
        private System.Windows.Forms.DataGridView dataGridViewWeaponList;
        private System.Windows.Forms.TextBox textBoxItemDescription;
        private System.Windows.Forms.PictureBox pictureBoxItemPicture;
        private System.Windows.Forms.Button buttonAddWeapon;
        private System.Windows.Forms.Button buttonDeleteItem;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Button buttonLoad;
        private System.Windows.Forms.Button buttonShowItemList;
        private System.Windows.Forms.DataGridView dataGridViewArmorList;
        private System.Windows.Forms.Button buttonAddArmor;
    }
}

