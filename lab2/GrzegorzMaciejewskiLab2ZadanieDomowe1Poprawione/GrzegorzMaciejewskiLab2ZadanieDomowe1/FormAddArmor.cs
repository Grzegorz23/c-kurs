﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GrzegorzMaciejewskiLab2ZadanieDomowe1
{
    public partial class FormAddArmor : Form
    {   
        ///zmienna pomocnicza do przekazania przedmiotu do FormMain
        //
        public Armor armorToAdd=null;
        public FormAddArmor()
        {
            InitializeComponent();
            textBoxImagePath.Text = "";// deklaracje zabezpieczające przed dodaniem zdęcia bez ścieżki lub nazwy
            textBoxArmorName.Text = "";
        }
        ///Wybieranie ścieżi do zdjęcia przedmiotu, każde zdjęcie jest kopiowane do folderu pictures
        //
        private void buttonChoosePath_Click(object sender, EventArgs e)
        {
            try
            {
                if (textBoxArmorName.Text == "")
                MessageBox.Show("Podaj najpierw nazwe przedmiotu"); // ponieważ kopia zdjęcia nazywana jest tak jak przedmiot
                    else
                {
                    OpenFileDialog openFileDialog = new OpenFileDialog();
                    openFileDialog.ShowDialog();
                    String pathToImage = openFileDialog.FileName;
                    textBoxImagePath.Text = pathToImage;
                    File.Copy(pathToImage, Path.GetFullPath(@"..\\..\\")+"Pictures\\"+ textBoxArmorName.Text +".jpg");
                }
             }
            catch(Exception ex)
            {
                MessageBox.Show("Niepoprawna ścieżka do pliku");
            }
        }
        ///Metoda zatwiedzająca dodawany przedmiot, w razie pomyłki wyrzuca wyjątek i umożliwia poprawe
        //
        private void buttonOK_Click(object sender, EventArgs e)
        {
            try
            {
                
                if (textBoxImagePath.Text != "")
                    armorToAdd = new Armor(textBoxArmorName.Text, Image.FromFile(Path.GetFullPath(@"..\\..\\")+"Pictures\\" + textBoxArmorName.Text + ".jpg"), textBoxArmorDescription.Text, Int32.Parse(textBoxArmorDurability.Text));
                else
                    armorToAdd = new Armor(textBoxArmorName.Text, textBoxArmorDescription.Text, Int32.Parse(textBoxArmorDurability.Text));
                Close();
            }
            catch(Exception ex)
            {
                MessageBox.Show("Nieprawidłowo wprowadzone dane");
            }
         }
        ///Cancel, zamyka okno dialogowe
        //
        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
