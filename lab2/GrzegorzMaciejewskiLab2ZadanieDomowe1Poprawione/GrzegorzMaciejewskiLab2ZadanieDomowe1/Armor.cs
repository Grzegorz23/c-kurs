﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace GrzegorzMaciejewskiLab2ZadanieDomowe1
{
    [Serializable]// aby można było wykonywać zapis do pliku
    public class Armor:Item
    {
        ///klasa dziedzicząca po klasie Item, opisuje pancerze
        //
        public int Durability { get; set; }
        ///konstruktory z parametrami
        //
        public Armor(String name, Image image, String description, int durability):base(name,image, description)
        {
            Durability = durability;
        }
        public Armor(String name, String description, int durability) : base(name,description)
        {
            Durability = durability;
        }
    }
}
