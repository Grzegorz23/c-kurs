﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrzegorzMaciejewskiLab2ZadanieDomowe1
{
    [Serializable] // aby można było wykonywać zapis do pliku
    public class Item
    {  
        ///Zmienne klasy matki, nazwa, obrazek i opis przedmiotu
        //
        public String Name { get; set; }
        public Image image;
        public String description;
       
        ///konstruktory z paramerami
        //
        public Item(String name,Image image,String description)
        {
            this.Name = name;
            this.image = image;
            this.description = description;
        }
        ///konstruktor który w razie braku obrazka przydziela obrazek brak obrazka
        //
        public Item(String name, String description)
        {
            this.Name = name;
            image = Image.FromFile(System.IO.Path.GetFullPath(@"..\\..\\") + "Pictures\\brakObrazka.jpg");
            this.description = description;
        }
    }
}
