﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GrzegorzMaciejewskiLab2
{
    public partial class FormAddGrade : Form
    {
        /// zmienna za pomocą które przekażemy nową ocene odpowiedniej osobie do FormMain
        //
        public Grade gradeToAdd = null;
        public FormAddGrade()
        {
            InitializeComponent();
        }
        /// Metoda która zatwierdza bądż odrzuca ocenę którą chcemy dodać 
        //
        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                gradeToAdd = new Grade(Double.Parse(textBoxGradeValue.Text), textBoxGradeName.Text);
            }
            catch (Exception exc)
            {
                MessageBox.Show("Za mało danych, lub nieprawidłowe dane.");
            }
            Close();
        }
        /// Zamykadełko
        //
        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }

     
    
}
