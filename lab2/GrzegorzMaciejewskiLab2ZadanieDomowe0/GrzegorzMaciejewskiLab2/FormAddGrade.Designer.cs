﻿namespace GrzegorzMaciejewskiLab2
{
    partial class FormAddGrade
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonOk = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.labelGradeValue = new System.Windows.Forms.Label();
            this.labelGradeName = new System.Windows.Forms.Label();
            this.textBoxGradeValue = new System.Windows.Forms.TextBox();
            this.textBoxGradeName = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // buttonOk
            // 
            this.buttonOk.Location = new System.Drawing.Point(29, 185);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(92, 40);
            this.buttonOk.TabIndex = 0;
            this.buttonOk.Text = "OK";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(155, 185);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(88, 40);
            this.buttonCancel.TabIndex = 1;
            this.buttonCancel.Text = "Anuluj";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // labelGradeValue
            // 
            this.labelGradeValue.AutoSize = true;
            this.labelGradeValue.Font = new System.Drawing.Font("OCR A Extended", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelGradeValue.Location = new System.Drawing.Point(3, 48);
            this.labelGradeValue.Name = "labelGradeValue";
            this.labelGradeValue.Size = new System.Drawing.Size(146, 20);
            this.labelGradeValue.TabIndex = 3;
            this.labelGradeValue.Text = "Wartość oceny";
            // 
            // labelGradeName
            // 
            this.labelGradeName.AutoSize = true;
            this.labelGradeName.Font = new System.Drawing.Font("OCR A Extended", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelGradeName.Location = new System.Drawing.Point(3, 114);
            this.labelGradeName.Name = "labelGradeName";
            this.labelGradeName.Size = new System.Drawing.Size(130, 20);
            this.labelGradeName.TabIndex = 4;
            this.labelGradeName.Text = "Nazwa oceny";
            // 
            // textBoxGradeValue
            // 
            this.textBoxGradeValue.Location = new System.Drawing.Point(155, 48);
            this.textBoxGradeValue.Name = "textBoxGradeValue";
            this.textBoxGradeValue.Size = new System.Drawing.Size(100, 20);
            this.textBoxGradeValue.TabIndex = 5;
            // 
            // textBoxGradeName
            // 
            this.textBoxGradeName.Location = new System.Drawing.Point(155, 114);
            this.textBoxGradeName.Name = "textBoxGradeName";
            this.textBoxGradeName.Size = new System.Drawing.Size(100, 20);
            this.textBoxGradeName.TabIndex = 6;
            // 
            // FormAddGrade
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.textBoxGradeName);
            this.Controls.Add(this.textBoxGradeValue);
            this.Controls.Add(this.labelGradeName);
            this.Controls.Add(this.labelGradeValue);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOk);
            this.Name = "FormAddGrade";
            this.Text = "FormAddGrade";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Label labelGradeValue;
        private System.Windows.Forms.Label labelGradeName;
        private System.Windows.Forms.TextBox textBoxGradeValue;
        private System.Windows.Forms.TextBox textBoxGradeName;
    }
}