﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrzegorzMaciejewskiLab2
{
    [Serializable]
   public class Grade
    {
        ///Kolejne zmienne są to po prostu zmienne klasy którymi charakteryzuje się każdy obiekt tej klasy
        //
        public String GradeName { get; set; }
        public double GradeValue { get; set; }
        public Grade() { } // konstruktor domyślny
        public Grade(double value,String name)  // konstruktor z parametrami
        {
            GradeName = name;
            GradeValue = value;
        }
    }
}
