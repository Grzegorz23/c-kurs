﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GrzegorzMaciejewskiLab2
{
    public partial class FormMain : Form
    {
        List<Person> listOfPeople; // lista osób

        public FormMain()
        {
            InitializeComponent();
            listOfPeople = new List<Person>();
        }
        /// Metoda zajmująca się obsługą przycisku dodawania nowej osoby, dodawanie osoby odbywa się w nowym oknie
        //
        private void buttonAddPerson_Click(object sender, EventArgs e)
        {
            FormAddPerson formAddPerson = new FormAddPerson(); // obiekt z pomocą którego dodamy nową osobe
            formAddPerson.ShowDialog();
            if(formAddPerson.personToAdd != null)
            listOfPeople.Add(formAddPerson.personToAdd);   
        }
        /// Metoda obsługująca przycisk wyświetlający liste osób
        //
        private void buttonDisplay_Click(object sender, EventArgs e)
        {
            if (listOfPeople.Any())
            {
                dataGridViewListOfPeople.DataSource = null;
                dataGridViewListOfPeople.DataSource = listOfPeople;
            }
        }
        /// Metoda obsługi guziczka dodajacego oceny, dodawanie oceny odbywa się w odrębnym oknie
        //
        private void buttonAddGrade_Click(object sender, EventArgs e)
        {
            try
            {
                FormAddGrade formAddGrade = new FormAddGrade(); // Obiekt do dodawania nowej oceny
                formAddGrade.ShowDialog();
                if(formAddGrade.gradeToAdd!=null)
                listOfPeople[dataGridViewListOfPeople.SelectedRows[0].Index].ListOfGrades.Add(formAddGrade.gradeToAdd);
            }
            catch(Exception ex)
            {
                MessageBox.Show("ERROR");
            }
        }
        /// Metoda wyświetlająca oceny danej osoby, osoba którą klikniemy(zaznaczymy), przechowywana jest chwilowo
        /// w zmiennej pomocniczej aby wyświetliły się tylko oceny tej osoby
        //
        private void ShowGrades(object sender, EventArgs e)
        {
            try
            {
                Person auxiliaryPerson = listOfPeople[dataGridViewListOfPeople.SelectedRows[0].Index]; // pomocnicza osoba
                dataGridViewGrades.DataSource = null;
                dataGridViewGrades.DataSource = auxiliaryPerson.ListOfGrades;
                Boolean badGradeOccured = false;
                foreach (Grade g in auxiliaryPerson.ListOfGrades)
                {
                    if (g.GradeValue < 3)
                        badGradeOccured = true;
                }
                if (badGradeOccured)        // Fragment kodu odpowiedzialny za puszczenie melodyjki w odpowiednim momencie
                {                           // oraz wyświetlenie smutnej lub wesołej minki
                    pictureBoxFace.Image = Properties.Resources.sadFace;
                    System.Media.SoundPlayer badGradeSound = new System.Media.SoundPlayer(Properties.Resources.The_Sound_of_Silence_Original_Version_from_1964_);
                    badGradeSound.Play();
                }
                else
                {
                    pictureBoxFace.Image = Properties.Resources.happyFace;
                    System.Media.SoundPlayer goodGradeSound = new System.Media.SoundPlayer(Properties.Resources.Queen___We_Are_The_Champions__lyrics_);
                    goodGradeSound.Play();
                }
            }
            catch(Exception exc)
            {
                MessageBox.Show("Nieprawidłowe zaznaczenie osoby lub jej brak.");
            }
        }
        /// Metoda wykonująca zapis do pliku binarnego o stałej nazwie, plik się nadpisuje sam
        //
         private void buttonSave_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderDialog = new FolderBrowserDialog(); // obsługa znajdywania scieżki do zapisu(w oknie)
            folderDialog.ShowDialog();
            String pathToSave = folderDialog.SelectedPath;
            if(pathToSave!="")
            using (System.IO.Stream stream = System.IO.File.Open((pathToSave+"\\Lista_Osob.bin"), System.IO.FileMode.Create))
            {
                BinaryFormatter binaryFormater = new BinaryFormatter(); // binarna serializacja danych
                binaryFormater.Serialize(stream, listOfPeople);
            }
        }
        /// Metoda odczytujące dane z pliku binarnego 
        //
        private void buttonLoad_Click(object sender, EventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog(); // znajdowanie ścieżki do pliku z którego chcemy odczytać dane
            fileDialog.ShowDialog();
            String pathToLoad = fileDialog.FileName;
            if (pathToLoad!= "")
                using (System.IO.Stream stream = System.IO.File.Open((pathToLoad), System.IO.FileMode.Open))
            {
                BinaryFormatter binaryFormater = new BinaryFormatter(); // binarna serializacja danych
                listOfPeople = (List<Person>)binaryFormater.Deserialize(stream);
            }
        }
   }
}
