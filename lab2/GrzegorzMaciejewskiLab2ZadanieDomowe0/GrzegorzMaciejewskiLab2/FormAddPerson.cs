﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GrzegorzMaciejewskiLab2
{
    public partial class FormAddPerson : Form
    {
        /// Zmienna typu Person z pomocą której przekażemy nową osobe do FormMain
        //
        public Person personToAdd = null;
         public FormAddPerson()
        {
            InitializeComponent();
            
        }
         /// Metoda która zatwierdza bądż odrzuca osobę którą chcemy dodać
        //
        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                personToAdd = new Person(textBoxName.Text, textBoxSurname.Text, Int32.Parse(textBoxAge.Text));
            }
            catch(Exception exc)
            {
                MessageBox.Show("Za mało danych, lub nieprawidłowe dane.");
            }
            Close();
        }
        /// Zamykadełko
        //
        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        
    }
}
