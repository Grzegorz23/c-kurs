﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrzegorzMaciejewskiLab2
{
   class Person
    {
       public String Name { get; set; }
       public String Surname { get; set; }
       public int Age { get; set; }
       public List<Grade> listOfGrades=new List<Grade>();

        /// konstruktor domyslny
        //
        public Person() { }
        /// konstruktor z parametrami
        // 
        public Person(String name,String surname, int age)
        {
            
            this.Name = name;
            this.Surname = surname;
            this.Age = age;
        }
    }
}
