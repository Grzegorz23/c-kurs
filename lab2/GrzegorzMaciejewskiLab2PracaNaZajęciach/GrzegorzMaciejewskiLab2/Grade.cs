﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrzegorzMaciejewskiLab2
{
    class Grade
    {
       public String GradeName { get; set; }
        public double GradeValue { get; set; }

        public Grade(double value,String name)
        {
            GradeName = name;
            GradeValue = value;
        }
    }
}
