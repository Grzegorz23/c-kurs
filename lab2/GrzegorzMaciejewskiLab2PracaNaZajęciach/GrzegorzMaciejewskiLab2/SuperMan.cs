﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrzegorzMaciejewskiLab2
{
    class SuperMan:Person
    {
       public int amountOfHands;

       public SuperMan(String name,String surname,int age,int amountOfHands):base(name,surname,age)
        {
            this.amountOfHands = amountOfHands;
        }
        
        
    }
}
