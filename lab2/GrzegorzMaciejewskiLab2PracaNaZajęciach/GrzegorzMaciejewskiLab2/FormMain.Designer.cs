﻿namespace GrzegorzMaciejewskiLab2
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelNameAndSurname = new System.Windows.Forms.Label();
            this.dataGridViewListOfPeople = new System.Windows.Forms.DataGridView();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.textBoxSurname = new System.Windows.Forms.TextBox();
            this.textBoxAge = new System.Windows.Forms.TextBox();
            this.buttonAddPerson = new System.Windows.Forms.Button();
            this.buttonDisplay = new System.Windows.Forms.Button();
            this.dataGridViewGrades = new System.Windows.Forms.DataGridView();
            this.labelPeople = new System.Windows.Forms.Label();
            this.labelGrades = new System.Windows.Forms.Label();
            this.textBoxGradeValue = new System.Windows.Forms.TextBox();
            this.textBoxValueName = new System.Windows.Forms.TextBox();
            this.buttonAddGrade = new System.Windows.Forms.Button();
            this.buttonShowGrades = new System.Windows.Forms.Button();
            this.pictureBoxFace = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewListOfPeople)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewGrades)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFace)).BeginInit();
            this.SuspendLayout();
            // 
            // labelNameAndSurname
            // 
            this.labelNameAndSurname.AutoSize = true;
            this.labelNameAndSurname.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelNameAndSurname.Location = new System.Drawing.Point(199, 9);
            this.labelNameAndSurname.Name = "labelNameAndSurname";
            this.labelNameAndSurname.Size = new System.Drawing.Size(280, 31);
            this.labelNameAndSurname.TabIndex = 0;
            this.labelNameAndSurname.Text = "Grzegorz Maciejewski";
            // 
            // dataGridViewListOfPeople
            // 
            this.dataGridViewListOfPeople.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridViewListOfPeople.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridViewListOfPeople.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dataGridViewListOfPeople.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewListOfPeople.Location = new System.Drawing.Point(127, 79);
            this.dataGridViewListOfPeople.Name = "dataGridViewListOfPeople";
            this.dataGridViewListOfPeople.Size = new System.Drawing.Size(245, 316);
            this.dataGridViewListOfPeople.TabIndex = 1;
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(12, 79);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(100, 20);
            this.textBoxName.TabIndex = 2;
            this.textBoxName.Text = "Imie";
            // 
            // textBoxSurname
            // 
            this.textBoxSurname.Location = new System.Drawing.Point(12, 105);
            this.textBoxSurname.Name = "textBoxSurname";
            this.textBoxSurname.Size = new System.Drawing.Size(100, 20);
            this.textBoxSurname.TabIndex = 3;
            this.textBoxSurname.Text = "Nazwisko";
            // 
            // textBoxAge
            // 
            this.textBoxAge.Location = new System.Drawing.Point(12, 131);
            this.textBoxAge.Name = "textBoxAge";
            this.textBoxAge.Size = new System.Drawing.Size(100, 20);
            this.textBoxAge.TabIndex = 4;
            this.textBoxAge.Text = "22";
            // 
            // buttonAddPerson
            // 
            this.buttonAddPerson.Location = new System.Drawing.Point(12, 157);
            this.buttonAddPerson.Name = "buttonAddPerson";
            this.buttonAddPerson.Size = new System.Drawing.Size(75, 23);
            this.buttonAddPerson.TabIndex = 5;
            this.buttonAddPerson.Text = "Dodaj Osobe";
            this.buttonAddPerson.UseVisualStyleBackColor = true;
            this.buttonAddPerson.Click += new System.EventHandler(this.buttonAddPerson_Click);
            // 
            // buttonDisplay
            // 
            this.buttonDisplay.Location = new System.Drawing.Point(12, 186);
            this.buttonDisplay.Name = "buttonDisplay";
            this.buttonDisplay.Size = new System.Drawing.Size(75, 23);
            this.buttonDisplay.TabIndex = 6;
            this.buttonDisplay.Text = "Wyswietl";
            this.buttonDisplay.UseVisualStyleBackColor = true;
            this.buttonDisplay.Click += new System.EventHandler(this.buttonDisplay_Click);
            // 
            // dataGridViewGrades
            // 
            this.dataGridViewGrades.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dataGridViewGrades.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewGrades.Location = new System.Drawing.Point(395, 79);
            this.dataGridViewGrades.Name = "dataGridViewGrades";
            this.dataGridViewGrades.Size = new System.Drawing.Size(240, 316);
            this.dataGridViewGrades.TabIndex = 7;
            // 
            // labelPeople
            // 
            this.labelPeople.AutoSize = true;
            this.labelPeople.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelPeople.Location = new System.Drawing.Point(201, 52);
            this.labelPeople.Name = "labelPeople";
            this.labelPeople.Size = new System.Drawing.Size(97, 24);
            this.labelPeople.TabIndex = 8;
            this.labelPeople.Text = "Lista Ludzi";
            // 
            // labelGrades
            // 
            this.labelGrades.AutoSize = true;
            this.labelGrades.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelGrades.Location = new System.Drawing.Point(485, 52);
            this.labelGrades.Name = "labelGrades";
            this.labelGrades.Size = new System.Drawing.Size(66, 24);
            this.labelGrades.TabIndex = 9;
            this.labelGrades.Text = "Oceny";
            // 
            // textBoxGradeValue
            // 
            this.textBoxGradeValue.Location = new System.Drawing.Point(12, 225);
            this.textBoxGradeValue.Name = "textBoxGradeValue";
            this.textBoxGradeValue.Size = new System.Drawing.Size(100, 20);
            this.textBoxGradeValue.TabIndex = 10;
            // 
            // textBoxValueName
            // 
            this.textBoxValueName.Location = new System.Drawing.Point(12, 251);
            this.textBoxValueName.Name = "textBoxValueName";
            this.textBoxValueName.Size = new System.Drawing.Size(100, 20);
            this.textBoxValueName.TabIndex = 11;
            // 
            // buttonAddGrade
            // 
            this.buttonAddGrade.Location = new System.Drawing.Point(12, 277);
            this.buttonAddGrade.Name = "buttonAddGrade";
            this.buttonAddGrade.Size = new System.Drawing.Size(75, 23);
            this.buttonAddGrade.TabIndex = 12;
            this.buttonAddGrade.Text = "Dodaj Ocene";
            this.buttonAddGrade.UseVisualStyleBackColor = true;
            this.buttonAddGrade.Click += new System.EventHandler(this.buttonAddGrade_Click);
            // 
            // buttonShowGrades
            // 
            this.buttonShowGrades.Location = new System.Drawing.Point(12, 306);
            this.buttonShowGrades.Name = "buttonShowGrades";
            this.buttonShowGrades.Size = new System.Drawing.Size(75, 23);
            this.buttonShowGrades.TabIndex = 13;
            this.buttonShowGrades.Text = "Wyswietl Oceny";
            this.buttonShowGrades.UseVisualStyleBackColor = true;
            this.buttonShowGrades.Click += new System.EventHandler(this.buttonShowGrades_Click);
            // 
            // pictureBoxFace
            // 
            this.pictureBoxFace.Location = new System.Drawing.Point(571, 23);
            this.pictureBoxFace.Name = "pictureBoxFace";
            this.pictureBoxFace.Size = new System.Drawing.Size(100, 50);
            this.pictureBoxFace.TabIndex = 14;
            this.pictureBoxFace.TabStop = false;
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(683, 407);
            this.Controls.Add(this.pictureBoxFace);
            this.Controls.Add(this.buttonShowGrades);
            this.Controls.Add(this.buttonAddGrade);
            this.Controls.Add(this.textBoxValueName);
            this.Controls.Add(this.textBoxGradeValue);
            this.Controls.Add(this.labelGrades);
            this.Controls.Add(this.labelPeople);
            this.Controls.Add(this.dataGridViewGrades);
            this.Controls.Add(this.buttonDisplay);
            this.Controls.Add(this.buttonAddPerson);
            this.Controls.Add(this.textBoxAge);
            this.Controls.Add(this.textBoxSurname);
            this.Controls.Add(this.textBoxName);
            this.Controls.Add(this.dataGridViewListOfPeople);
            this.Controls.Add(this.labelNameAndSurname);
            this.Name = "FormMain";
            this.Text = "FormName";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewListOfPeople)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewGrades)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFace)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelNameAndSurname;
        private System.Windows.Forms.DataGridView dataGridViewListOfPeople;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.TextBox textBoxSurname;
        private System.Windows.Forms.TextBox textBoxAge;
        private System.Windows.Forms.Button buttonAddPerson;
        private System.Windows.Forms.Button buttonDisplay;
        private System.Windows.Forms.DataGridView dataGridViewGrades;
        private System.Windows.Forms.Label labelPeople;
        private System.Windows.Forms.Label labelGrades;
        private System.Windows.Forms.TextBox textBoxGradeValue;
        private System.Windows.Forms.TextBox textBoxValueName;
        private System.Windows.Forms.Button buttonAddGrade;
        private System.Windows.Forms.Button buttonShowGrades;
        private System.Windows.Forms.PictureBox pictureBoxFace;
    }
}

