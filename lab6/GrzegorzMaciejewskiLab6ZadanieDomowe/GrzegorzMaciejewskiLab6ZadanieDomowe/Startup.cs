﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GrzegorzMaciejewskiLab6ZadanieDomowe.Startup))]
namespace GrzegorzMaciejewskiLab6ZadanieDomowe
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
