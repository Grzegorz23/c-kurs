﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GrzegorzMaciejewskiLab6.Startup))]
namespace GrzegorzMaciejewskiLab6
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
