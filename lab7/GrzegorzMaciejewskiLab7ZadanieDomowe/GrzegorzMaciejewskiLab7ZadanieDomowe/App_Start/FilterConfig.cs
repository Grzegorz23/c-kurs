﻿using System.Web;
using System.Web.Mvc;

namespace GrzegorzMaciejewskiLab7ZadanieDomowe
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
