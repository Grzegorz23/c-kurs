﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GrzegorzMaciejewskiLab7ZadanieDomowe.Startup))]
namespace GrzegorzMaciejewskiLab7ZadanieDomowe
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
