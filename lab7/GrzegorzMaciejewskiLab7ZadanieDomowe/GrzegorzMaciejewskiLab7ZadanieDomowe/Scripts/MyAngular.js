﻿//KONTROLER


var app = angular.module("myApp", ['ngDialog'])


app.controller("MyController", ["$scope", "$http",'ngDialog', function ($scope, $http, ngDialog) {

  

    var promiseOfFlights = $http.get("/Flight/GetListOfFlights");

    var onSuccess = function (response) {
        $scope.flights = response.data
        
    }
   
    //Przydzielanie odpowiedniego koloru dla pola w zależności od ilości miejsc na lot
    $scope.freeSeats = function (miejsca) {
        if (miejsca > 10)
            return "btn-success";
        else if (miejsca > 0)
            return "btn-warning";
        else return "btn-danger";

    }

    //Zmienne pomocnicze
    $scope.FlightID;
    $scope.FlightSeats;
    //ngDialog okienko do rezerwacji
    $scope.clickToOpen = function (flight) {
        $scope.FlightID = flight.ID;
        $scope.FlightSeats = flight.WolneMiejsca;
        ngDialog.open({ template: "templateId", scope: $scope, });
        if (flight.WolneMiejsca > 0)
            $scope.freeSeats = true;
        else
            $scope.freeSeats = false;
     };

    ///Rezerwacja biletów, i zaktualizowanie bazy danych - nie obeszło się bez pomocy internetu
    $scope.bookTickets = function ()
    {
        var tickets = document.getElementsByName("tickets")[0].value;
        if (tickets > 0 && tickets <= $scope.FlightSeats)
        {
            ngDialog.close();
            var amountOfTickets = $scope.FlightSeats - tickets;
            //dodawanie zaktualizowanego lotu do bazy danych
            $.ajax({
                type: "POST",
                url: "/Flight/UpdateSeatsAmount",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({ id: $scope.FlightID, tickets: amountOfTickets }),
                success: function (data)
                {
                    alert(data);
                },
                failure: function ()
                {
                    alert("error");
                }
            });

            var name = document.getElementsByName("CustomerName")[0].value;
            var surname = document.getElementsByName("CustomerSurname")[0].value;
            
            //Dodawanie klienta do bazy danych
            $.ajax(
                {
                type: "POST",
                url: "/Flight/AddCustomerToDatabase",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({ name: name, surname: surname, flightId: $scope.FlightID, tickets: tickets }),
                success: function (data)
                {
                    alert(data);
                },
                failure: function ()
                {
                    alert("error");
                }
            });
            location.reload(true);
        }
        //jeżeli liczba biletów jest za duża
        else {
            ngDialog.close();
            ngDialog.open({ template: 'tooManyTickets', scope: $scope })
        }
    };

  
    var onError = function (response) {
        $scope.omgError = "No flights avaiable"
    }

    promiseOfFlights.then(onSuccess, onError)


}])