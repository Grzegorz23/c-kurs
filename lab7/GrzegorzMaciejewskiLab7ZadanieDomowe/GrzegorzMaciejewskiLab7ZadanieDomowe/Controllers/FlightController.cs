﻿using GrzegorzMaciejewskiLab7ZadanieDomowe.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GrzegorzMaciejewskiLab7ZadanieDomowe.Controllers
{
    public class FlightController : Controller
    {
        // GET: Flight
        public ActionResult Index()
        {
            //var entities = new GrzegorzMaciejewskiLab7ZadanieDomoweEntities();
            return View();
        }

        public JsonResult GetListOfFlights()
        {
            var ctx = new GrzegorzMaciejewskiLab7Entities();
            List<Loty> list = ctx.Loty.ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        public ActionResult UpdateSeatsAmount(int id, int tickets)
        {
            Loty flight;
            using (var ctx = new GrzegorzMaciejewskiLab7Entities())
            {
                flight = ctx.Loty.FirstOrDefault(m => m.ID == id);
                flight.WolneMiejsca = tickets;
                ctx.SaveChanges();
            }
            return View("Index");
        }

        public ActionResult AddCustomerToDatabase(string name, string surname, int flightId, int tickets)
        {
            Customers customer;
            using (var ctx = new GrzegorzMaciejewskiLab7Entities())
            {
                customer = new Customers()
                {
                    Name = name,
                    Surname = surname,
                    FlightID = flightId,
                    TicketsAmount = tickets
                };
                ctx.Customers.Add(customer);
                ctx.SaveChanges();
            }
            return View("Index");
        }
    }
}