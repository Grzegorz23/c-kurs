USE [master]
GO
/****** Object:  Database [GrzegorzMaciejewskiLab7ZadanieDomowe]    Script Date: 2016-06-17 21:43:18 ******/
CREATE DATABASE [GrzegorzMaciejewskiLab7ZadanieDomowe]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'GrzegorzMaciejewskiLab7ZadanieDomowe', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\GrzegorzMaciejewskiLab7ZadanieDomowe.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'GrzegorzMaciejewskiLab7ZadanieDomowe_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\GrzegorzMaciejewskiLab7ZadanieDomowe_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [GrzegorzMaciejewskiLab7ZadanieDomowe] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [GrzegorzMaciejewskiLab7ZadanieDomowe].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [GrzegorzMaciejewskiLab7ZadanieDomowe] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [GrzegorzMaciejewskiLab7ZadanieDomowe] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [GrzegorzMaciejewskiLab7ZadanieDomowe] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [GrzegorzMaciejewskiLab7ZadanieDomowe] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [GrzegorzMaciejewskiLab7ZadanieDomowe] SET ARITHABORT OFF 
GO
ALTER DATABASE [GrzegorzMaciejewskiLab7ZadanieDomowe] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [GrzegorzMaciejewskiLab7ZadanieDomowe] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [GrzegorzMaciejewskiLab7ZadanieDomowe] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [GrzegorzMaciejewskiLab7ZadanieDomowe] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [GrzegorzMaciejewskiLab7ZadanieDomowe] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [GrzegorzMaciejewskiLab7ZadanieDomowe] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [GrzegorzMaciejewskiLab7ZadanieDomowe] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [GrzegorzMaciejewskiLab7ZadanieDomowe] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [GrzegorzMaciejewskiLab7ZadanieDomowe] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [GrzegorzMaciejewskiLab7ZadanieDomowe] SET  DISABLE_BROKER 
GO
ALTER DATABASE [GrzegorzMaciejewskiLab7ZadanieDomowe] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [GrzegorzMaciejewskiLab7ZadanieDomowe] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [GrzegorzMaciejewskiLab7ZadanieDomowe] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [GrzegorzMaciejewskiLab7ZadanieDomowe] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [GrzegorzMaciejewskiLab7ZadanieDomowe] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [GrzegorzMaciejewskiLab7ZadanieDomowe] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [GrzegorzMaciejewskiLab7ZadanieDomowe] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [GrzegorzMaciejewskiLab7ZadanieDomowe] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [GrzegorzMaciejewskiLab7ZadanieDomowe] SET  MULTI_USER 
GO
ALTER DATABASE [GrzegorzMaciejewskiLab7ZadanieDomowe] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [GrzegorzMaciejewskiLab7ZadanieDomowe] SET DB_CHAINING OFF 
GO
ALTER DATABASE [GrzegorzMaciejewskiLab7ZadanieDomowe] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [GrzegorzMaciejewskiLab7ZadanieDomowe] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [GrzegorzMaciejewskiLab7ZadanieDomowe] SET DELAYED_DURABILITY = DISABLED 
GO
USE [GrzegorzMaciejewskiLab7ZadanieDomowe]
GO
/****** Object:  Table [dbo].[Customers]    Script Date: 2016-06-17 21:43:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Customers](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FlightID] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Surname] [varchar](50) NOT NULL,
	[TicketsAmount] [int] NOT NULL,
 CONSTRAINT [PK_Customers] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Loty]    Script Date: 2016-06-17 21:43:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Loty](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MiejsceStartu] [varchar](50) NOT NULL,
	[MiejsceDocelowe] [varchar](50) NOT NULL,
	[CzasOdlotu] [varchar](50) NOT NULL,
	[CzasPrzylotu] [varchar](50) NOT NULL,
	[WolneMiejsca] [int] NOT NULL,
	[Cena] [money] NOT NULL,
 CONSTRAINT [PK_Loty] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Customers] ON 

INSERT [dbo].[Customers] ([ID], [FlightID], [Name], [Surname], [TicketsAmount]) VALUES (1, 15, N'Grzegorz', N'M', 47)
INSERT [dbo].[Customers] ([ID], [FlightID], [Name], [Surname], [TicketsAmount]) VALUES (2, 15, N'Marian ', N'K', 3)
SET IDENTITY_INSERT [dbo].[Customers] OFF
SET IDENTITY_INSERT [dbo].[Loty] ON 

INSERT [dbo].[Loty] ([ID], [MiejsceStartu], [MiejsceDocelowe], [CzasOdlotu], [CzasPrzylotu], [WolneMiejsca], [Cena]) VALUES (5, N'Wrocław', N'Nowy Jork', N'Jul  2 2016 12:00AM', N'Jul  2 2016 12:00AM', 250, 300.0000)
INSERT [dbo].[Loty] ([ID], [MiejsceStartu], [MiejsceDocelowe], [CzasOdlotu], [CzasPrzylotu], [WolneMiejsca], [Cena]) VALUES (7, N'Cacao', N'DeMoreno', N'Nov 14 2015 12:00AM', N'Nov 14 2016 12:00AM', 122, 250.0000)
INSERT [dbo].[Loty] ([ID], [MiejsceStartu], [MiejsceDocelowe], [CzasOdlotu], [CzasPrzylotu], [WolneMiejsca], [Cena]) VALUES (8, N'Warszawa', N'Krakow', N'Aug 12 2016 12:00AM', N'Aug 17 2016 12:00AM', 100, 213.0000)
INSERT [dbo].[Loty] ([ID], [MiejsceStartu], [MiejsceDocelowe], [CzasOdlotu], [CzasPrzylotu], [WolneMiejsca], [Cena]) VALUES (9, N'Krakow', N'Waszyngton', N'Jul  3 2016 12:00AM', N'Jul  4 2016 12:00AM', 124, 2424.0000)
INSERT [dbo].[Loty] ([ID], [MiejsceStartu], [MiejsceDocelowe], [CzasOdlotu], [CzasPrzylotu], [WolneMiejsca], [Cena]) VALUES (12, N'Oslo', N'Berlin', N'Dec 13 2017 12:00AM', N'Dec 15 2017 12:00AM', 239, 233.0000)
INSERT [dbo].[Loty] ([ID], [MiejsceStartu], [MiejsceDocelowe], [CzasOdlotu], [CzasPrzylotu], [WolneMiejsca], [Cena]) VALUES (13, N'Nowy Jork', N'Las Vegas', N'Aug 15 2016 12:00AM', N'Aug 15 2016 12:00AM', 233, 266.0000)
INSERT [dbo].[Loty] ([ID], [MiejsceStartu], [MiejsceDocelowe], [CzasOdlotu], [CzasPrzylotu], [WolneMiejsca], [Cena]) VALUES (14, N'Helsinki', N'Sydney', N'Nov  5 2016 12:00AM', N'Nov 16 2016 12:00AM', 231, 1500.0000)
INSERT [dbo].[Loty] ([ID], [MiejsceStartu], [MiejsceDocelowe], [CzasOdlotu], [CzasPrzylotu], [WolneMiejsca], [Cena]) VALUES (15, N'Wroclaw', N'Trzebnica', N'Jun  5 2016 12:00AM', N'Jun  5 2016 12:00AM', 0, 120.0000)
INSERT [dbo].[Loty] ([ID], [MiejsceStartu], [MiejsceDocelowe], [CzasOdlotu], [CzasPrzylotu], [WolneMiejsca], [Cena]) VALUES (17, N'Opole', N'Bydgoszcz', N'Mar  4 2016 12:00AM', N'Mar  5 2016 12:00AM', 231, 1244.0000)
INSERT [dbo].[Loty] ([ID], [MiejsceStartu], [MiejsceDocelowe], [CzasOdlotu], [CzasPrzylotu], [WolneMiejsca], [Cena]) VALUES (18, N'Miami', N'Los Angeles', N'Jul 17 2016 12:00AM', N'Jul 18 2016 12:00AM', 244, 2525.0000)
INSERT [dbo].[Loty] ([ID], [MiejsceStartu], [MiejsceDocelowe], [CzasOdlotu], [CzasPrzylotu], [WolneMiejsca], [Cena]) VALUES (19, N'Waszyngton', N'Los Angeles', N'Aug 26 2016 12:00AM', N'Aug 17 2016 12:00AM', 440, 1220.0000)
INSERT [dbo].[Loty] ([ID], [MiejsceStartu], [MiejsceDocelowe], [CzasOdlotu], [CzasPrzylotu], [WolneMiejsca], [Cena]) VALUES (20, N'Moskwa', N'Waszyngton', N'Sep  1 2016 12:00AM', N'Sep  2 2016 12:00AM', 666, 6666.0000)
INSERT [dbo].[Loty] ([ID], [MiejsceStartu], [MiejsceDocelowe], [CzasOdlotu], [CzasPrzylotu], [WolneMiejsca], [Cena]) VALUES (21, N'Sydney', N'Moskwa', N'Jun  6 2016 12:00AM', N'Jun  7 2016 12:00AM', 510, 1700.0000)
INSERT [dbo].[Loty] ([ID], [MiejsceStartu], [MiejsceDocelowe], [CzasOdlotu], [CzasPrzylotu], [WolneMiejsca], [Cena]) VALUES (22, N'Trzebnica', N'Będkowo', N'Oct  7 2016 12:00AM', N'Oct  7 2016 12:00AM', 20, 10.0000)
INSERT [dbo].[Loty] ([ID], [MiejsceStartu], [MiejsceDocelowe], [CzasOdlotu], [CzasPrzylotu], [WolneMiejsca], [Cena]) VALUES (23, N'Poznań', N'Gdynia', N'Jan  7 2017 12:00AM', N'Jan  7 2017 12:00AM', 100, 700.0000)
INSERT [dbo].[Loty] ([ID], [MiejsceStartu], [MiejsceDocelowe], [CzasOdlotu], [CzasPrzylotu], [WolneMiejsca], [Cena]) VALUES (24, N'Gdynia', N'Frankfurt', N'Jul 10 2016 12:00AM', N'Jul 10 2016 12:00AM', 250, 1000.0000)
INSERT [dbo].[Loty] ([ID], [MiejsceStartu], [MiejsceDocelowe], [CzasOdlotu], [CzasPrzylotu], [WolneMiejsca], [Cena]) VALUES (25, N'Wrocław', N'Frankfurt', N'Jun  6 2016 12:00AM', N'Jun  6 2016 12:00AM', 290, 1200.0000)
INSERT [dbo].[Loty] ([ID], [MiejsceStartu], [MiejsceDocelowe], [CzasOdlotu], [CzasPrzylotu], [WolneMiejsca], [Cena]) VALUES (26, N'Frankfurt', N'Bangalore', N'Jun  6 2016 12:00AM', N'Jun  6 2016 12:00AM', 0, 3000.0000)
INSERT [dbo].[Loty] ([ID], [MiejsceStartu], [MiejsceDocelowe], [CzasOdlotu], [CzasPrzylotu], [WolneMiejsca], [Cena]) VALUES (27, N'Praga', N'Trzebnica', N'Jan  5 2016 12:00AM', N'Jan  5 2016 12:00AM', 3, 777.0000)
INSERT [dbo].[Loty] ([ID], [MiejsceStartu], [MiejsceDocelowe], [CzasOdlotu], [CzasPrzylotu], [WolneMiejsca], [Cena]) VALUES (28, N'Trzebnica', N'Warszawa', N'Aug  7 2016 12:00AM', N'Aug  7 2016 12:00AM', 5, 567.0000)
SET IDENTITY_INSERT [dbo].[Loty] OFF
USE [master]
GO
ALTER DATABASE [GrzegorzMaciejewskiLab7ZadanieDomowe] SET  READ_WRITE 
GO
