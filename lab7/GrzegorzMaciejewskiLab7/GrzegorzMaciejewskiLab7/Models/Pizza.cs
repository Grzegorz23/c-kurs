﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GrzegorzMaciejewskiLab7.Models
{
    public class Pizza
    {
      
        public string Name { get; set; }
        public float Price { get; set; }
        public string Ingredients { get; set; }


    }
}