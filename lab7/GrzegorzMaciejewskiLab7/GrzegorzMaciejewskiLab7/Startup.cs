﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GrzegorzMaciejewskiLab7.Startup))]
namespace GrzegorzMaciejewskiLab7
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
