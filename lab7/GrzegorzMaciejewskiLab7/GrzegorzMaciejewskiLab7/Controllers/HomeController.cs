﻿using GrzegorzMaciejewskiLab7.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GrzegorzMaciejewskiLab7.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public JsonResult GetListOfPizzas()
        {
            String baseIngredients = "ser,sos,oregano";
            List<Pizza> list = new List<Pizza>();

            Pizza newPizza = new Pizza()
            { Name="Margarita",Price=9.30f,Ingredients=baseIngredients};
            list.Add(newPizza);
             newPizza = new Pizza()
            { Name = "Salami", Price = 10.30f, Ingredients = baseIngredients };
            list.Add(newPizza);
            newPizza = new Pizza()
            { Name = "Włoska", Price = 17.60f, Ingredients = baseIngredients };
            list.Add(newPizza);

            return Json(list, JsonRequestBehavior.AllowGet);
        }
    }
}