﻿//KONTROLER


var app = angular.module("myApp", [])


app.controller("MyController", ["$scope","$http", function ($scope,$http) {
    
    $scope.visible = false;
    $scope.togglePizzas = function () {
        $scope.visible=!$scope.visible
    }

    var promiseOfPizzas = $http.get("/Home/GetListOfPizzas");

    var onSuccess = function (response) {
        $scope.pizzas = response.data
    }


    var onError = function (response) {
        $scope.omgError="No Pizzas were found"
    }

    promiseOfPizzas.then(onSuccess,onError)


}])