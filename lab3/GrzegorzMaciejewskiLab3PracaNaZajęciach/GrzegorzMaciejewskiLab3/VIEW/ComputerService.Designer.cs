﻿namespace GrzegorzMaciejewskiLab3
{
    partial class FormComputerService
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewComputerService = new System.Windows.Forms.DataGridView();
            this.labelName = new System.Windows.Forms.Label();
            this.buttonDisplay = new System.Windows.Forms.Button();
            this.textBoxCondition = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewComputerService)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewComputerService
            // 
            this.dataGridViewComputerService.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewComputerService.Location = new System.Drawing.Point(352, 12);
            this.dataGridViewComputerService.Name = "dataGridViewComputerService";
            this.dataGridViewComputerService.Size = new System.Drawing.Size(391, 482);
            this.dataGridViewComputerService.TabIndex = 0;
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Font = new System.Drawing.Font("Microsoft YaHei UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelName.Location = new System.Drawing.Point(41, 49);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(242, 26);
            this.labelName.TabIndex = 1;
            this.labelName.Text = "Grzegorz Maciejewski";
            // 
            // buttonDisplay
            // 
            this.buttonDisplay.Location = new System.Drawing.Point(215, 106);
            this.buttonDisplay.Name = "buttonDisplay";
            this.buttonDisplay.Size = new System.Drawing.Size(91, 44);
            this.buttonDisplay.TabIndex = 2;
            this.buttonDisplay.Text = "Pokaż pracowników";
            this.buttonDisplay.UseVisualStyleBackColor = true;
            this.buttonDisplay.Click += new System.EventHandler(this.buttonDisplay_Click);
            // 
            // textBoxCondition
            // 
            this.textBoxCondition.Location = new System.Drawing.Point(206, 156);
            this.textBoxCondition.Name = "textBoxCondition";
            this.textBoxCondition.Size = new System.Drawing.Size(100, 20);
            this.textBoxCondition.TabIndex = 3;
            // 
            // FormComputerService
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(758, 506);
            this.Controls.Add(this.textBoxCondition);
            this.Controls.Add(this.buttonDisplay);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.dataGridViewComputerService);
            this.Name = "FormComputerService";
            this.Text = "ComputerService";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewComputerService)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewComputerService;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.Button buttonDisplay;
        private System.Windows.Forms.TextBox textBoxCondition;
    }
}

