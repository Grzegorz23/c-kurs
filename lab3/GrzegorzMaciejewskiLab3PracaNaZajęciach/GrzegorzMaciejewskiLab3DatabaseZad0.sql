USE [GrzegorzMaciejewskiLab3ComputerService]
GO
/****** Object:  Table [dbo].[Computers]    Script Date: 2016-04-21 21:04:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Computers](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Producer] [nvarchar](50) NOT NULL,
	[Model] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Computers] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Employees]    Script Date: 2016-04-21 21:04:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Employees](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Surname] [varchar](50) NOT NULL,
	[Age] [int] NOT NULL,
 CONSTRAINT [PK_Employees] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Services]    Script Date: 2016-04-21 21:04:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Services](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ComputerID] [int] NOT NULL,
	[EmployeeID] [int] NOT NULL,
	[Cost] [int] NOT NULL,
	[Description] [varchar](100) NOT NULL,
 CONSTRAINT [PK_Services] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Computers] ON 

INSERT [dbo].[Computers] ([ID], [Producer], [Model]) VALUES (1, N'Asus', N'R510')
INSERT [dbo].[Computers] ([ID], [Producer], [Model]) VALUES (2, N'Lenovo', N'50-70')
INSERT [dbo].[Computers] ([ID], [Producer], [Model]) VALUES (3, N'Toszyba', N'5670')
INSERT [dbo].[Computers] ([ID], [Producer], [Model]) VALUES (4, N'MSI', N'GS570')
SET IDENTITY_INSERT [dbo].[Computers] OFF
SET IDENTITY_INSERT [dbo].[Employees] ON 

INSERT [dbo].[Employees] ([ID], [Name], [Surname], [Age]) VALUES (1, N'Dawid', N'Szewczyk', 21)
INSERT [dbo].[Employees] ([ID], [Name], [Surname], [Age]) VALUES (2, N'Grzegorz', N'Maciejewski', 20)
INSERT [dbo].[Employees] ([ID], [Name], [Surname], [Age]) VALUES (3, N'Witold', N'Kościukiewicz', 20)
INSERT [dbo].[Employees] ([ID], [Name], [Surname], [Age]) VALUES (4, N'Dawid', N'Rainczuk', 20)
SET IDENTITY_INSERT [dbo].[Employees] OFF
SET IDENTITY_INSERT [dbo].[Services] ON 

INSERT [dbo].[Services] ([ID], [ComputerID], [EmployeeID], [Cost], [Description]) VALUES (2, 1, 1, 500, N'Dysk')
INSERT [dbo].[Services] ([ID], [ComputerID], [EmployeeID], [Cost], [Description]) VALUES (3, 1, 3, 600, N'RAM')
INSERT [dbo].[Services] ([ID], [ComputerID], [EmployeeID], [Cost], [Description]) VALUES (4, 4, 2, 50, N'Format')
INSERT [dbo].[Services] ([ID], [ComputerID], [EmployeeID], [Cost], [Description]) VALUES (5, 4, 4, 7000, N'Karta Graficzna')
INSERT [dbo].[Services] ([ID], [ComputerID], [EmployeeID], [Cost], [Description]) VALUES (6, 3, 3, 1000, N'Płyta Główna')
SET IDENTITY_INSERT [dbo].[Services] OFF
ALTER TABLE [dbo].[Services]  WITH CHECK ADD  CONSTRAINT [FK_Services_Computers] FOREIGN KEY([ComputerID])
REFERENCES [dbo].[Computers] ([ID])
GO
ALTER TABLE [dbo].[Services] CHECK CONSTRAINT [FK_Services_Computers]
GO
ALTER TABLE [dbo].[Services]  WITH CHECK ADD  CONSTRAINT [FK_Services_Employees] FOREIGN KEY([EmployeeID])
REFERENCES [dbo].[Employees] ([ID])
GO
ALTER TABLE [dbo].[Services] CHECK CONSTRAINT [FK_Services_Employees]
GO
