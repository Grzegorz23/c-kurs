﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GrzegorzMaciejewskiLab3.MODEL
{
    class Computer
    {
        /// Metoda która wyświetla pola z tabel wpisanych jako argumenty konstruktora SqlDataAdapter
        /// wyświetla wybrane pola z tabel do DataGridView
        // 
        public static void GetAllComputers(SqlConnection sqlConnection, SqlDataAdapter sqlDataAdapter, DataGridView dataGridView)
        {
            dataGridView.DataSource = null;
            sqlDataAdapter = new SqlDataAdapter("Select ID as Identyfikator_komputera, Producer as Producent,model from Computers ", sqlConnection);
            DataTable dataTable = new DataTable();
            sqlDataAdapter.Fill(dataTable);
            dataGridView.DataSource = dataTable;

        }
    }
}
