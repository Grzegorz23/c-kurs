﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GrzegorzMaciejewskiLab3.MODEL
{
    class Client
    {   
        /// Metoda która wyświetla pola z tabel wpisanych jako argumenty konstruktora SqlDataAdapter
        /// wyświetla wybrane pola z tabel do DataGridView
        // 
        public static void GetAllClients(SqlConnection sqlConnection, SqlDataAdapter sqlDataAdapter, DataGridView dataGridView)
        {
            dataGridView.DataSource = null;
            sqlDataAdapter = new SqlDataAdapter("Select name as Imie,surname as Nazwisko, ComputerID as ID_komputera from Clients ", sqlConnection);
            DataTable dataTable = new DataTable();
            sqlDataAdapter.Fill(dataTable);
            dataGridView.DataSource = dataTable;

        }
        
    }
}
