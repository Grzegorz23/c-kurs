﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GrzegorzMaciejewskiLab3.MODEL
{
    class Service
    {
        /// Metoda która wyświetla pola z tabel wpisanych jako argumenty konstruktora SqlDataAdapter
        /// wyświetla wybrane pola z tabel do DataGridView
        // 
        public static void GetAllServices(SqlConnection sqlConnection, SqlDataAdapter sqlDataAdapter, DataGridView dataGridView,String condition)
        {
            dataGridView.DataSource = null;
            sqlDataAdapter = new SqlDataAdapter("Select s.ID as Identyfikator_naprawy,name as Imie_serwisanta, surname as Nazwisko_serwisanta,cost as Koszt, description as Opis from Services s join Employees e on s.EmployeeID=e.ID where cost>"+ condition, sqlConnection);
            DataTable dataTable = new DataTable();
            sqlDataAdapter.Fill(dataTable);
            dataGridView.DataSource = dataTable;

        }
    }
}
