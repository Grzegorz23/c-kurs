﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GrzegorzMaciejewskiLab3.MODEL;

namespace GrzegorzMaciejewskiLab3
{
    public partial class FormComputerService : Form
    {

        SqlConnection sqlConnection; // połączenie z bazą danych
        SqlDataAdapter sqlDataAdapter;

        ///Przy inicjalizacji okna odrazu łączymy się z bazą danych
        //
        public FormComputerService()
        {
            InitializeComponent();
            sqlConnection = new SqlConnection("Data Source= GRZEGORZ-KOMP\\SQLEXPRESS; database = GrzegorzMaciejewskiLab3ComputerService; Trusted_Connection=yes");
        }

        ///Guzik wyświetlający wszystkich pracowników z pomocą metody z klasy Employee
        //
         private void buttonDisplayServices_Click(object sender, EventArgs e)
        {
         Service.GetAllServices(sqlConnection, sqlDataAdapter, dataGridViewComputerService, "0");
        }
        ///Guzik wyświetlający wszystie komputery które były serwisowane z pomocą metody z klasy Computer
        //
        private void buttonDisplayComputers_Click(object sender, EventArgs e)
        {
            Computer.GetAllComputers(sqlConnection, sqlDataAdapter, dataGridViewComputerService);
        }
        ///Guzik wyświetlający wszystkich klientów z pomocą metody z klasy Client
        //
        private void buttonDisplayClients_Click(object sender, EventArgs e)
        {
            Client.GetAllClients(sqlConnection, sqlDataAdapter, dataGridViewComputerService);
        }
        ///Guzik wyświetlający wykonane usługi które spełniają podane w TextBoxie kryterium cenowe
        //
        private void buttonDislpayServicesWithCertainCost_Click(object sender, EventArgs e)
        {
            Service.GetAllServices(sqlConnection, sqlDataAdapter, dataGridViewComputerService, textBoxCondition.Text);
        }
        ///Guzik wyświetlający wszystkich pracowników z pomocą metody z klasy Employee, 
        //
        private void buttonDisplayEmployees_Click(object sender, EventArgs e)
        {
            String condition = textBoxCondition.Text;
            Employee.GetAllEmployees(sqlConnection, sqlDataAdapter, dataGridViewComputerService);
        }
    }
}
