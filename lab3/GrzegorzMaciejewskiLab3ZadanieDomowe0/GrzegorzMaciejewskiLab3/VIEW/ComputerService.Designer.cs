﻿namespace GrzegorzMaciejewskiLab3
{
    partial class FormComputerService
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewComputerService = new System.Windows.Forms.DataGridView();
            this.labelName = new System.Windows.Forms.Label();
            this.buttonDisplayEmployees = new System.Windows.Forms.Button();
            this.textBoxCondition = new System.Windows.Forms.TextBox();
            this.buttonDisplayServices = new System.Windows.Forms.Button();
            this.buttonDisplayComputers = new System.Windows.Forms.Button();
            this.buttonDisplayClients = new System.Windows.Forms.Button();
            this.buttonDislpayServicesWithCertainCost = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewComputerService)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewComputerService
            // 
            this.dataGridViewComputerService.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewComputerService.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedHeaders;
            this.dataGridViewComputerService.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridViewComputerService.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridViewComputerService.Cursor = System.Windows.Forms.Cursors.Default;
            this.dataGridViewComputerService.Location = new System.Drawing.Point(306, 12);
            this.dataGridViewComputerService.Name = "dataGridViewComputerService";
            this.dataGridViewComputerService.Size = new System.Drawing.Size(517, 482);
            this.dataGridViewComputerService.TabIndex = 0;
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Font = new System.Drawing.Font("Microsoft YaHei UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelName.Location = new System.Drawing.Point(41, 49);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(242, 26);
            this.labelName.TabIndex = 1;
            this.labelName.Text = "Grzegorz Maciejewski";
            // 
            // buttonDisplayEmployees
            // 
            this.buttonDisplayEmployees.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonDisplayEmployees.Location = new System.Drawing.Point(182, 96);
            this.buttonDisplayEmployees.Name = "buttonDisplayEmployees";
            this.buttonDisplayEmployees.Size = new System.Drawing.Size(91, 44);
            this.buttonDisplayEmployees.TabIndex = 2;
            this.buttonDisplayEmployees.Text = "Pokaż pracowników";
            this.buttonDisplayEmployees.UseVisualStyleBackColor = true;
            this.buttonDisplayEmployees.Click += new System.EventHandler(this.buttonDisplayEmployees_Click);
            // 
            // textBoxCondition
            // 
            this.textBoxCondition.Font = new System.Drawing.Font("Modern No. 20", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCondition.Location = new System.Drawing.Point(76, 146);
            this.textBoxCondition.Multiline = true;
            this.textBoxCondition.Name = "textBoxCondition";
            this.textBoxCondition.Size = new System.Drawing.Size(100, 36);
            this.textBoxCondition.TabIndex = 3;
            this.textBoxCondition.Text = "0";
            this.textBoxCondition.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // buttonDisplayServices
            // 
            this.buttonDisplayServices.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonDisplayServices.Location = new System.Drawing.Point(182, 146);
            this.buttonDisplayServices.Name = "buttonDisplayServices";
            this.buttonDisplayServices.Size = new System.Drawing.Size(91, 52);
            this.buttonDisplayServices.TabIndex = 4;
            this.buttonDisplayServices.Text = "Pokaż wykonane serwisy";
            this.buttonDisplayServices.UseVisualStyleBackColor = true;
            this.buttonDisplayServices.Click += new System.EventHandler(this.buttonDisplayServices_Click);
            // 
            // buttonDisplayComputers
            // 
            this.buttonDisplayComputers.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonDisplayComputers.Location = new System.Drawing.Point(182, 204);
            this.buttonDisplayComputers.Name = "buttonDisplayComputers";
            this.buttonDisplayComputers.Size = new System.Drawing.Size(91, 39);
            this.buttonDisplayComputers.TabIndex = 5;
            this.buttonDisplayComputers.Text = "Pokaż komputery";
            this.buttonDisplayComputers.UseVisualStyleBackColor = true;
            this.buttonDisplayComputers.Click += new System.EventHandler(this.buttonDisplayComputers_Click);
            // 
            // buttonDisplayClients
            // 
            this.buttonDisplayClients.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonDisplayClients.Location = new System.Drawing.Point(182, 249);
            this.buttonDisplayClients.Name = "buttonDisplayClients";
            this.buttonDisplayClients.Size = new System.Drawing.Size(91, 41);
            this.buttonDisplayClients.TabIndex = 6;
            this.buttonDisplayClients.Text = "Pokaż klientów";
            this.buttonDisplayClients.UseVisualStyleBackColor = true;
            this.buttonDisplayClients.Click += new System.EventHandler(this.buttonDisplayClients_Click);
            // 
            // buttonDislpayServicesWithCertainCost
            // 
            this.buttonDislpayServicesWithCertainCost.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonDislpayServicesWithCertainCost.Location = new System.Drawing.Point(76, 96);
            this.buttonDislpayServicesWithCertainCost.Name = "buttonDislpayServicesWithCertainCost";
            this.buttonDislpayServicesWithCertainCost.Size = new System.Drawing.Size(100, 44);
            this.buttonDislpayServicesWithCertainCost.TabIndex = 7;
            this.buttonDislpayServicesWithCertainCost.Text = "Pokaż naprawy droższe niż :";
            this.buttonDislpayServicesWithCertainCost.UseVisualStyleBackColor = true;
            this.buttonDislpayServicesWithCertainCost.Click += new System.EventHandler(this.buttonDislpayServicesWithCertainCost_Click);
            // 
            // FormComputerService
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.HotTrack;
            this.ClientSize = new System.Drawing.Size(835, 506);
            this.Controls.Add(this.buttonDislpayServicesWithCertainCost);
            this.Controls.Add(this.buttonDisplayClients);
            this.Controls.Add(this.buttonDisplayComputers);
            this.Controls.Add(this.buttonDisplayServices);
            this.Controls.Add(this.textBoxCondition);
            this.Controls.Add(this.buttonDisplayEmployees);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.dataGridViewComputerService);
            this.Name = "FormComputerService";
            this.Text = "ComputerService";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewComputerService)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewComputerService;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.Button buttonDisplayEmployees;
        private System.Windows.Forms.TextBox textBoxCondition;
        private System.Windows.Forms.Button buttonDisplayServices;
        private System.Windows.Forms.Button buttonDisplayComputers;
        private System.Windows.Forms.Button buttonDisplayClients;
        private System.Windows.Forms.Button buttonDislpayServicesWithCertainCost;
    }
}

