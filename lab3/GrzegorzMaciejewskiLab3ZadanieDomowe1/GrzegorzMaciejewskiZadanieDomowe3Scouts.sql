USE [GrzegorzMaciejewskiZadanieDomowe3Scouts]
GO
/****** Object:  Table [dbo].[Achievements]    Script Date: 2016-04-27 00:32:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Achievements](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[IDHarcerza] [int] NOT NULL,
	[Sprawnosc] [varchar](50) NOT NULL,
	[Ilosc_Gwiazdek] [int] NOT NULL,
 CONSTRAINT [PK_Sprawnosci] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Camps]    Script Date: 2016-04-27 00:32:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Camps](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[IDZastepu] [int] NOT NULL,
	[MiejsceBiwaku] [varchar](50) NOT NULL,
	[IloscDni] [int] NOT NULL,
 CONSTRAINT [PK_Biwaki] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Circles]    Script Date: 2016-04-27 00:32:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Circles](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Nazwa] [varchar](50) NOT NULL,
	[Przewodniczacy] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Okregi] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Districts]    Script Date: 2016-04-27 00:32:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Districts](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[IDChoragwi] [int] NOT NULL,
	[Nazwa] [varchar](50) NOT NULL,
	[Hufcowy] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Hufce] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Groups]    Script Date: 2016-04-27 00:32:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Groups](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[IDHufca] [int] NOT NULL,
	[Nazwa] [varchar](50) NOT NULL,
	[Drużynowy] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Drużyny] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[HolidayCamps]    Script Date: 2016-04-27 00:32:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HolidayCamps](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[IDDruzyny] [int] NOT NULL,
	[Miejsce] [varchar](50) NOT NULL,
	[Rok] [int] NOT NULL,
 CONSTRAINT [PK_Obozy] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MoneyLogs]    Script Date: 2016-04-27 00:32:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MoneyLogs](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[IDDruzyny] [int] NOT NULL,
	[Data] [date] NOT NULL,
	[Składka] [float] NOT NULL,
 CONSTRAINT [PK_Skladki] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Patrols]    Script Date: 2016-04-27 00:32:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Patrols](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[IDDruzyny] [int] NOT NULL,
	[NazwaZastepu] [varchar](50) NOT NULL,
	[Zastepowy] [varchar](50) NOT NULL,
	[Okrzyk] [bit] NOT NULL,
 CONSTRAINT [PK_Zastepy] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RegionBanners]    Script Date: 2016-04-27 00:32:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RegionBanners](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[IDOkregu] [int] NOT NULL,
	[Nazwa] [varchar](50) NOT NULL,
	[Komendant(ka)] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Choragwie] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Scouts]    Script Date: 2016-04-27 00:32:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Scouts](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[IDZastepu] [int] NOT NULL,
	[Imie] [varchar](50) NOT NULL,
	[Nazwisko] [varchar](50) NOT NULL,
	[Stopien] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Harcerze] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tournaments]    Script Date: 2016-04-27 00:32:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tournaments](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[IDChoragwi] [int] NOT NULL,
	[Turniej] [varchar](50) NOT NULL,
	[opis] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Turnieje] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Uniforms]    Script Date: 2016-04-27 00:32:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Uniforms](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[IDDruzyny] [int] NOT NULL,
	[NakrycieGlowy] [varchar](50) NOT NULL,
	[KolorMunduru] [varchar](50) NOT NULL,
	[DlugoscChusty] [int] NOT NULL,
 CONSTRAINT [PK_Umundurowanie] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Achievements] ON 

INSERT [dbo].[Achievements] ([ID], [IDHarcerza], [Sprawnosc], [Ilosc_Gwiazdek]) VALUES (1, 1, N'Sobieradek', 1)
INSERT [dbo].[Achievements] ([ID], [IDHarcerza], [Sprawnosc], [Ilosc_Gwiazdek]) VALUES (2, 2, N'Sobieradek', 1)
INSERT [dbo].[Achievements] ([ID], [IDHarcerza], [Sprawnosc], [Ilosc_Gwiazdek]) VALUES (4, 3, N'Sobieradek', 1)
INSERT [dbo].[Achievements] ([ID], [IDHarcerza], [Sprawnosc], [Ilosc_Gwiazdek]) VALUES (5, 1, N'Mistrz Musztry', 3)
INSERT [dbo].[Achievements] ([ID], [IDHarcerza], [Sprawnosc], [Ilosc_Gwiazdek]) VALUES (6, 4, N'Alfa i Omega', 3)
INSERT [dbo].[Achievements] ([ID], [IDHarcerza], [Sprawnosc], [Ilosc_Gwiazdek]) VALUES (7, 5, N'Lekka Stopa', 1)
INSERT [dbo].[Achievements] ([ID], [IDHarcerza], [Sprawnosc], [Ilosc_Gwiazdek]) VALUES (8, 6, N'Samarytanin', 2)
INSERT [dbo].[Achievements] ([ID], [IDHarcerza], [Sprawnosc], [Ilosc_Gwiazdek]) VALUES (9, 4, N'Tarcza św jerzego', 1)
INSERT [dbo].[Achievements] ([ID], [IDHarcerza], [Sprawnosc], [Ilosc_Gwiazdek]) VALUES (10, 7, N'Pomocnik', 2)
INSERT [dbo].[Achievements] ([ID], [IDHarcerza], [Sprawnosc], [Ilosc_Gwiazdek]) VALUES (11, 13, N'Wyga', 3)
INSERT [dbo].[Achievements] ([ID], [IDHarcerza], [Sprawnosc], [Ilosc_Gwiazdek]) VALUES (12, 12, N'Dromader', 3)
INSERT [dbo].[Achievements] ([ID], [IDHarcerza], [Sprawnosc], [Ilosc_Gwiazdek]) VALUES (13, 11, N'Strażnik Ognia', 2)
INSERT [dbo].[Achievements] ([ID], [IDHarcerza], [Sprawnosc], [Ilosc_Gwiazdek]) VALUES (14, 5, N'Ogniomistrz', 3)
INSERT [dbo].[Achievements] ([ID], [IDHarcerza], [Sprawnosc], [Ilosc_Gwiazdek]) VALUES (15, 1, N' Mistrz Musztry', 2)
INSERT [dbo].[Achievements] ([ID], [IDHarcerza], [Sprawnosc], [Ilosc_Gwiazdek]) VALUES (16, 2, N'Instruktor Musztry', 3)
INSERT [dbo].[Achievements] ([ID], [IDHarcerza], [Sprawnosc], [Ilosc_Gwiazdek]) VALUES (17, 1, N'Majsterkowicz', 2)
SET IDENTITY_INSERT [dbo].[Achievements] OFF
SET IDENTITY_INSERT [dbo].[Camps] ON 

INSERT [dbo].[Camps] ([ID], [IDZastepu], [MiejsceBiwaku], [IloscDni]) VALUES (1, 1, N'Las', 3)
INSERT [dbo].[Camps] ([ID], [IDZastepu], [MiejsceBiwaku], [IloscDni]) VALUES (2, 2, N'Las', 2)
INSERT [dbo].[Camps] ([ID], [IDZastepu], [MiejsceBiwaku], [IloscDni]) VALUES (3, 3, N'Nad Rzeczką', 2)
INSERT [dbo].[Camps] ([ID], [IDZastepu], [MiejsceBiwaku], [IloscDni]) VALUES (4, 2, N'Świetlica Wiejska', 4)
INSERT [dbo].[Camps] ([ID], [IDZastepu], [MiejsceBiwaku], [IloscDni]) VALUES (6, 7, N'Zagajnik', 2)
INSERT [dbo].[Camps] ([ID], [IDZastepu], [MiejsceBiwaku], [IloscDni]) VALUES (7, 4, N'Jezioro', 3)
INSERT [dbo].[Camps] ([ID], [IDZastepu], [MiejsceBiwaku], [IloscDni]) VALUES (8, 1, N'Las', 4)
INSERT [dbo].[Camps] ([ID], [IDZastepu], [MiejsceBiwaku], [IloscDni]) VALUES (9, 5, N'Jaz Niezgoda', 2)
SET IDENTITY_INSERT [dbo].[Camps] OFF
SET IDENTITY_INSERT [dbo].[Circles] ON 

INSERT [dbo].[Circles] ([ID], [Nazwa], [Przewodniczacy]) VALUES (1, N'Okręg Dolnoslaski', N'Patryk Mak')
INSERT [dbo].[Circles] ([ID], [Nazwa], [Przewodniczacy]) VALUES (2, N'Okręg Mazowiecki', N'Robert Cych')
SET IDENTITY_INSERT [dbo].[Circles] OFF
SET IDENTITY_INSERT [dbo].[Districts] ON 

INSERT [dbo].[Districts] ([ID], [IDChoragwi], [Nazwa], [Hufcowy]) VALUES (1, 1, N'Starodrzew', N'Maciej Bark')
INSERT [dbo].[Districts] ([ID], [IDChoragwi], [Nazwa], [Hufcowy]) VALUES (2, 1, N'Nowodrzew', N'Dżon Rambo')
INSERT [dbo].[Districts] ([ID], [IDChoragwi], [Nazwa], [Hufcowy]) VALUES (3, 2, N'Klementynki', N'Ania Be')
INSERT [dbo].[Districts] ([ID], [IDChoragwi], [Nazwa], [Hufcowy]) VALUES (4, 2, N'Nowowolowo', N'Wololololo')
INSERT [dbo].[Districts] ([ID], [IDChoragwi], [Nazwa], [Hufcowy]) VALUES (5, 3, N'Mazowczaki', N'Maciek OfClan')
INSERT [dbo].[Districts] ([ID], [IDChoragwi], [Nazwa], [Hufcowy]) VALUES (6, 3, N'Kasztaniaki', N'Kolololo Wolol')
INSERT [dbo].[Districts] ([ID], [IDChoragwi], [Nazwa], [Hufcowy]) VALUES (7, 4, N'Zielone', N'Ania Zielona')
SET IDENTITY_INSERT [dbo].[Districts] OFF
SET IDENTITY_INSERT [dbo].[Groups] ON 

INSERT [dbo].[Groups] ([ID], [IDHufca], [Nazwa], [Drużynowy]) VALUES (1, 1, N'Gawra', N'Adam Kwaśniewski')
INSERT [dbo].[Groups] ([ID], [IDHufca], [Nazwa], [Drużynowy]) VALUES (2, 1, N'Druzyna2', N'Druzynowy2')
INSERT [dbo].[Groups] ([ID], [IDHufca], [Nazwa], [Drużynowy]) VALUES (5, 2, N'Druzyna3', N'Druzynowy2')
INSERT [dbo].[Groups] ([ID], [IDHufca], [Nazwa], [Drużynowy]) VALUES (6, 2, N'Knieja', N'Adam Garnecki')
INSERT [dbo].[Groups] ([ID], [IDHufca], [Nazwa], [Drużynowy]) VALUES (7, 5, N'Iguana', N'Sandra Gieniusz')
INSERT [dbo].[Groups] ([ID], [IDHufca], [Nazwa], [Drużynowy]) VALUES (8, 5, N'Convivera', N'Monika Kiermut')
INSERT [dbo].[Groups] ([ID], [IDHufca], [Nazwa], [Drużynowy]) VALUES (9, 6, N'Druzyna7', N'Druzynowa7')
INSERT [dbo].[Groups] ([ID], [IDHufca], [Nazwa], [Drużynowy]) VALUES (10, 6, N'Druzyna8', N'Druzynowa8')
SET IDENTITY_INSERT [dbo].[Groups] OFF
SET IDENTITY_INSERT [dbo].[HolidayCamps] ON 

INSERT [dbo].[HolidayCamps] ([ID], [IDDruzyny], [Miejsce], [Rok]) VALUES (1, 1, N'Jezioro', 2002)
INSERT [dbo].[HolidayCamps] ([ID], [IDDruzyny], [Miejsce], [Rok]) VALUES (2, 1, N'Las', 2003)
INSERT [dbo].[HolidayCamps] ([ID], [IDDruzyny], [Miejsce], [Rok]) VALUES (3, 6, N'Góry', 2002)
INSERT [dbo].[HolidayCamps] ([ID], [IDDruzyny], [Miejsce], [Rok]) VALUES (4, 8, N'Wędrowny', 2001)
INSERT [dbo].[HolidayCamps] ([ID], [IDDruzyny], [Miejsce], [Rok]) VALUES (5, 6, N'Połoniny', 2005)
INSERT [dbo].[HolidayCamps] ([ID], [IDDruzyny], [Miejsce], [Rok]) VALUES (6, 7, N'Machov', 2004)
INSERT [dbo].[HolidayCamps] ([ID], [IDDruzyny], [Miejsce], [Rok]) VALUES (7, 1, N'Machov', 2004)
INSERT [dbo].[HolidayCamps] ([ID], [IDDruzyny], [Miejsce], [Rok]) VALUES (8, 8, N'Machov', 2004)
SET IDENTITY_INSERT [dbo].[HolidayCamps] OFF
SET IDENTITY_INSERT [dbo].[MoneyLogs] ON 

INSERT [dbo].[MoneyLogs] ([ID], [IDDruzyny], [Data], [Składka]) VALUES (1, 2, CAST(N'2009-05-01' AS Date), 100)
INSERT [dbo].[MoneyLogs] ([ID], [IDDruzyny], [Data], [Składka]) VALUES (2, 2, CAST(N'2009-06-01' AS Date), 500)
INSERT [dbo].[MoneyLogs] ([ID], [IDDruzyny], [Data], [Składka]) VALUES (4, 5, CAST(N'2010-04-01' AS Date), 23)
INSERT [dbo].[MoneyLogs] ([ID], [IDDruzyny], [Data], [Składka]) VALUES (5, 6, CAST(N'2010-03-01' AS Date), 200)
INSERT [dbo].[MoneyLogs] ([ID], [IDDruzyny], [Data], [Składka]) VALUES (6, 7, CAST(N'2010-07-01' AS Date), 210)
INSERT [dbo].[MoneyLogs] ([ID], [IDDruzyny], [Data], [Składka]) VALUES (7, 1, CAST(N'2011-08-01' AS Date), 300)
SET IDENTITY_INSERT [dbo].[MoneyLogs] OFF
SET IDENTITY_INSERT [dbo].[Patrols] ON 

INSERT [dbo].[Patrols] ([ID], [IDDruzyny], [NazwaZastepu], [Zastepowy], [Okrzyk]) VALUES (1, 1, N'BiałeOrły', N'Patryk Czara', 1)
INSERT [dbo].[Patrols] ([ID], [IDDruzyny], [NazwaZastepu], [Zastepowy], [Okrzyk]) VALUES (2, 1, N'Halarctos', N'Piotr Zbir', 0)
INSERT [dbo].[Patrols] ([ID], [IDDruzyny], [NazwaZastepu], [Zastepowy], [Okrzyk]) VALUES (3, 1, N'Arctosses', N'Marcin Akac', 1)
INSERT [dbo].[Patrols] ([ID], [IDDruzyny], [NazwaZastepu], [Zastepowy], [Okrzyk]) VALUES (4, 7, N'Misie', N'Dżordż żak', 0)
INSERT [dbo].[Patrols] ([ID], [IDDruzyny], [NazwaZastepu], [Zastepowy], [Okrzyk]) VALUES (5, 8, N'Urszulki', N'Patrycja Wnuk', 1)
INSERT [dbo].[Patrols] ([ID], [IDDruzyny], [NazwaZastepu], [Zastepowy], [Okrzyk]) VALUES (7, 8, N'Martynki', N'Ania Wróblewska', 1)
SET IDENTITY_INSERT [dbo].[Patrols] OFF
SET IDENTITY_INSERT [dbo].[RegionBanners] ON 

INSERT [dbo].[RegionBanners] ([ID], [IDOkregu], [Nazwa], [Komendant(ka)]) VALUES (1, 1, N'Dolnoslaska Chorągiew Harcerzy', N'Adrian Bombała')
INSERT [dbo].[RegionBanners] ([ID], [IDOkregu], [Nazwa], [Komendant(ka)]) VALUES (2, 1, N'Dolnoslaska Chorągiew Harcerek', N'Marzena Bunic')
INSERT [dbo].[RegionBanners] ([ID], [IDOkregu], [Nazwa], [Komendant(ka)]) VALUES (3, 2, N'Mazowiecka Choragiew Harcerzy', N'Marcin Ug')
INSERT [dbo].[RegionBanners] ([ID], [IDOkregu], [Nazwa], [Komendant(ka)]) VALUES (4, 2, N'Mazowiecka Chorągiew Harcerek', N'Renata Gil')
SET IDENTITY_INSERT [dbo].[RegionBanners] OFF
SET IDENTITY_INSERT [dbo].[Scouts] ON 

INSERT [dbo].[Scouts] ([ID], [IDZastepu], [Imie], [Nazwisko], [Stopien]) VALUES (1, 2, N'Grzegorz', N'Maciejewski', N'HR')
INSERT [dbo].[Scouts] ([ID], [IDZastepu], [Imie], [Nazwisko], [Stopien]) VALUES (2, 2, N'Marcin', N'Niebudek', N'HR')
INSERT [dbo].[Scouts] ([ID], [IDZastepu], [Imie], [Nazwisko], [Stopien]) VALUES (3, 2, N'Adam', N'Kwas', N'HO')
INSERT [dbo].[Scouts] ([ID], [IDZastepu], [Imie], [Nazwisko], [Stopien]) VALUES (4, 3, N'Dżordż', N'Żekel', N'Ćwik')
INSERT [dbo].[Scouts] ([ID], [IDZastepu], [Imie], [Nazwisko], [Stopien]) VALUES (5, 3, N'Man', N'En', N'Ćwik')
INSERT [dbo].[Scouts] ([ID], [IDZastepu], [Imie], [Nazwisko], [Stopien]) VALUES (6, 5, N'Katarzyna', N'Okoń', N'Samarytanka')
INSERT [dbo].[Scouts] ([ID], [IDZastepu], [Imie], [Nazwisko], [Stopien]) VALUES (7, 5, N'Ania', N'Kac', N'Ochotniczka')
INSERT [dbo].[Scouts] ([ID], [IDZastepu], [Imie], [Nazwisko], [Stopien]) VALUES (8, 7, N'Katarzyna', N'Kuc', N'Samarytanka')
INSERT [dbo].[Scouts] ([ID], [IDZastepu], [Imie], [Nazwisko], [Stopien]) VALUES (9, 7, N'Urszula', N'Maj', N'Ochotniczka')
INSERT [dbo].[Scouts] ([ID], [IDZastepu], [Imie], [Nazwisko], [Stopien]) VALUES (10, 7, N'Pamela', N'Deska', N'Wędrowniczka')
INSERT [dbo].[Scouts] ([ID], [IDZastepu], [Imie], [Nazwisko], [Stopien]) VALUES (11, 1, N'Dżon', N'Rambo', N'Biszkopt')
INSERT [dbo].[Scouts] ([ID], [IDZastepu], [Imie], [Nazwisko], [Stopien]) VALUES (12, 1, N'Andrzej', N'Makowiec', N'Biszkopt')
INSERT [dbo].[Scouts] ([ID], [IDZastepu], [Imie], [Nazwisko], [Stopien]) VALUES (13, 1, N'Patryk', N'Kulawiec', N'Biszkopt')
INSERT [dbo].[Scouts] ([ID], [IDZastepu], [Imie], [Nazwisko], [Stopien]) VALUES (14, 4, N'Kasia', N'Kafel', N'Samarytanka')
INSERT [dbo].[Scouts] ([ID], [IDZastepu], [Imie], [Nazwisko], [Stopien]) VALUES (15, 4, N'Asia', N'Kał', N'Ochotniczka')
INSERT [dbo].[Scouts] ([ID], [IDZastepu], [Imie], [Nazwisko], [Stopien]) VALUES (16, 4, N'Patrycja', N'Pat', N'Wędrowniczka')
SET IDENTITY_INSERT [dbo].[Scouts] OFF
SET IDENTITY_INSERT [dbo].[Tournaments] ON 

INSERT [dbo].[Tournaments] ([ID], [IDChoragwi], [Turniej], [opis]) VALUES (1, 1, N'Turniej zastępow', N'św jerzy')
INSERT [dbo].[Tournaments] ([ID], [IDChoragwi], [Turniej], [opis]) VALUES (2, 2, N'Turniej drużyn', N'turniej puszczański')
INSERT [dbo].[Tournaments] ([ID], [IDChoragwi], [Turniej], [opis]) VALUES (3, 3, N'Turniej Druzyn', N'turniej o tytuł')
INSERT [dbo].[Tournaments] ([ID], [IDChoragwi], [Turniej], [opis]) VALUES (4, 4, N'Rozgrywki zastępow', N'tytuł najlepszego zastępu')
INSERT [dbo].[Tournaments] ([ID], [IDChoragwi], [Turniej], [opis]) VALUES (5, 1, N'Turniej o szyszki', N'rozgrywki o złotą szyszke')
INSERT [dbo].[Tournaments] ([ID], [IDChoragwi], [Turniej], [opis]) VALUES (6, 3, N'Marcinkowo', N'Turniej św Marcina')
SET IDENTITY_INSERT [dbo].[Tournaments] OFF
SET IDENTITY_INSERT [dbo].[Uniforms] ON 

INSERT [dbo].[Uniforms] ([ID], [IDDruzyny], [NakrycieGlowy], [KolorMunduru], [DlugoscChusty]) VALUES (1, 1, N'Beret', N'Zielony', 20)
INSERT [dbo].[Uniforms] ([ID], [IDDruzyny], [NakrycieGlowy], [KolorMunduru], [DlugoscChusty]) VALUES (2, 2, N'Kapelusz', N'Zielony', 20)
INSERT [dbo].[Uniforms] ([ID], [IDDruzyny], [NakrycieGlowy], [KolorMunduru], [DlugoscChusty]) VALUES (3, 6, N'Kapelusz', N'szary', 30)
INSERT [dbo].[Uniforms] ([ID], [IDDruzyny], [NakrycieGlowy], [KolorMunduru], [DlugoscChusty]) VALUES (5, 6, N'Rogatywka', N'Niebieski', 10)
INSERT [dbo].[Uniforms] ([ID], [IDDruzyny], [NakrycieGlowy], [KolorMunduru], [DlugoscChusty]) VALUES (6, 7, N'Rogatywka', N'Zielony', 20)
INSERT [dbo].[Uniforms] ([ID], [IDDruzyny], [NakrycieGlowy], [KolorMunduru], [DlugoscChusty]) VALUES (7, 8, N'Kapelusz', N'Niebieski', 20)
INSERT [dbo].[Uniforms] ([ID], [IDDruzyny], [NakrycieGlowy], [KolorMunduru], [DlugoscChusty]) VALUES (8, 7, N'Kapelusz', N'Szary', 30)
INSERT [dbo].[Uniforms] ([ID], [IDDruzyny], [NakrycieGlowy], [KolorMunduru], [DlugoscChusty]) VALUES (9, 2, N'Kapelusz', N'Zielony', 20)
SET IDENTITY_INSERT [dbo].[Uniforms] OFF
ALTER TABLE [dbo].[Achievements]  WITH CHECK ADD  CONSTRAINT [FK_Sprawnosci_Harcerze] FOREIGN KEY([IDHarcerza])
REFERENCES [dbo].[Scouts] ([ID])
GO
ALTER TABLE [dbo].[Achievements] CHECK CONSTRAINT [FK_Sprawnosci_Harcerze]
GO
ALTER TABLE [dbo].[Camps]  WITH CHECK ADD  CONSTRAINT [FK_Biwaki_Zastepy] FOREIGN KEY([IDZastepu])
REFERENCES [dbo].[Patrols] ([ID])
GO
ALTER TABLE [dbo].[Camps] CHECK CONSTRAINT [FK_Biwaki_Zastepy]
GO
ALTER TABLE [dbo].[Districts]  WITH CHECK ADD  CONSTRAINT [FK_Hufce_Choragwie] FOREIGN KEY([IDChoragwi])
REFERENCES [dbo].[RegionBanners] ([ID])
GO
ALTER TABLE [dbo].[Districts] CHECK CONSTRAINT [FK_Hufce_Choragwie]
GO
ALTER TABLE [dbo].[Groups]  WITH CHECK ADD  CONSTRAINT [FK_Drużyny_Hufce] FOREIGN KEY([IDHufca])
REFERENCES [dbo].[Districts] ([ID])
GO
ALTER TABLE [dbo].[Groups] CHECK CONSTRAINT [FK_Drużyny_Hufce]
GO
ALTER TABLE [dbo].[HolidayCamps]  WITH CHECK ADD  CONSTRAINT [FK_Obozy_Drużyny] FOREIGN KEY([IDDruzyny])
REFERENCES [dbo].[Groups] ([ID])
GO
ALTER TABLE [dbo].[HolidayCamps] CHECK CONSTRAINT [FK_Obozy_Drużyny]
GO
ALTER TABLE [dbo].[MoneyLogs]  WITH CHECK ADD  CONSTRAINT [FK_MoneyLogs_Groups] FOREIGN KEY([IDDruzyny])
REFERENCES [dbo].[Groups] ([ID])
GO
ALTER TABLE [dbo].[MoneyLogs] CHECK CONSTRAINT [FK_MoneyLogs_Groups]
GO
ALTER TABLE [dbo].[Patrols]  WITH CHECK ADD  CONSTRAINT [FK_Zastepy_Drużyny] FOREIGN KEY([IDDruzyny])
REFERENCES [dbo].[Groups] ([ID])
GO
ALTER TABLE [dbo].[Patrols] CHECK CONSTRAINT [FK_Zastepy_Drużyny]
GO
ALTER TABLE [dbo].[RegionBanners]  WITH CHECK ADD  CONSTRAINT [FK_Choragwie_Okregi] FOREIGN KEY([IDOkregu])
REFERENCES [dbo].[Circles] ([ID])
GO
ALTER TABLE [dbo].[RegionBanners] CHECK CONSTRAINT [FK_Choragwie_Okregi]
GO
ALTER TABLE [dbo].[Scouts]  WITH CHECK ADD  CONSTRAINT [FK_Harcerze_Zastepy1] FOREIGN KEY([IDZastepu])
REFERENCES [dbo].[Patrols] ([ID])
GO
ALTER TABLE [dbo].[Scouts] CHECK CONSTRAINT [FK_Harcerze_Zastepy1]
GO
ALTER TABLE [dbo].[Tournaments]  WITH CHECK ADD  CONSTRAINT [FK_Turnieje_Choragwie] FOREIGN KEY([IDChoragwi])
REFERENCES [dbo].[RegionBanners] ([ID])
GO
ALTER TABLE [dbo].[Tournaments] CHECK CONSTRAINT [FK_Turnieje_Choragwie]
GO
ALTER TABLE [dbo].[Uniforms]  WITH CHECK ADD  CONSTRAINT [FK_Umundurowanie_Drużyny] FOREIGN KEY([IDDruzyny])
REFERENCES [dbo].[Groups] ([ID])
GO
ALTER TABLE [dbo].[Uniforms] CHECK CONSTRAINT [FK_Umundurowanie_Drużyny]
GO
