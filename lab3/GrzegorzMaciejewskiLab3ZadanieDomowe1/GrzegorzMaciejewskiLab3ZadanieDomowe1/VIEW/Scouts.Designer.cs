﻿namespace GrzegorzMaciejewskiLab3ZadanieDomowe1
{
    partial class Form1Scouts
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewDisplayFromDatabase = new System.Windows.Forms.DataGridView();
            this.buttonScouts = new System.Windows.Forms.Button();
            this.buttonPatrols = new System.Windows.Forms.Button();
            this.buttonGroups = new System.Windows.Forms.Button();
            this.buttonDistricts = new System.Windows.Forms.Button();
            this.buttonMoneyLog = new System.Windows.Forms.Button();
            this.buttonHolidayCamps = new System.Windows.Forms.Button();
            this.buttonGroupsAndScoutsByColor = new System.Windows.Forms.Button();
            this.buttonTournaments = new System.Windows.Forms.Button();
            this.buttonCircles = new System.Windows.Forms.Button();
            this.buttonCamps = new System.Windows.Forms.Button();
            this.buttonScoutsAchievements = new System.Windows.Forms.Button();
            this.buttonGroupsAndScoutsByHeadgear = new System.Windows.Forms.Button();
            this.labelInformationAboutButtons = new System.Windows.Forms.Label();
            this.textBoxUniformColor = new System.Windows.Forms.TextBox();
            this.labelTextBoxColorInfo = new System.Windows.Forms.Label();
            this.textBoxHeadgearType = new System.Windows.Forms.TextBox();
            this.labelInfoAboutAvaiableHeadhear = new System.Windows.Forms.Label();
            this.labelInfoAboutStars = new System.Windows.Forms.Label();
            this.textBoxAchievementStars = new System.Windows.Forms.TextBox();
            this.buttonRegionBanners = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDisplayFromDatabase)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewDisplayFromDatabase
            // 
            this.dataGridViewDisplayFromDatabase.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewDisplayFromDatabase.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridViewDisplayFromDatabase.BackgroundColor = System.Drawing.SystemColors.ActiveCaption;
            this.dataGridViewDisplayFromDatabase.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewDisplayFromDatabase.Location = new System.Drawing.Point(12, 12);
            this.dataGridViewDisplayFromDatabase.Name = "dataGridViewDisplayFromDatabase";
            this.dataGridViewDisplayFromDatabase.Size = new System.Drawing.Size(886, 346);
            this.dataGridViewDisplayFromDatabase.TabIndex = 0;
            // 
            // buttonScouts
            // 
            this.buttonScouts.BackColor = System.Drawing.SystemColors.Highlight;
            this.buttonScouts.Location = new System.Drawing.Point(12, 451);
            this.buttonScouts.Name = "buttonScouts";
            this.buttonScouts.Size = new System.Drawing.Size(75, 23);
            this.buttonScouts.TabIndex = 1;
            this.buttonScouts.Text = "Harcerze";
            this.buttonScouts.UseVisualStyleBackColor = false;
            this.buttonScouts.Click += new System.EventHandler(this.buttonScouts_Click);
            // 
            // buttonPatrols
            // 
            this.buttonPatrols.BackColor = System.Drawing.SystemColors.Highlight;
            this.buttonPatrols.Location = new System.Drawing.Point(12, 478);
            this.buttonPatrols.Name = "buttonPatrols";
            this.buttonPatrols.Size = new System.Drawing.Size(75, 23);
            this.buttonPatrols.TabIndex = 2;
            this.buttonPatrols.Text = "Zastępy";
            this.buttonPatrols.UseVisualStyleBackColor = false;
            this.buttonPatrols.Click += new System.EventHandler(this.buttonPatrols_Click);
            // 
            // buttonGroups
            // 
            this.buttonGroups.BackColor = System.Drawing.SystemColors.Highlight;
            this.buttonGroups.Location = new System.Drawing.Point(12, 507);
            this.buttonGroups.Name = "buttonGroups";
            this.buttonGroups.Size = new System.Drawing.Size(75, 23);
            this.buttonGroups.TabIndex = 3;
            this.buttonGroups.Text = "Drużyny";
            this.buttonGroups.UseVisualStyleBackColor = false;
            this.buttonGroups.Click += new System.EventHandler(this.buttonGroups_Click);
            // 
            // buttonDistricts
            // 
            this.buttonDistricts.BackColor = System.Drawing.SystemColors.Highlight;
            this.buttonDistricts.Location = new System.Drawing.Point(12, 536);
            this.buttonDistricts.Name = "buttonDistricts";
            this.buttonDistricts.Size = new System.Drawing.Size(75, 23);
            this.buttonDistricts.TabIndex = 4;
            this.buttonDistricts.Text = "Hufce";
            this.buttonDistricts.UseVisualStyleBackColor = false;
            this.buttonDistricts.Click += new System.EventHandler(this.buttonDistricts_Click);
            // 
            // buttonMoneyLog
            // 
            this.buttonMoneyLog.BackColor = System.Drawing.SystemColors.Highlight;
            this.buttonMoneyLog.Location = new System.Drawing.Point(93, 451);
            this.buttonMoneyLog.Name = "buttonMoneyLog";
            this.buttonMoneyLog.Size = new System.Drawing.Size(77, 52);
            this.buttonMoneyLog.TabIndex = 5;
            this.buttonMoneyLog.Text = "Pokaż Składki";
            this.buttonMoneyLog.UseVisualStyleBackColor = false;
            this.buttonMoneyLog.Click += new System.EventHandler(this.buttonMoneyLog_Click);
            // 
            // buttonHolidayCamps
            // 
            this.buttonHolidayCamps.BackColor = System.Drawing.SystemColors.Highlight;
            this.buttonHolidayCamps.Location = new System.Drawing.Point(95, 536);
            this.buttonHolidayCamps.Name = "buttonHolidayCamps";
            this.buttonHolidayCamps.Size = new System.Drawing.Size(75, 23);
            this.buttonHolidayCamps.TabIndex = 6;
            this.buttonHolidayCamps.Text = "Obozy";
            this.buttonHolidayCamps.UseVisualStyleBackColor = false;
            this.buttonHolidayCamps.Click += new System.EventHandler(this.buttonHolidayCamps_Click);
            // 
            // buttonGroupsAndScoutsByColor
            // 
            this.buttonGroupsAndScoutsByColor.BackColor = System.Drawing.SystemColors.Highlight;
            this.buttonGroupsAndScoutsByColor.Location = new System.Drawing.Point(608, 364);
            this.buttonGroupsAndScoutsByColor.Name = "buttonGroupsAndScoutsByColor";
            this.buttonGroupsAndScoutsByColor.Size = new System.Drawing.Size(126, 74);
            this.buttonGroupsAndScoutsByColor.TabIndex = 7;
            this.buttonGroupsAndScoutsByColor.Text = "Wyświetl harcerzy i druzyny które noszą mundury w podanym kolorze";
            this.buttonGroupsAndScoutsByColor.UseVisualStyleBackColor = false;
            this.buttonGroupsAndScoutsByColor.Click += new System.EventHandler(this.buttonGroupsAndScoutsByColor_Click);
            // 
            // buttonTournaments
            // 
            this.buttonTournaments.BackColor = System.Drawing.SystemColors.Highlight;
            this.buttonTournaments.Location = new System.Drawing.Point(12, 563);
            this.buttonTournaments.Name = "buttonTournaments";
            this.buttonTournaments.Size = new System.Drawing.Size(75, 40);
            this.buttonTournaments.TabIndex = 8;
            this.buttonTournaments.Text = "Turnieje Chorągwi";
            this.buttonTournaments.UseVisualStyleBackColor = false;
            this.buttonTournaments.Click += new System.EventHandler(this.buttonTournaments_Click);
            // 
            // buttonCircles
            // 
            this.buttonCircles.BackColor = System.Drawing.SystemColors.Highlight;
            this.buttonCircles.Location = new System.Drawing.Point(93, 563);
            this.buttonCircles.Name = "buttonCircles";
            this.buttonCircles.Size = new System.Drawing.Size(79, 40);
            this.buttonCircles.TabIndex = 9;
            this.buttonCircles.Text = "Okręgi Harcerskie";
            this.buttonCircles.UseVisualStyleBackColor = false;
            this.buttonCircles.Click += new System.EventHandler(this.buttonCircles_Click);
            // 
            // buttonCamps
            // 
            this.buttonCamps.BackColor = System.Drawing.SystemColors.Highlight;
            this.buttonCamps.Location = new System.Drawing.Point(95, 507);
            this.buttonCamps.Name = "buttonCamps";
            this.buttonCamps.Size = new System.Drawing.Size(75, 23);
            this.buttonCamps.TabIndex = 10;
            this.buttonCamps.Text = "Biwaki";
            this.buttonCamps.UseVisualStyleBackColor = false;
            this.buttonCamps.Click += new System.EventHandler(this.buttonCamps_Click);
            // 
            // buttonScoutsAchievements
            // 
            this.buttonScoutsAchievements.BackColor = System.Drawing.SystemColors.Highlight;
            this.buttonScoutsAchievements.Location = new System.Drawing.Point(608, 517);
            this.buttonScoutsAchievements.Name = "buttonScoutsAchievements";
            this.buttonScoutsAchievements.Size = new System.Drawing.Size(126, 86);
            this.buttonScoutsAchievements.TabIndex = 11;
            this.buttonScoutsAchievements.Text = "Pokaż sprawności o podanej ilości gwiazdek lub większej oraz harcerzy którzy je z" +
    "dobyli";
            this.buttonScoutsAchievements.UseVisualStyleBackColor = false;
            this.buttonScoutsAchievements.Click += new System.EventHandler(this.buttonScoutsAchievements_Click);
            // 
            // buttonGroupsAndScoutsByHeadgear
            // 
            this.buttonGroupsAndScoutsByHeadgear.BackColor = System.Drawing.SystemColors.Highlight;
            this.buttonGroupsAndScoutsByHeadgear.Location = new System.Drawing.Point(608, 444);
            this.buttonGroupsAndScoutsByHeadgear.Name = "buttonGroupsAndScoutsByHeadgear";
            this.buttonGroupsAndScoutsByHeadgear.Size = new System.Drawing.Size(126, 67);
            this.buttonGroupsAndScoutsByHeadgear.TabIndex = 12;
            this.buttonGroupsAndScoutsByHeadgear.Text = "Wyświetl wszystkich harcerzy którzy noszą podane nakrycie głowy";
            this.buttonGroupsAndScoutsByHeadgear.UseVisualStyleBackColor = false;
            this.buttonGroupsAndScoutsByHeadgear.Click += new System.EventHandler(this.buttonGroupsAndScoutsByHeadgear_Click);
            // 
            // labelInformationAboutButtons
            // 
            this.labelInformationAboutButtons.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelInformationAboutButtons.Location = new System.Drawing.Point(12, 374);
            this.labelInformationAboutButtons.Name = "labelInformationAboutButtons";
            this.labelInformationAboutButtons.Size = new System.Drawing.Size(151, 74);
            this.labelInformationAboutButtons.TabIndex = 13;
            this.labelInformationAboutButtons.Text = "Aby wyświetlić dane poszczególnych grup kliknij jeden z przycisków";
            // 
            // textBoxUniformColor
            // 
            this.textBoxUniformColor.Location = new System.Drawing.Point(743, 398);
            this.textBoxUniformColor.Multiline = true;
            this.textBoxUniformColor.Name = "textBoxUniformColor";
            this.textBoxUniformColor.Size = new System.Drawing.Size(133, 35);
            this.textBoxUniformColor.TabIndex = 14;
            // 
            // labelTextBoxColorInfo
            // 
            this.labelTextBoxColorInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelTextBoxColorInfo.Location = new System.Drawing.Point(740, 364);
            this.labelTextBoxColorInfo.Name = "labelTextBoxColorInfo";
            this.labelTextBoxColorInfo.Size = new System.Drawing.Size(158, 31);
            this.labelTextBoxColorInfo.TabIndex = 15;
            this.labelTextBoxColorInfo.Text = "Wpisz kolor, do wyboru: niebieski,zielony i szary";
            // 
            // textBoxHeadgearType
            // 
            this.textBoxHeadgearType.Location = new System.Drawing.Point(740, 481);
            this.textBoxHeadgearType.Multiline = true;
            this.textBoxHeadgearType.Name = "textBoxHeadgearType";
            this.textBoxHeadgearType.Size = new System.Drawing.Size(136, 30);
            this.textBoxHeadgearType.TabIndex = 16;
            // 
            // labelInfoAboutAvaiableHeadhear
            // 
            this.labelInfoAboutAvaiableHeadhear.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelInfoAboutAvaiableHeadhear.Location = new System.Drawing.Point(740, 436);
            this.labelInfoAboutAvaiableHeadhear.Name = "labelInfoAboutAvaiableHeadhear";
            this.labelInfoAboutAvaiableHeadhear.Size = new System.Drawing.Size(149, 42);
            this.labelInfoAboutAvaiableHeadhear.TabIndex = 17;
            this.labelInfoAboutAvaiableHeadhear.Text = "Wybierz nakrycie głowy, do wyboru: kapelusz, rogatywka i beret";
            // 
            // labelInfoAboutStars
            // 
            this.labelInfoAboutStars.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelInfoAboutStars.Location = new System.Drawing.Point(740, 517);
            this.labelInfoAboutStars.Name = "labelInfoAboutStars";
            this.labelInfoAboutStars.Size = new System.Drawing.Size(136, 28);
            this.labelInfoAboutStars.TabIndex = 18;
            this.labelInfoAboutStars.Text = "Maksymalna liczba gwiazdek to 3";
            // 
            // textBoxAchievementStars
            // 
            this.textBoxAchievementStars.Location = new System.Drawing.Point(740, 548);
            this.textBoxAchievementStars.Multiline = true;
            this.textBoxAchievementStars.Name = "textBoxAchievementStars";
            this.textBoxAchievementStars.Size = new System.Drawing.Size(136, 35);
            this.textBoxAchievementStars.TabIndex = 19;
            // 
            // buttonRegionBanners
            // 
            this.buttonRegionBanners.BackColor = System.Drawing.SystemColors.Highlight;
            this.buttonRegionBanners.Location = new System.Drawing.Point(176, 451);
            this.buttonRegionBanners.Name = "buttonRegionBanners";
            this.buttonRegionBanners.Size = new System.Drawing.Size(75, 23);
            this.buttonRegionBanners.TabIndex = 20;
            this.buttonRegionBanners.Text = "Chorągwie";
            this.buttonRegionBanners.UseVisualStyleBackColor = false;
            this.buttonRegionBanners.Click += new System.EventHandler(this.buttonRegionBanners_Click);
            // 
            // Form1Scouts
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Green;
            this.ClientSize = new System.Drawing.Size(910, 629);
            this.Controls.Add(this.buttonRegionBanners);
            this.Controls.Add(this.textBoxAchievementStars);
            this.Controls.Add(this.labelInfoAboutStars);
            this.Controls.Add(this.labelInfoAboutAvaiableHeadhear);
            this.Controls.Add(this.textBoxHeadgearType);
            this.Controls.Add(this.labelTextBoxColorInfo);
            this.Controls.Add(this.textBoxUniformColor);
            this.Controls.Add(this.labelInformationAboutButtons);
            this.Controls.Add(this.buttonGroupsAndScoutsByHeadgear);
            this.Controls.Add(this.buttonScoutsAchievements);
            this.Controls.Add(this.buttonCamps);
            this.Controls.Add(this.buttonCircles);
            this.Controls.Add(this.buttonTournaments);
            this.Controls.Add(this.buttonGroupsAndScoutsByColor);
            this.Controls.Add(this.buttonHolidayCamps);
            this.Controls.Add(this.buttonMoneyLog);
            this.Controls.Add(this.buttonDistricts);
            this.Controls.Add(this.buttonGroups);
            this.Controls.Add(this.buttonPatrols);
            this.Controls.Add(this.buttonScouts);
            this.Controls.Add(this.dataGridViewDisplayFromDatabase);
            this.Name = "Form1Scouts";
            this.Text = "Scouts";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDisplayFromDatabase)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewDisplayFromDatabase;
        private System.Windows.Forms.Button buttonScouts;
        private System.Windows.Forms.Button buttonPatrols;
        private System.Windows.Forms.Button buttonGroups;
        private System.Windows.Forms.Button buttonDistricts;
        private System.Windows.Forms.Button buttonMoneyLog;
        private System.Windows.Forms.Button buttonHolidayCamps;
        private System.Windows.Forms.Button buttonGroupsAndScoutsByColor;
        private System.Windows.Forms.Button buttonTournaments;
        private System.Windows.Forms.Button buttonCircles;
        private System.Windows.Forms.Button buttonCamps;
        private System.Windows.Forms.Button buttonScoutsAchievements;
        private System.Windows.Forms.Button buttonGroupsAndScoutsByHeadgear;
        private System.Windows.Forms.Label labelInformationAboutButtons;
        private System.Windows.Forms.TextBox textBoxUniformColor;
        private System.Windows.Forms.Label labelTextBoxColorInfo;
        private System.Windows.Forms.TextBox textBoxHeadgearType;
        private System.Windows.Forms.Label labelInfoAboutAvaiableHeadhear;
        private System.Windows.Forms.Label labelInfoAboutStars;
        private System.Windows.Forms.TextBox textBoxAchievementStars;
        private System.Windows.Forms.Button buttonRegionBanners;
    }
}

