﻿using GrzegorzMaciejewskiLab3ZadanieDomowe1.MODEL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GrzegorzMaciejewskiLab3ZadanieDomowe1
{
     public partial class Form1Scouts : Form
    {
        SqlConnection sqlConnection; // połączenie z bazą danych
        SqlDataAdapter sqlDataAdapter;//

        /// <summary>
        /// Przy inicjalizacji okna łączymy się z bazą danych
        /// 
        /// </summary>
        public Form1Scouts()
        {
            InitializeComponent();
            sqlConnection = new SqlConnection("Data Source= GRZEGORZ-KOMP\\SQLEXPRESS; database = GrzegorzMaciejewskiZadanieDomowe3Scouts; Trusted_Connection=yes");
        }


        ///Poniższe metody obsługują przyciski odpowiedzialne za wyświetlanie informacji z podłączonej bazy danych, metody
        ///są opisane w każdej klasie, tutaj znajdują się tylko odwołania, każda z metod działa na tej samej zasadzie to znaczy
        ///pobiera dane z podłączonej bazy danych i na podstawie podanych instrukcji  SELECT FROM WHERE wyświetla dane w DataGridView w
        ///postaci tabeli.
        ///Program przedstawia jedynie przykładowe zastosowanie zapytań, może ich być o wiele więcej, są łatwe w konstruowaniu
        ///w swojej aplikacji starałem się przekazać jak najlepiej strukturę harcerstwa w polsce :)
        //
        private void buttonScouts_Click(object sender, EventArgs e)
        {
            Scout.GetAllScouts(sqlConnection, sqlDataAdapter, dataGridViewDisplayFromDatabase);
        }

        private void buttonPatrols_Click(object sender, EventArgs e)
        {
            Patrol.GetAllPatrols(sqlConnection, sqlDataAdapter, dataGridViewDisplayFromDatabase);
        }

        private void buttonGroups_Click(object sender, EventArgs e)
        {
            Group.GetAllGroups(sqlConnection, sqlDataAdapter, dataGridViewDisplayFromDatabase);
        }

        private void buttonDistricts_Click(object sender, EventArgs e)
        {
            District.GetAllDistricts(sqlConnection, sqlDataAdapter, dataGridViewDisplayFromDatabase);
        }

        private void buttonTournaments_Click(object sender, EventArgs e)
        {
            Tournament.GetAllTournaments(sqlConnection, sqlDataAdapter, dataGridViewDisplayFromDatabase);
        }

        private void buttonMoneyLog_Click(object sender, EventArgs e)
        {
            MoneyLog.GetAllMoneyLogs(sqlConnection, sqlDataAdapter, dataGridViewDisplayFromDatabase);
        }

        private void buttonCamps_Click(object sender, EventArgs e)
        {
            Camp.GetAllCamps(sqlConnection, sqlDataAdapter, dataGridViewDisplayFromDatabase);
        }

        private void buttonHolidayCamps_Click(object sender, EventArgs e)
        {
            HolidayCamp.GetAllHolidayCamps(sqlConnection, sqlDataAdapter, dataGridViewDisplayFromDatabase);
        }

        private void buttonCircles_Click(object sender, EventArgs e)
        {
            Circle.GetAllCircles(sqlConnection, sqlDataAdapter, dataGridViewDisplayFromDatabase);
        }

        private void buttonRegionBanners_Click(object sender, EventArgs e)
        {
            RegionBanner.GetAllRegionBanners(sqlConnection, sqlDataAdapter, dataGridViewDisplayFromDatabase);
        }

        private void buttonGroupsAndScoutsByColor_Click(object sender, EventArgs e)
        {
            Scout.GetAllScoutsWithUniformColor(sqlConnection, sqlDataAdapter, dataGridViewDisplayFromDatabase, textBoxUniformColor.Text);
        }

        private void buttonGroupsAndScoutsByHeadgear_Click(object sender, EventArgs e)
        {
            try
            {
                Scout.GetAllScoutsWithConditionHeadGear(sqlConnection, sqlDataAdapter, dataGridViewDisplayFromDatabase, textBoxHeadgearType.Text);
            }
            catch(Exception ex)
            {
                MessageBox.Show("Błędne dane");
            }

        }

        private void buttonScoutsAchievements_Click(object sender, EventArgs e)
        {
            Achievement.GetAllAchievementsByStars(sqlConnection, sqlDataAdapter, dataGridViewDisplayFromDatabase, textBoxAchievementStars.Text);
        }
    }
}
