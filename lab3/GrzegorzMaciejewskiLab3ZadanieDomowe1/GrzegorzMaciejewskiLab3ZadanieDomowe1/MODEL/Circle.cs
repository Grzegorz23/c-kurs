﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GrzegorzMaciejewskiLab3ZadanieDomowe1.MODEL
{
    class Circle
    {   
        ///Metoda wyświetla wszystkie okręgi harcerskie jakie są w związku oraz ich przewodniczących
        ///dane znajdują się w jednej tabeli
        //
        public static void GetAllCircles(SqlConnection sqlConnection, SqlDataAdapter sqlDataAdapter, DataGridView dataGridView)
        {
            dataGridView.DataSource = null;
            sqlDataAdapter = new SqlDataAdapter("Select Nazwa as Nazwa_Okręgu,Przewodniczacy as Przewodniczący_okręgu from Circles ", sqlConnection);
            DataTable dataTable = new DataTable();
            sqlDataAdapter.Fill(dataTable);
            dataGridView.DataSource = dataTable;
        }
    }
}
