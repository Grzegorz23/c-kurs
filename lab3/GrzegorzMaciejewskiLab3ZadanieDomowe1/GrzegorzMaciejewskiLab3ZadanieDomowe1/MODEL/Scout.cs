﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GrzegorzMaciejewskiLab3ZadanieDomowe1.MODEL
{
   public class Scout
    {
        /// <summary>
        /// Metoda wyświetla wszystkich harcerzy i ich stopnie
        /// </summary>
        /// <param name="sqlConnection"></param>
        /// <param name="sqlDataAdapter"></param>
        /// <param name="dataGridView"></param>
        public static void GetAllScouts(SqlConnection sqlConnection, SqlDataAdapter sqlDataAdapter,DataGridView dataGridView)
        {
            dataGridView.DataSource = null;
            sqlDataAdapter = new SqlDataAdapter("Select Imie,Nazwisko,stopien from Scouts ", sqlConnection);
            DataTable dataTable = new DataTable();
            sqlDataAdapter.Fill(dataTable);
            dataGridView.DataSource = dataTable;
        }
        /// <summary>
        /// Metoda wyświetla wystkich harcerzy z wybranym przez nas poprzez wpisanie w TextBoxie nakryciem głowy
        /// tabele z bazy danych są połączone za pomocą komendy join on, wyszukiwanie poprzez zapytanie Where
        /// </summary>
        /// <param name="sqlConnection"></param>
        /// <param name="sqlDataAdapter"></param>
        /// <param name="dataGridView"></param>
        /// <param name="condition"></param>
        public static void GetAllScoutsWithConditionHeadGear(SqlConnection sqlConnection, SqlDataAdapter sqlDataAdapter, DataGridView dataGridView,String condition)
        {
            dataGridView.DataSource = null;
            sqlDataAdapter = new SqlDataAdapter("Select Imie, Nazwisko, NakrycieGlowy, g.Nazwa as Nazwa_Druzyny" +
            " From Groups g join Patrols p on g.ID = p.IDDruzyny join Scouts s on s.IDZastepu = p.ID join" +
            " Uniforms u on u.IDDruzyny = g.ID Where NakrycieGlowy = "+"'"+condition+"'", sqlConnection);
            DataTable dataTable = new DataTable();
            sqlDataAdapter.Fill(dataTable);
            dataGridView.DataSource = dataTable;
        }
        /// <summary>
        /// Metoda wyświetla wystkich harcerzy w mundurach w wybranym przez nas kolorze
        /// tabele z bazy danych są połączone za pomocą komendy join on, wyszukiwanie poprzez zapytanie Where
        /// </summary>
        /// <param name="sqlConnection"></param>
        /// <param name="sqlDataAdapter"></param>
        /// <param name="dataGridView"></param>
        /// <param name="condition"></param>
        public static void GetAllScoutsWithUniformColor(SqlConnection sqlConnection, SqlDataAdapter sqlDataAdapter, DataGridView dataGridView, String condition)
        {
            dataGridView.DataSource = null;
            sqlDataAdapter = new SqlDataAdapter("Select g.Nazwa as Nazwa_Druzyny, s.Imie as Imie_Harcerza,"+
                "s.Nazwisko as Nazwisko_Harcerza, kolormunduru as Kolor_Munduru"+
                " from Groups g join Patrols p on p.IDDruzyny = g.ID join Scouts s on p.ID = s.IDZastepu"+
                " join Uniforms u on g.ID = u.IDDruzyny where KolorMunduru= " + "'" + condition + "'", sqlConnection);
            DataTable dataTable = new DataTable();
            sqlDataAdapter.Fill(dataTable);
            dataGridView.DataSource = dataTable;
        }
    }
}
