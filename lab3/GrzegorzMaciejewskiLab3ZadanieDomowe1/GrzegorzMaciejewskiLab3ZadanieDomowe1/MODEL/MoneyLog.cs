﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GrzegorzMaciejewskiLab3ZadanieDomowe1.MODEL
{
    class MoneyLog
    {
        ///Metoda wyświetla wszystkie składki które zostały wpłacone do związku oraz informacje o tym jaka drużyna i kiedy je wpłaciła
        ///dane znajdują się w kilku tabalach, aby je wydobyć użyłem komendy join on
        /// 
        //
        public static void GetAllMoneyLogs(SqlConnection sqlConnection, SqlDataAdapter sqlDataAdapter, DataGridView dataGridView)
        {
            dataGridView.DataSource = null;
            sqlDataAdapter = new SqlDataAdapter("Select Data as Za_jaki_miesiąc_w_danym_roku,Składka as"+ 
                "Ile_wpłacono,Nazwa as Nazwa_Drużyny, Drużynowy"+
                 " from MoneyLogs ml join Groups g on ml.IDDruzyny = g.ID ", sqlConnection);
            DataTable dataTable = new DataTable();
            sqlDataAdapter.Fill(dataTable);
            dataGridView.DataSource = dataTable;
        }
    }
}
