﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GrzegorzMaciejewskiLab3ZadanieDomowe1.MODEL
{
    class RegionBanner
    {
        ///Metoda wyświetla wszystkie Chorągwie we wszystkich okręgach oraz ich komendantów
        ///dane znajdują się w jednej tabeli
        /// 
        //
        public static void GetAllRegionBanners(SqlConnection sqlConnection, SqlDataAdapter sqlDataAdapter, DataGridView dataGridView)
        {
            dataGridView.DataSource = null;
            sqlDataAdapter = new SqlDataAdapter("Select Nazwa as Nazwa_Chorągwi,[Komendant(ka)] as Komendant_lub_komendantka_chorągwi from RegionBanners ", sqlConnection);
            DataTable dataTable = new DataTable();
            sqlDataAdapter.Fill(dataTable);
            dataGridView.DataSource = dataTable;
        }
    }
}
