﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GrzegorzMaciejewskiLab3ZadanieDomowe1.MODEL
{
    class Patrol
    {
        private static DataTable dataTable;

        ///Metoda wyświetla wszystkie Zastępy ze wszystkich druzyn oraz ich zastępowego, wyśietla także informacje
        ///czy dany zastęp posiada okrzyk czy nie
        ///dane znajdują się w jednej tabeli
        /// 
        //
        public static void GetAllPatrols(SqlConnection sqlConnection, SqlDataAdapter sqlDataAdapter, DataGridView dataGridView)
        {
            dataGridView.DataSource = null;
            sqlDataAdapter = new SqlDataAdapter("Select NazwaZastepu as Nazwa_Zastępu,Zastepowy,Okrzyk from Patrols ", sqlConnection);
            dataTable = new DataTable();
            sqlDataAdapter.Fill(dataTable);
            dataGridView.DataSource = dataTable;
        }
    }
}
