﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GrzegorzMaciejewskiLab3ZadanieDomowe1.MODEL
{
    class Achievement
    {   
        /// <summary>
        /// Metoda wyświetla wszystkich harcerzy którzy zdobyli sprawności o podanej ilości gwiazdek, tabele z baz danych 
        /// są połączone ze sobą za pomocą operatora "join on" żeby uzyskać więcej informacji
        /// </summary>
        /// <param name="sqlConnection"></param>
        /// <param name="sqlDataAdapter"></param>
        /// <param name="dataGridView"></param>
        /// <param name="condition"></param>
        public static void GetAllAchievementsByStars(SqlConnection sqlConnection, SqlDataAdapter sqlDataAdapter, DataGridView dataGridView,String condition)
        {
            if (condition == "")
                condition = "999";
            dataGridView.DataSource = null;
            sqlDataAdapter = new SqlDataAdapter("Select Sprawnosc, Ilosc_gwiazdek, imie as Imie_harcerza,"+
                " nazwisko as Nazwisko_harcerza,stopien as Stopien_harcerza from Achievements a"+
                " join Scouts s on s.ID=a.IDHarcerza where ilosc_gwiazdek >= "+"'"+condition+"'", sqlConnection);
            DataTable dataTable = new DataTable();
            sqlDataAdapter.Fill(dataTable);
            dataGridView.DataSource = dataTable;
            
        }
    }
}
