﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GrzegorzMaciejewskiLab3ZadanieDomowe1.MODEL
{
    class Camp
    {
        ///Metoda wyświetla wszystkie Biwaki na których były wszstkie zastępy ze wszystkich drużyn związku
        ///dane znajdują się w kilku tabelach ab je wydobyć użyłem komendy join on
        //
        public static void GetAllCamps(SqlConnection sqlConnection, SqlDataAdapter sqlDataAdapter, DataGridView dataGridView)
        {
            dataGridView.DataSource = null;
            sqlDataAdapter = new SqlDataAdapter("Select MiejsceBiwaku,IloscDni,NazwaZastepu"+
            " from Camps c join Patrols p on c.IDZastepu = p.ID ", sqlConnection);
            DataTable dataTable = new DataTable();
            sqlDataAdapter.Fill(dataTable);
            dataGridView.DataSource = dataTable;
        }
    }
}
