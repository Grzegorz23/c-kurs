﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GrzegorzMaciejewskiLab3ZadanieDomowe1.MODEL
{
    class HolidayCamp
    {
        ///Metoda wyświetla wszystkie obozy oraz ich lokalizacje a także rok w którym dany obóz się odbył
        ///dane znajdują się kilku tabelach, aby je wydobyć użyłem komendy join on
        /// 
        //
        public static void GetAllHolidayCamps(SqlConnection sqlConnection, SqlDataAdapter sqlDataAdapter, DataGridView dataGridView)
        {
            dataGridView.DataSource = null;
            sqlDataAdapter = new SqlDataAdapter("Select Miejsce as Miejsce_Obozowe,rok as Rok_Obozu, g.nazwa as Nazwa_Drużyny_która_obozuje"+
            " from HolidayCamps hc join Groups g on g.ID = hc.IDDruzyny", sqlConnection);
            DataTable dataTable = new DataTable();
            sqlDataAdapter.Fill(dataTable);
            dataGridView.DataSource = dataTable;
        }
    }
}
