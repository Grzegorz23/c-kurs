﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GrzegorzMaciejewskiLab3ZadanieDomowe1.MODEL
{
    class Tournament
    {
        ///Metoda wyświetla wszystkie turnieje organizowane przez wszystkie chorągwie
        ///dane znajdują się w jednej tabeli
        /// 
        //
        public static void GetAllTournaments(SqlConnection sqlConnection, SqlDataAdapter sqlDataAdapter, DataGridView dataGridView)
        {
            dataGridView.DataSource = null;
            sqlDataAdapter = new SqlDataAdapter("Select Turniej as Nazwa_Druzyny,Opis from Tournaments ", sqlConnection);
            DataTable dataTable= new DataTable();
            sqlDataAdapter.Fill(dataTable);
            dataGridView.DataSource = dataTable;
        }
    }
}
